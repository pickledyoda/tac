<html>
<head>
<link rel="stylesheet" type="text/css" href="tac.css">
</head>
<body>
<font class="text">
<?php
include "includes/phpself.php";

/* if the `submit`  has been clicked... */

if (isset($_POST['submit'])) {
  $target_path = "uploads/";
  $uploadfile = $target_path.basename($_FILES['cbfile']['name']);
  $tmp = $_FILES['cbfile']['name'];

  /*  check 1:  are the fields filled?  */

  if (!$_POST['cbname'] || !$_POST['cbtype'] || !$_POST['cbplatform'] || !$uploadfile || !$_POST['cbnumber'] || !$_POST['authname'] || !$_POST['authpin'] || !$_POST['authmail']) {
    echo "<p class=\"text\">Your submission is incomplete. Please use the [BACK] button on your browser to return to the form and make sure all the mandatory fields are filled.</p>";
  }

  /*  check 2:  filetype  */

  elseif (strtoupper(substr($uploadfile, -4)) != ".ZIP" && strtoupper(substr($uploadfile, -4)) != ".RAR") { echo "<p class=\"text\">Sorry, your battle can not be uploaded - the file must be a .ZIP or .RAR archive.<br>Use the [BACK] button on your browser to return to the form.</p>"; }

  /*  check 3:  duplicate filename  */

  elseif (file_exists($uploadfile)) { echo "<p class=\"text\">Sorry, your battle can not be uploaded - the target file already exists on the server. please use another filename.<br>Use the [BACK] button on your browser to return to the form.</p>"; }

  /*  go ahead  */

  elseif (move_uploaded_file($_FILES['cbfile']['tmp_name'], $uploadfile)) {
    echo "<font class=\"text\"><p>Your custom battle has been uploaded to the database. ";
    echo "<br>";
    echo "You will receive updates via e-mail as your battle is being processed. Processing can take several weeks, so please be patient.";
    echo "</p><p>";
    echo "Feel free to e-mail the Tactical Officer if you have not received an update on your battle's progress within the next two weeks.";
    echo "</p><p>";
    echo "Thank you for your contribution!";
    echo "</p></font>";
    echo $query;

    $cbname = $_POST['cbname'];
    $cbtype = $_POST['cbtype'];
    $cbplatform = $_POST['cbplatform'];
    $cbmedal = $_POST['cbmedal'];
    $cbpatch = $_POST['cbpatch'];
    $cbfile = str_replace($targetpath,"",$uploadfile);
    $cbnumber = $_POST['cbnumber'];
    $authname = $_POST['authname'];
    $authpin = $_POST['authpin'];
    $authmail = $_POST['authmail'];
    $authcomment = $_POST['authcomment'];
    $safeip = $_SERVER['REMOTE_ADDR'];

    include "database/addbattle.php";
    $mailrsn = "newbattle";
    include "database/mailform.php";

  }
  else {
    echo "<p class=\"text\">Your upload has failed. Please try again, or submit your custom battle via e-mail.</p>";
  }
}

/* else, display form */

else {
  echo "<p class=\"text\"> Use this form to upload your custom Battle, Free Mission, or Level to the TAC Office. Once done, you will be kept
  appraised of your battle's progress through the queue until it is released.<br><i>Note: Be sure to include the Pilot File you used to test fly the mission in the archive you upload!</i></p>";

  echo "<form enctype=\"multipart/form-data\" action=\"".getPHPSelf()."\" method=\"POST\">";
  echo "<table width=\"600\">";
  echo "<tr><td width=\"600\" colspan=\"2\" height=\"30\" valign=\"center\" class=\"tacmantitle\"><b>BATTLE INFORMATION</b></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" class=\"text\" valign=\"center\">Battle/Mission Title:</td><td width=\"400\" height=\"30\" class=\"text\" valign=\"center\"><input type=\"text\" size=\"50\" name=\"cbname\"></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" class=\"text\" valign=\"center\">Game Platform:</td><td width=\"400\" height=\"30\" class=\"text\" valign=\"center\"><select name=\"cbplatform\" size=\"1\"><option>X-wing (XW)</option><option>TIE Fighter (TIE)</option><option>X-wing vs TIE Fighter (XvT)</option><option>Balance of Power (BoP)</option><option>X-wing Alliance (XWA)</option><option>Jedi Academy (JA)</option><option>Star Wars Galactic Battlegrounds (SWGB)</option><option>Imperial Alliance (IA)</option><option>Imperial Assault (IAS)</option><option>Empire at War (EaW)</option><option>Battlefront (BF)</option><option>Battlefront II (BF2)</option></select></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" class=\"text\" valign=\"center\">Battle/Mission Type:</td><td width=\"400\" height=\"30\" class=\"text\" valign=\"center\"><select name=\"cbtype\" size=\"1\"><option>TIE Corps (TC)</option><option>Infiltrator Wing (IW)</option><option>Dark Brotherhood (DB)</option><option>Intelligence Division (ID)</option><option>Bounty Hunter's Guild (BHG)</option><option>Combined Arms Battles (CAB)</option><option>Fleet Commander's Honor Guard (FCHG)</option><option>Imperial Senate (IS)</option><option>Hammer's Fist (HF)</option><option>Fleet Medical Corps (FMC)</option><option>Directorate (DIR)</option><option>Corporate Division (CD)</option><option>Multi Player (MP)</option><option>Free Mission (FREE)</option></select></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" class=\"text\" valign=\"center\">Number of Missions/Levels:</td><td width=\"400\" height=\"30\" class=\"text\" valign=\"center\"><input type=\"text\" size=\"5\" name=\"cbnumber\"></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" class=\"text\" valign=\"center\">Medal Name: <sup>*</sup></td><td width=\"400\" height=\"30\" class=\"text\" valign=\"center\"><input type=\"text\" size=\"50\" name=\"cbmedal\"></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" class=\"text\" valign=\"center\">Patches Needed: <sup>*</sup></td><td width=\"400\" height=\"30\" class=\"text\" valign=\"center\"><input type=\"text\" size=\"50\" name=\"cbpatch\"></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" class=\"text\" valign=\"center\">Battle .ZIP File:</td><td width=\"400\" height=\"30\" class=\"text\" valign=\"center\"><input name=\"cbfile\" size=\"50\" type=\"file\"></td></tr>";
  echo "<tr><td width=\"600\" colspan=\"2\" height=\"30\" valign=\"center\">Fields marked with <sup>*</sup> are optional.</td></tr>";
  echo "<tr><td width=\"600\" colspan=\"2\" height=\"30\" valign=\"center\">&nbsp;</td></tr>";
  echo "<tr><td width=\"600\" colspan=\"2\" height=\"30\" valign=\"center\" class=\"tacmantitle\"><b>CREATOR INFORMATION</b></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" class=\"text\" valign=\"center\">Creator's Name:</td><td width=\"400\" height=\"30\" class=\"text\" valign=\"center\"><input type=\"text\" size=\"50\" name=\"authname\"></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" class=\"text\" valign=\"center\">Creator's PIN:</td><td width=\"400\" height=\"30\" class=\"text\" valign=\"center\"><input type=\"text\" size=\"50\" name=\"authpin\"></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" class=\"text\" valign=\"center\">Creator's e-Mail:</td><td width=\"400\" height=\"30\" class=\"text\" valign=\"center\"><input type=\"text\" size=\"50\" name=\"authmail\"></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" class=\"text\" valign=\"top\">Comments: <sup>*</sup></td><td width=\"400\" height=\"30\" class=\"text\" valign=\"center\"><textarea name=\"authcomment\" cols=\"38\" rows=\"5\"></textarea></td></tr>";
  echo "<tr><td width=\"600\" colspan=\"2\" height=\"30\" valign=\"center\">Fields marked with <sup>*</sup> are optional.</td></tr>";
  echo "<tr><td width=\"600\" colspan=\"2\" height=\"30\" valign=\"center\">&nbsp;</td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" class=\"text\" valign=\"center\">&nbsp;</td><td width=\"400\" height=\"30\" class=\"text\" valign=\"center\"><input type=\"submit\" value=\"Submit Custom Battle\" name=\"submit\"></td></tr>";
  echo "</table>";
  echo "</form>";
}

?>
