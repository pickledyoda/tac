# Rewards

No one works for free. That's why the Tactical Office has set some rules about awards given to its members.

These are Emperor's Hammer medals that can be earned only for services in Tactical Office, custom mission submission and any other Tactical Office related things. Service medals are generally earned for the specified amount of work done, while hammers are earned for each single submission of battle and mission.

## Medal of Tactics

The Medal of Tactics is awarded by the Tactical Officer to members who create an EH Battle Center approved battle or free mission.

For every approved Free Mission, the Medal of Tactics - Green Hammer is awarded.

For every approved Battle, the Medal of Tactics - Red Hammer is awarded. Multiple Red Hammers are awarded for longer battles in 5-mission increments: 4-8 missions awards 1 Red Hammer, 9-13 missions awards 2 Red Hammers, etc

For every non-flying platform mission there is a Yellow Hammer.

For every 5 missions a Tactician or Tactical Coordinator corrects, the Medal of Tactics - Blue Hammer is awarded. A Blue Hammer may also be awarded to participants of bug correction projects.

## Commendation of Service

The Tactical Officer awards the Commendation of Service to members who submit an approved plotline in conjunction with an approved battle. The requirements for the CoS are as follows:

1. the battle must be composed of at least 6 missions;
2. each mission must have at least 12 radio messages;
3. the battle must have an outstanding description/plotline provided in a text text file;
4. a medal image must be submitted.

<i>The Tactical Officer reserves the right to deny the awarding of a CoS even if the requirements are met.</i>

## Beta points

Tacticians and Tactical Surveyors use a &beta; points system, while Tactical Coordinators and Head Coordinators are awarded on an individual basis.

Tacticians and Tactical Surveyors who have rendered their services to the Tactical Office for a prolonged period of time can be awarded several merit medals for these services. This can be an Imperial Security Medal all the way up to Bronze and Silver Stars of the Empire.

In exceptional cases, at the TAC's discretion, a Tactician or Tactical Surveyor may even be awarded a Gold Star of the Empire for his work in the Tactical Office.
