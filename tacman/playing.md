## Playing custom battles

The ability to play missions from the EH Mission Compendium is the most basic and important function of each TIE Corps pilot. With the introduction of the EH Battle Launcher (EHBL), the entire process became as easy as one click of the mouse. All flying platforms use basically the same method, with a few differences.

Custom battles or missions are downloaded from the Mission Compendium in .ehm format. This is a custom file format designed and created by the Science Office for use with the EHBL. Double-clicking on the .ehm file will install the mission files and battle file in your game. The EHBL window will show you four buttons for each of the flying platforms. Simply click on the burron for the game you wish to play.

Note that the installation procedure will overwrite the game's original battle files. However, restoring those is easily done by copying the original file from the game CD.

The battle files involved are:

1. <b>TIE Fighter</b>: BATTLE1.LFD in the /RESOURCE/ directory (sometimes also BATTLE2.LFD, etc)
2. <b>X-wing vs. TIE Fighter</b>: IMPERIAL.LST in the /TRAIN/ directory
3. <b>Balance of Power</b>: IMPERIAL.LST in the /TRAIN/ directory
4. <b>X-wing Alliance</b>: MISSION.LST in the /MISSIONS/ directory

After installation, the README.TXT file included in the battle will automatically be opened. This file will inform you if there are things that you need to do before playing the game, so you are advised to read it. The pilot should also always make sure the necessary patches are installed. The README.TXT does not always tell you if patches are needed. Installing patches is done via the Super XP Installer that can be downloaded from the Science Office website.

## Special considerations

### TIE Fighter

Rebel fighters and transports are not provided in the EHSP or as separate patches anymore. For battles or missions which will have you fly them, you must enable them in the EHBL via the Game -> TIE Options menu. Note that you can only have one option enabled.

### X-Wing vs. TIE Fighter

Transports are not provided in the EHSP or as separate patches anymore. For battles or missions which will have you fly them, you must enable them in the EHBL via the Game -> XvT Options menu.

### Balance of Power

Transports are not provided in the EHSP or as separate patches anymore. For battles or missions which will have you fly them, you must enable them in the EHBL via the Game -> XvT Options menu.

### X-Wing Alliance

X-wing Alliance requires you to use the EHBL to create a pilot fo fly the battle or mission with. To create a pilot, choose Game -> XWA options from the menu, and click on the 'New Pilot' button to create a pilot. Make sure to check the box for 'Prepare for EH Battle'. Click OK, and select the pilot before starting the game.

## Other gaming platforms

Installation of custom missions for the other gaming platforms in the Compendium (X-wing, Empire at War, Star Wars Galactic Battlegrounds, Jedi Academy) are provided in the archive you download. At some point this document will also have installation instructions for those games.
