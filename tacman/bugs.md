# Bug reporting

If you happen to encounter a battle or free mission that has bugs, do not attempt to correct it on your own (see the Cheating Policy). Only members of the Tactical Office staff are authorized to fix bugs in official missions.

Instead, you can fill out a Bug Report for the battle. Access the Mission Compendium and bring up the profile of the faulty battle. Click "Submit Bug Report" and fill out the necessary information. Even if someone else has posted a bug report about the same problem, report the bug anyway. Keep in mind that you must fly the mission without using Invulnerability/Unlimited Ammo (TIE/XWA) or Unlimited Waves (XvT/BoP) to post a legal bug report. If you encounter a problem with mission and you use "cheats", turn them off and try again. This will prevent you from posting bug reports caused only by "cheats".

After reporting the bug, you have to hold off completing the battle until after the bugs have been fixed. Note that all missions have been completed at one time or another and you may have just misunderstood the mission objectives; the 'bug' you have encountered may be linked to the fact you haven't completed the mission the way it has to be.

Authors of an official battle or free mission may submit to the Tactical Officer an updated version of their work at any time, if it fits the following conditions:

1. the latest bug corrections brought by the Tactical Staff must be taken into account;
2. the maximum reachable score cannot be lower than in the original mission, to prevent unreachable High Scores to occur.

## Fatal bugs

Fatal bugs are those which prevent the tester or player from successfully completing the mission. There are many different types of fatal bugs:

### Ship Crashes

These are major bugs, but there are two exceptions:

1. if author wanted to have it this way
2. if it's random crash (sometimes fighters may crash with capital ship)

### Violation of the craft limit

Every platform has craft limit. If violated, it can cause unexpected results. If the mission you test is acting weird, then you can check it with mission editor and see what's going on.

### Errors in mission goals

These are serious bugs. Mission goals should always be clear and you should be able to achieve them. This applies to bonus goals as well.

### EH Command Staff events

These guys and girls cannot die. They can be captured, attacked or any other thing can happen to them, but they cannot die. Better kill some no-name admiral.

### Everything else

Use your best judgment. Ask your superior.

## Spelling and grammar mistakes

Spelling mistakes are not fatal bugs and you may encounter many of them in the EH Mission Compendium. While generally they should be caught by the beta testers, you must understand that this club is international.

Although English is the official language of the Emperor's Hammer, some of the pilots do not know the language perfectly and they can often miss the spelling bugs while they test.

## Reinforcements

Lack of reinforcements is not a bug, however it is good for every battle to have them. Not every pilot is a Top Gun.

## Patches

All problems with patches should be reported directly to EH Science Officer as he is the one who manages/updates them. Problem with patches will usually result in a game crash.

Before going directly to SCO, you should first verify if the patch is properly installed in your game. Try installing it again, and if all of your attempts fail, contact the SCO.

Also, make sure that when you install the game to select full/complete install. It has been known that some patches need to have it on the complete install, not minimum to work.
