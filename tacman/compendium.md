# Mission Compendium

The Mission Compendium holds the complete listing of all the official battles and free missions that have been created for the Emperor's Hammer. The battles are divided into different types, to reflect the subgroup for which they were created. However, any pilot can fly battles from all subgroups and submit their pilotfiles.

Battles in the Mission Compendium are organized by platform, and then by subgroup.

## Battle Submission Form

The Battle Submission Form (BSF) is a system to automate processing of submitted pilot files. When a pilot file is processed, the pilot's combat record and FCHG ranking will be automatically updated to reflect the pilot has flown the battle and record any high scores attained.

All members can log-in to the TIE Corps Database and submit BSFs for any battle that they or their subordinates have completed. To BSF a pilot file, simply use the link in your Administration Menu entitled 'Submit battle (BSF) to Tactical Office'.

If for any reason you are unable to submit BSFs on the database, send your pilot files to your commanding officer.

## High Scores

High Scores serve a special function in the Emperor's Hammer. The Battle Board is a complete listing of the best scores for every mission that has been flown in the Mission Compendium (and who has achieved it), as well as calculating the Battle High Score. These Scores serve an integral part in the competitive spirit that helps the TIE Corps flourish and also play a major role in the Fleet Commander's Honor Guard (FCHG) system.

High Scores provide an incentive to pilots to fly their best in addition to flying often. High Scores can increase a pilot's FCHG score exponentially, especially if they get all of the High Scores on a battle.

In any scoring system there are scoring exceptions allied to the rules. It is not different in the EH's rules. Exceptions have been created to address special circumstances pilots may encounter while using official EH games. The exceptions currently in place are:

<b><u>TIE Fighter</u></b>: if a pilot played the battle in the Combat Chamber, the pilot file is invalid.

<b><u>X-Wing vs. TIE Fighter</u></b>: pilots must use the original included .LST files with the battles. If a pilot uses a custom made .LST file that prevents you from viewing individual scores, the pilot file is invalid.

<b><u>X-Wing Alliance</u></b>: if a pilot has informed you that he skipped a mission in a battle, the pilot file is invalid.

<b><u>Laserless scoring</u></b>: In TIE Fighter, a user is deducted points by the game each time he triggers his lasers. However, each hit wins you more points than you lose. The game uses this to reward pilots for accuracy. This way, endlessly shooting lasers at targets will gain points. To cancel this, the database enforces a "laserless" scoring system for TIE Fighter. Each laser hit wins the pilot 3 points in the game.

During processing, these points are deducted from a mission score. This means that it is pointless to fire lasers just to win points, as it will not do so anymore. The formula applied is:

`Final Score = Overall Score - (Laser hits x 3)`

<b><u>X-Wing Alliance bonus scoring</u></b>: in XWA missions it is highly likely that bonus points will be awarded in the midst of flying them. These points should be included in the mission scores, as they are similar to the secondary and bonus goals in TIE Fighter.

<b><u>Audits</u></b>: while the TIE Corps database system automatically approves all BSFs that do not contain any High Scores, audits do occur at random (though they occur more often in BSFs with High Scores). Simply reply quickly to the audit, supplying the pilot file that you used to fill out the BSF. For this reason you should always keep any pilot files that you use until they have been approved and are on your pilot's record. Even then, many CMDRs like to save them in case they are ever needed again. The faster you reply to the Audit, the more quickly your BSF will be approved (with the exception of pilot files that look suspicious, in which case you will be further notified). You have one week to comply or the BSF will automatically be denied.

## Rating System

The TIE Corps Database has a Rating System where pilots can rate battles they have played. This is not required but it is encouraged for pilots to rate battles that they fly. It lets designers know what kinds of missions people like to fly and what particular things about a battle people liked the most or the least, for consideration with future battle designs.

To review and rate a battle, choose the battle that you would like to rate on the Mission Compendium, and click 'Submit Review'. If you are not logged into the database, you will be prompted for a PIN # and password. You can give it a rating depending on how much you enjoyed the battle. '5' is the best, and is usually reserved for the best battles/free missions with an original and intriguing storyline and no bugs.

Battles with a '0' rating usually signify that something is very wrong with it: either it is a poor storyline or there are bugs rampant throughout the battle.

You can also post your comments on the battle where everyone can see them or choose to rate it anonymously. Comments are optional and should be kept to a reasonable length. Comments are not the place to submit a bug report! See the section on Bug Reports about how to properly report bugs in battles. Comments that are off-topic, contain swearing or spamming, or make personal attacks on the battle designer will be deleted and the author may lose the privilege to post future reviews on the Battle Center.

Pilots must use their discretion to give each battle its own rating. The reviewer can change ratings at any time.
