# Creating and submitting battles

There is a high standard for all battles, missions and levels that are to be placed in the Mission Compendium. All battles, missions and levels that are submitted to the Tactical Officer must obey these standards or they will be rejected at the correction stage. The guidelines for each platform can be found below, as well as information on plotlines, medals, and the Review Process. Please follow exactly the submission procedures, or your submission will be simply rejected.

## Submission procedure

There are several rules you have to follow, while you submit your battle for EH Mission Compendium:

1. a mission creator must have passed the course 'Tactical Mission Creation and Beta Testing Standards' (MCBS) from the Imperial Academy;
2. a mission creator must have passed the misson design course for the platform he is creating for;
3. all text and briefings must be in English;
4. the battle or mission must be suitable for all ages with no inappropriate content;
5. the battle or mission must meet standards for its platform (see below);
6. the mission creator must have test-flown his missions and provide a pilot file as proof;
7. the battle must be submitted via the battle submission form on the TAC Office website. The uploaded archive (.ZIP or .RAR format) must include all required files. Only if this form does not work can you e-mail it to the Tactical officer, providing the same information the form would ask you for.

All archives must contain the following files:

1. custom battle file
2. mission files
3. pilot file
4. readme.txt
5. optional: patches.txt
6. optional: image of the medal awarded (in .JPG or .PNG or .GIF format)

You can also add storyline to your mission or battle to make it more playable and enjoyable for all members. The Tactical Officer can award you with Commendation of Service for every good storyline you make to your battle. Also, the ratings of your battle may be higher when it contains a storyline.

## Battle standards

Every battle submitted to the Mission Compendium must follow the guidelines and regulations of Tactical Office. These guidelines and regulations have been set to help people with their submissions, to make the submission process more efficient and to avoid total chaos with submissions.

Every platform is different, so there are slightly different submission regulations for each platform. You can find the minimum requirements below. These listings are for battles. The same rules apply to free missions, although you only have 1 mission there, there is no medal required, and you can not receive a Commendation of Service for them.

## TIE Fighter Battle standards

TIE Fighter is the first and the most popular game of the Emperor's Hammer. There are over 200 TIE Fighter battles and missions and the numbers are still growing. Even with the introduction of new, more advanced gaming platforms like X-Wing Alliance, TIE Fighter is still popular and people still submit battles for it.

The official mission creation program for TIE Fighter is TIE Fighter Workshop by Evan Sabatelli. While mission creators are not obliged to use this utility, the use of other tools can create problems when testers or tacticians attempt to open mission files, which can lead to rejection of your battle.

All TIE Fighter battles must meet the following standards:

1. <b><u>readme.txt</u></b>
   every battle must contain this file, with basic information about the battle and its author. It must also contain the disclaimer that is required by LucasArts with ever custom mission (see the bottom of this document). You can find a detailed list of the information expected in a readme file below;
2. <b><u>patches.txt</u></b>
   you can include information about required patches (including the EHSP) inside a dedicated file called patches.txt. As the information is required in the readme file, use of this file is optional;
3. <b><u>number of missions</u></b>
   every custom battle must consist of at least 4 missions. Custom battles with more than 8 missions will have to be split across multiple battle files;
4. <b><u>.LFD file</u></b>
   every battle must contain a BATTLE1.LFD file which is required to play the battle;
5. <b><u>briefings</u></b>
   animated and officer briefings must be provided for all missions. Providing good debriefings is highly encouraged.The briefings must supply the pilot with enough information to achieve the mission's primary goals (except in certain scenarios);
6. <b><u>radio messages</u></b>
   all missions must utilize the in flight radio messages;
7. <b><u>TIE95 compatible</u></b>
   TIE95 is the only official TIE version of the Emperor's Hammer. therefore, all EH battles must work with TIE95. You can make it compatible with all TIE versions, but you must consider TIE95 as highest priority. This also means you may NOT use the decoy beam, as this only works in TIE Disk;
8. <b><u>updates.txt</u></b>
   this is an optional file, used by Tactical Staff, to list updates made to the battle during the beta testing and correction process.

## X-wing vs. TIE Fighter Battle standards

The introduction of X-Wing vs. TIE Fighter was another milestone of the Emperor's Hammer. The game itself had advanced graphic design, much more realistic flying, faster lasers, and even multiplayer ability. Also, the mission creation process was much easier. Pilot files became more advanced and allowed Tactical Officer to limit cheating much more efficiently than in TIE.

The official mission creation program for X-wing vs. TIE Fighter is XvTED by Troy Dangerfield. While mission creators are not obliged to use this utility, the use of other tools can create problems when testers or tacticians attempt to open mission files, which can lead to rejection of your battle.

All X-wing vs. TIE Fighter battles must meet the following standards:

1. <b><u>readme.txt</u></b>
   every battle must contain this file, with basic information about the battle and its author. It must also contain the disclaimer that is required by LucasArts with ever custom mission (see the bottom of this document). You can find a detailed list of the information expected in a readme file below;
2. <b><u>patches.txt</u></b>
   you can include information about required patches (including the EHSP) inside a dedicated file called patches.txt.
   As the information is required in the readme file, use of this file is optional;
3. <b><u>number of missions</u></b>
   every custom battle must consist of at least 4 missions;
4. <b><u>.LST file</u></b>
   every battle must contain a IMPERIAL.LST file which is required to play the battle;
5. <b><u>briefing and mission introduction text</u></b>
   an animated briefing and a mission introduction text must be provided for all missions. The briefing and mission introduction text must supply the pilot with enough information to achieve the mission goals;
6. <b><u>custom goal descriptions</u></b>
   missions are required to have a 'goals to be completed' and 'goals failed' descriptions for all mission goals (with the exception of the bonus goals) so a pilot will know what to do. All goals (including the bonus goals) must also have custom 'goals completed' messages;
7. <b><u>radio messages</u></b>
   all missions must utilize the in flight radio messages;
8. <b><u>custom sounds</u></b>
   when included, custom sounds must be recorded at 11kHz, 8-bit mono quality or less, and included in the .WAV format;
9. <b><u>Balance of Power</u></b>
   XvT battles and missions may not require the Balance of Power addon. If you use ships that are only available in the balance of Power addon, submit your battle as a Balance of Power battle;
10. <b><u>updates.txt</u></b>
    this is an optional file, used by Tactical Staff, to list updates made to the battle during the beta testing and correction process.

## Balance of Power Battle standards

Balance of Power is add-on to X-Wing vs. TIE Fighter. It upgrades to version 2.0, along with many other good improvements. Ships in BoP can be larger and the graphics engine is more advanced. Although it's very hard to get Balance of Power these days, it has become very popular among EH mission designers.

BoP requirements for battles and free missions are the same as the XvT requirements, with the only difference that the missions must require the Balance of power addon.

## X-wing Alliance Battle standards

Although it is few years old, X-Wing Alliance is the newest EH flying simulation. However, with the lack of new games from LucasArts, X-Wing Alliance is the most popular and the most advanced gaming platform of the Emperor's Hammer.

X-Wing Alliance engine offers mission designer the most advanced abilities, not encountered in previous games. Using dynamic goals, four regions and a very high craft limit, it is possible to create outstanding missions with very realistic areas. Also, the craft palette is very large and offers many ships, much more than previous games from the series.

The official mission creation program for X-wing Alliance is AlliED by Troy Dangerfield. While mission creators are not obliged to use this utility, the use of other tools can create problems when testers or tacticians attempt to open mission files, which can lead to rejection of your battle.

All X-wing Alliance battles must meet the following standards:

1. <b><u>readme.txt</u></b>
   every battle must contain this file, with basic information about the battle and its author. It must also contain the disclaimer that is required by LucasArts with ever custom mission (see the bottom of this document). You can find a detailed list of the information expected in a readme file below;
2. <b><u>patches.txt</u></b>
   you can include information about required patches (including the EHSP) inside a dedicated file called patches.txt. As the information is required in the readme file, use of this file is optional;
3. <b><u>number of missions</u></b>
   every custom battle must consist of at least 4 missions. Custom battles can have up to 20 missions;;
4. <b><u>.LST file</u></b>
   every battle must contain a IMPERIAL.LST file which is required to play the battle;
5. <b><u>mission file naming standard</u></b>
   contrary to other games, XWA requires you to name your mission files after a certain standard, or the game will not be able to play them. All mission files must be named 1B8M#---.TIE, where '#' is the mission number, and '---' is extra text (optional);
6. <b><u>briefings and hints</u></b>
   an animated briefing must be provided for all missions. XWA does not give additional briefings, so the animated briefing must supply the pilot with enough information to achieve the mission goals. Providing good mission hints and debriefings is highly encouraged.
7. <b><u>custom goal descriptions</u></b>
   missions are required to have a 'goals to be completed' and 'goals failed' descriptions for all mission goals (with the exception of the bonus goals) so a pilot will know what to do. All goals (including the bonus goals) must also have custom 'goals completed' messages;
8. <b><u>radio messages</u></b>
   all missions must utilize the in flight radio messages;
9. <b><u>custom sounds</u></b>
   when included, custom sounds must be recorded at 11kHz, 8-bit mono quality or less, and included in the .WAV format;
10. <b><u>updates.txt</u></b>
    this is an optional file, used by Tactical Staff, to list updates made to the battle during the beta testing and correction process.

## Empire at War Battle standards

Empire at War is the latest addition to the Compendium. Rules are still somewhat lax and the TAC Office is still learning the mighty power of such a game. Currently the TAC office is accepting only single Skirmish maps (both Space and Land) with plans to add Galactic Conquests in the future.

All Empire at War maps must meet the following standards:

1. <b><u>readme.txt</u></b>
   every battle must contain this file, with basic information about the battle and its author, and installation instructions. It must also contain the disclaimer that is required by LucasArts with ever custom mission (see the bottom of this document).
2. <b><u>planet.txt</u></b>
   here you write some information about the planet in which the battle will orbit/be on. this doesn't have to be long, just two paragraphs as it is designed to replace the Alderaan loading screen if we ever figure out how;
3. <b><u>X.ted mission format.</u></b>
4. <b><u>1 map only</u></b>
5. <b><u>EaW Compatible</u></b>
   the map must be playable on Empire at War without the FoC expansion;

## Readme files

A readme file for a custom battle or mission for any of the flying platforms should follow the following standard. Readme files that do not follow it may be changed by the TAC Office to meet them.

Readme files must include the following information: name of the battle, name of author and PIN, ID line of author (wihtout medal and IWATS information), flying platform, number of missions, medal name, patch information, a description of the battle and disclaimer. Additionally, you can add extra information such as a plotline, notes, etc. Plot information and such can be provided in plain text, but you may also use other formats such as Word, HTML or .PDF.

You can download a <a href="files/readme.txt" target="_BLANK">sample file here</a>

## Subgroup battles

By default, most missions will be made for the TIE Corps, and as such become TC battles. Sometimes however, your battle may be more fitting in another subgroup. For instance if you have a 7 mission battle in wich the pilot flies only rebel fighters, you might consider making it an Infiltrator Wing (IW) battle. In the past, subgroup battles required the permission of the subgroup commander. This is no longer necessary. The only exceptions are:

1. <b><u>Combined Arms Battles (CAB)</u></b>
   These are large battles, usually involving more than one subgroup. These can only be created after prior approval by the Tactical Officer. To get approval, e-mail a detailed plotline with all needed information to the Tactical Officer.
2. <b><u>Fleet Commander's Honor Guard (FCHG)</u></b>:
   Fleet Commander's Honor Guard battles are selected battles which have won a FCHG mission creation competition. These competitions can be held by the Tactical Office for the Fleet Commander's and Emperor's Hammer's birthday. These battles must follow high quality standards and must have a plotline following the general Emperor's Hammer timeline. The Tactical Officer has final say on the selection of these FCHG Battles.

## Platform conversions

Platform Conversions battles are created by converting an existing battle on the Mission Compendium to be played on another platform than it was originally designed for. These should only be done by the original creator of the battle and are generally discouraged because of the problems arising when doing a conversion.

Problems arise because the AI is different, as well as the available craft. briefings almost never convert properly. Any platform conversion MUST be approved by the Tactical Officer before you begin.

## Medals

Battle Medals are awarded to pilots who complete EH Battles on the Mission Compendium. The Battle Medal is awarded after the pilot's superior has processed the pilot file. The battle author suggests the name of the Battle Medal that is to be awarded upon completing their battle. New Battle Medal names cannot conflict with existing medal names. The Tactical Officer approves and has final say on new Battle Medal names.

Battle Medals are listed on the Mission Compendium and on a pilot's Combat Profile. Battle Medals are not displayed on uniforms, only as campaign stars. You get a Campaign Star for each 100 missions you fly.

## LucasArts new levels rules

LucasArts has devised a set of rules to which all custom battles and missions must adhere. These rules are absolete and no deviation is allowed. When there is doubt whether a EH rule conflicts with a LucasArts rule, the LucasArts rule applies. The LucasArts rules are:

1. New Levels must work only with the retail version of the Software, and may not work with any demo or OEM versions of the Software;
2. New Levels may not modify COM, EXE, DLL or other executable files;
   (as such, the Emperor's Hammer will not distribute custom .COM, .EXE, or .DLL files bundled with its missions. The Emperor's Hammer does not and will not distribute modified LucasArts .COM, .EXE, or .DLL files, but does distribute patch programs for these files);
3. New Levels must not contain any illegal, scandalous, illicit, defamatory, libelous, or objectionable material (as may be determined by LEC in its sole discretion), or any material that infringes any trademarks, copyrights, protected works, publicity, proprietary, or other rights of any third party or of LEC;
   (as such, the Emperor's Hammer also enforces that all new missions and battles must comply with the Articles of War and Emperor's Hammer Bylaws.)
4. New Levels may not include any LEC sound effects or music files or portions thereof;
5. New Levels must identify in every description file, on-line description, readme, and in comments in the New Level code:
   (a) the name, address, and an e-mail address of the level's creators, and
   (b) the following disclaimer:
   <b>THIS LEVEL IS NOT MADE, DISTRIBUTED, OR SUPPORTED BY LUCASARTS ENTERTAINMENT COMPANY, ELEMENTS TM & (C) LUCASARTS ENTERTAINMENT COMPANY.";</b>
   (despite what this says, you are NOT required or asked to put your real name or home address in custom missions! In fact, we discourage this practice, especially if you're under the age of 18. Only your Emperor's Hammer ID Line and e-mail address to identify you as the creator are required. The disclaimer is required as well.)
6. New Levels may not be sold, bartered, or distributed with any other product for which any charge is made (other than incidental charges for time spent online), but rather must be distributed free of charge;
7. By distributing or permitting the distribution of any New Levels, all creators or owners of any trademark, copyright, or other right, title or interest therein grant to LEC an irrevocable, perpetual, royalty-free, sub licensable right to distribute the New Level by any means (whether now known or hereafter invented) derivative works thereof, and to charge for the distribution of such New Level or derivative work, with no obligation to account to any of the creators or owners of the New Level in any manner.
