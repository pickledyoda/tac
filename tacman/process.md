# From submission to release

In the early years of the Emperor's Hammer, all submitted battles were immediately released by adding them to the EH Newsletter. Mission creation was very poor and often the only way to do it was to use a hex editing tool. However, with the evolution of the EH and creation of the Mission Compendium, the Tactical Office had to develop better ways to check new submissions and eventually correct them. That's how the beta testing process was initiated. Unfortunately, many of the older battles still suffer from design flaws that would have not allowed them to be released today.

## Beta testing procedure

When you submit your battle to the Tactical Officer, it will be initially reviewed to see that the basic standards have been met. This means all the required files and information must be present. If the battle is missing either, the Tactical Officer will notify you of the problems and require you to resubmit the battle again once the problems have been fixed. Make sure to follow the submission procedures or your submission will be simply rejected.

If the first check passes, the battle will be added to the database. Battles are processed in the order they are submitted. When a battle's turn is up, a Tactical Surveyors (TCS) will be assigned to test-fly the battle and submit a report on problems experienced. usually, a TCS is assigned for each of the three difficulty settings. Such an assignment can vary in length, but could usually goes between one and three weeks.

Once the play testing has been done, all beta test reports go to the Tactical Officer who puts the reports together. At this point, the Tactical Officer will assign the battle to a Tactician (TCT) to make corrections on the problems the Tactical Surveyors find. In order to do so, the TCT will receive all three beta test reports. This will take another one to two weeks.

Based on the number and complexity of problems encountered, a battle may be rejected without corrections. The TCTs will make minor corrections to a battle, but cannot be expected to fix major problems. If a battle is rejected because of major problems, the author will be informed of the decision, and also given specifc reason on why the battle was rejected. The author may then decide to address these problems, after which he can resubmit the battle.

Once the battle has been corrected, the Tactical Officer will perform a final check by flying the battle to make sure all the reported problems were fixed and that the battle plays properly. Finally, the completed battle will become official, added to the Mission Compendium, and the author will receive the appropriate award(s) for creating the battle by the Tactical Officer and the Tactical Staff will receive credits for playtesting (battle added to their combat profile) and Medals of Tactics for correction.

## TAC staff regulations

There are several regulations in place that serve as rules and guidelines for all testers and engineers.

### Tactical Surveyor (TCS)

- When new battles are submitted, you will receive a battle to test. You are also provided with the difficulty level on which you are to test the battle. Unless noted otherwise , you will have two weeks to complete your testing. Once tested, you can submit your beta test report into the database via the TAC Office website.
- If for any reason you cannot test it for the deadline, you must notify the Tactical Officer or you will be marked with an unexcused miss. Several unexcused misses may get you removed from the TAC Office staff.
- Tactical Surveyors must have passed the 'Tactical Mission Creation and Beta Testing Standards' (MCBS) course at the Imperial Academy.
- Tactical Surveyors are not required to pass the appropriate mission creation course, but it is strongly recommended, as it gives a better knowledge of the way a battle works, and will improve the quality and precision of your beta reports.
- Play on your assigned difficulty level. If needed, you are allowed to use "cheats" like invulnerability, unlimited ammunition or unlimited waves, but mention this in your report. If you are still unable to complete, switch to a lower difficulty. Be sure to mention this in your report. If you are still unable to complete a mission on the Easy setting, skip the mission. Again, make a note of this in your report.
- Make sure you also lose the mission (quit the game right after starting) to view the failed mission debriefings.
- In TIE Fighter, try to complete the secondary goals as well, so you can see the goals completed debriefing (when applicable).
- If you encounter problems that make the battle unplayable (corrupt archive, unreadable battle file or mission files, etc), inform the Tactical Officer immediately. In case of a corrupt battle file, you can create a battle file yourself and continue testing, but be sure to report it.
- If the battle you play is added to the Mission Compendium, you will earn FCHG credit for completing it, and the battle will be added to your Combat Record, but you may not use your Beta Testing pilot file to turn in a high score. You must play the official distribution version of the battle to achieve a high score.
- Make sure you are testflying the battle on an unmodified version of the game (with the exception of official LucasArts fixes, the EHSP and any battle specific EH patches).
- Do not play battles that have been rejected! If the Tactical Officer sends a notice saying a battle has been prematurely rejected for a reason, you may not continue testing it.
- Grammatical errors and misspellings should be noted on reports. The same goes for text running off the screen. Be sure to be specific when reporting these types of errors.
- Check for appropriate content. This includes checking for anything that might violate EH Bylaws or EH Articles of War, or if you feel in some way that the battle is not Emperor's Hammer related. Battles should be suitable for pilots of all ages. If you feel something is offensive or inappropriate, state it in your report as to why. The
  Tactical Officer will make a final judgment call on the appropriateness after viewing the reports. This includes to text where the author has self-censored his text by bleeping out letters. please note that racial slurs common in the Star Wars universe are allowed!
- Be sure your report comments to say something about the mission (and particularly, something specific you liked/disliked about it).
- Excessive wait times are classified as bugs and should be reported as such. You may choose to skip the mission if excessive waiting is involved.
- If you are accidentally assigned to test a battle that you made yourself, notify the Tactical Officer so it can be reassigned.

### Tactician (TCT)

- When new battles have been tested, you will be assigned to correct one. For this, you will be given the battle, along with a set of reports from the Tactical Surveyors. You may have from one to three weeks to correct the battle.
- If for any reason you cannot fix the battle for the deadline or need to go on leave for any reason, you must notify the Tactical Officer or you will be marked with an unexcused miss. Several unexcused misses may get you removed from the TAC Office staff.
- Tacticians must have passed the 'Tactical Mission Creation and Beta Testing Standards' (MCBS) course at the Imperial Academy.
- Tacticians are required to pass the appropriate mission creation course from IWATS.
- Document all changes you make to the battle in the update.txt file with a date stamp and your name:
- Add difficulty levels or reinforcement Flight Groups if necessary if complaints of the battle being too difficult are present. Be very cautious that you don't assign a mission-critical or plot-critical ship to a difficulty level that makes the battle unplayable or disrupts the plotline.
- Playing with unlimited ammo and invulnerability is permitted to speed up time with testing corrections.
- Common sense is required for this job. Use it! Don't blindly work on every bug reported by testers. However, take in any suggestions made to make the battle better.
- Bad quality radio messages in .WAV format do not have to be re-recorded by Tacticians.
- The following are commonly reported bugs that need to be corrected:
  - spelling and grammar mistakes in briefings, descriptions, radio messages,
  - Secret Order Officer present without any questions or without secondary goals,
  - blank spaces in briefings,
  - capital ships with starfighter orders,
  - impossible difficulty on Easy mode, lack of difficulty settings,
  - long wait times,
  - unbeatable primary mission goals (unbeatable secondary and bonus goals are acceptable)

Note: in the past, the TAC Office also had Tactical Coordinators and Tactical Head Coordinators. these
positions are currently no longer in service. They can be brought back if need be.

## Applications

When new openings are available for TAC Office positions, the Tactical Officer will announce open applications on TIE Corps News, the TAC Office reports, the TAC Office website, including how to apply.

Those who have some skills in mission creation and passed the appropriate mission creation courses have a significantly better chance of becoming testers, even if they apply for a Tactical Surveyor position. All Tacticians are required to pass the appropriate mission creation course from IWATS, Tactical Surveyors are strongly recommended to do so.

Beta Testing is a huge commitment, and the rewards of beta testing are great. Merit medals are awarded depending on how many battles pilots has tested or corrected, FCHG points are awarded for test-flying once the battles are officially released, and Medals of Tactics are handled for bug fixes.

At any point can you submit an open application to the Tactical Officer, but you are not guaranteed a position.
