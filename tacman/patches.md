# Updates and Patches

Games used by Emperor's Hammer and its Subgroups are not perfect. While the often offer a very good graphic engine, they usually lack in number of built-in vessels. That is why the Science Office provides us with a large assortment of craft to use outside of that the games themselves offer us.

A patch is a small piece of software that updates the EH official platform by adding a new ship or fighter. In order to avoid troubles with international laws and copyrights only EH approved patches (those you can download from EH Science Office webpage) can be used in EH approved battles. Using a third source patch, not approved by the Science Office will result in submission rejection.

There are two kinds of patches in the archives. Normal patches use common slots (in other words many patches use the same slot) and they should be installed only in order to complete the battle/mission in question. Those patches should be uninstalled when they are not needed.

The second kind is the Ship Patch. Those patches do not have to be uninstalled and they can be kept permanently installed.

There are also game updates released by both the Science Office and LucasArts. Those updates are required and should be applied to the games and kept installed permanently (they usually cannot be uninstalled).

## Installing patches

To install a patch, download the patch for the appropriate platform from the Science Office website. Thebattle's page in the Mission Compendium provides you with a direct link to the .ZIP file.

Once downloaded, extract the archive. this will create a directory called EHPatch. You must copy this directory directly to your game directory.

Next, start the Super XP Installer provided by the Science Office, and click on the button that matches the game which you are installing a patch for. This will display a list of all the .BAT files found for that game. For each patch, you will find two batch files: one to install, and one to remove. Click the .BAT file of the patch you wish to install, and click the 'Run' button. Repeat this process if you need to install more than one patch.

- You can find the `XP Installer` <a href="http://sco.emperorshammer.net/files/XPInstaller.zip">here</a>
- You can find the `Super XP Installer` <a href="http://sco.emperorshammer.net/files/superxpinstaller.zip">here</a>

To uninstall a patch, you follow the same procedure, only you select the .BAT file for uninstalling the patch.

### IMPORTANT

When you have installed more than one patch, it is very important to uninstall them in the <b>reverse order</b> that you installed them. So if you installed the TIE Dragon first, and then the DGN Lichtor V, you must uninstall the DGN Lichtor V first, and then the TIE Dragon. This is to ensure the backups that are restored will be restored properly.

From time to time, it is adviseable to reinstall your game.

## Detailed information

Each platform has its own patches and rules concerning them. While basic rules apply to all platforms, there may be differences between different platforms.

### TIE Fighter

TIE Fighter comes in many version from the older disk version to the newest one, which is XvT based. The only official EH platform is TIE95 and all EH battles and mission are designed for this platform. However some of the battles and missions may work with the older version, especially TIECD.

The Science Office has released one permanent patch for TIE95, called EH Ship Patch (EHSP-TIE). This patch adds additional craft to the game, making it more playable.

### X-Wing vs. TIE Fighter

LucasArts has released one update to this game, adding 3D acceleration support and fixing minor bugs. It is required to apply this patch to the game (users of BoP have it automatically applied) in order to use both single player and multi player battles.

Since XvT has the least craft of all LucasArts simulation games, the Science Office has released a Ship Patch (EHSP-XVT) for this game as well.

### Balance of Power

Balance of Power itself is a mission pack to XvT. It upgrades it to version 2.0 and adds some changes to the engine.

The Science Office has released a Ship Patch (EHSOP-BOP) for this game as well, adding missing craft.

### X-Wing Alliance

X-Wing Alliance has one LucasArts patch fixing some bugs. It is required to install it in order to play SP and MP missions. There is no Ship Patch available for this game.
