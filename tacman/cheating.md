# Cheating

Each club has its good and its bad sides, and the EH is no different. Like in any club where pilots compete, there is certain amount of black sheep pilots, who try to get an unfair advantage over the others. Those who try to get an unfair advantage in missions and battles are called cheaters. It is one of the Tactical Officer's core duties to hunt them down and bring them to justice.

Thankfully, the vast majority of pilots in the TIE Corps are honorable and would not stoop to cheating to try and get better results in competitions. Nevertheless, there are the few who will attempt such actions and the TIE Corps has a very strict policy on it. Review this policy carefully so you are aware what is and isn't acceptable.

## Invulnerability, unlimited ammo and unlimited waves

Using the Invulnerability, Unlimited Weapons, or Unlimited Waves option in your game is NOT considered cheating. Keep in mind that if you do use either, your score is automatically deducted 90% by the game, and in X-Wing vs. TIE Fighter, it will simply give you a score of 0 for the mission flown. In X-Wing Alliance, Unlimited Weapons doesn't reduce your score.

## Randomize

X-Wing vs. TIE Fighter, Balance of Power and X-Wing Alliance allow playing random missions. However turning randomization on will change the parameters of the mission, changing ships and fighters, what causes different scores. Some pilots may try to use randomization in order to gain an unfair advantage over the others.

Tactical Office rules do not allow pilot files with random option turned on to be submitted for approval. While single submission by mistake is not considered cheating, multiple submissions from pilot who is aware of this rule, will be considered cheating and will be punished accordingly.

## Trainers

Using a game trainer, an external program that works with the game and that would give you an unfair advantage, is strictly prohibited.

## Editing missions

Editing official battles and missions in any way is strictly forbidden. Do not use any mission editor or hex-editor to modify any part of a mission that you download from the Mission Compendium. If the battle or free mission that you are playing has a mission critical bug in it that will not allow you to proceed in the game, do not change the mission to fix it. Even if your intentions are good, you may accidentally modify the mission in a way that it allows you to achieve a higher score, thus giving you an unfair advantage over the other pilots. Instead, file a Bug Report on the battle or mission. In the meantime, the Mission Compendium is home to over 4000 missions that you can fly.

## Skipping missions

You are not allowed to skip a mission in a battle whatever the reason is. All missions of a battle must be completed. You are not to use the in-game 'skipping mission' option of X-Wing Alliance. If you encounter a battle that has a fatal bug in it that prevents you from playing the rest of the battle, you cannot edit your pilot file to skip the mission. If you encounter a fatal bug, fill out a Bug Report on the Battle Center to explain why you could not finish a mission, and save a backup of your pilot file so you can finish the battle after it has been repaired.

Skipping missions may lead to have the battle removed from your combat profile.

## Pilot file editing

Some pilots may try to use a pilot editor or hex-editor to add points, kills or captures to their pilot file. Editing pilot files under any circumstances is strictly forbidden. There is no reason to ever edit a pilot file except to skip a mission. However, as stated before, skipping missions is not allowed and may result in removal of the entry from your combat record.

## Patches

Some of the battles and missions in the Mission Compendium require you to patch your game to add a new custom starfighter for you to fly. You are NOT required to patch your game. Most missions will continue to work without the patch, though the difficulty could be significantly increased as a result. If the mission is impossible to win because the patch is required to play it, then you will have to skip playing the mission. If the mission is playable, it is not considered cheating to play the mission through with an unpatched game.

You should immediately remove the starfighter patch to return your gaming platform to its original state as soon as you complete the custom mission that requires it. Ship patches can change your craft's speed, shields and weapons, making it possible to be almost totally invulnerable, giving you an unfair advantage in missions that don't use the patch. In addition to this, they can change other ships too, rendering some missions impossible to complete.

This is why it's important that you always remove any starfighter patches from your games after you complete the battle the patch was used for. If you have accidentally played and completed a battle or free mission using patched starfighters and have submitted it to your superior officer for review, you should immediately contact the Tactical Office directly and CC your superior officer, explaining that the pilot file you sent should be ignored until you can re-fly it using a non-modified platform.

This rule applies to starfighter patches only. This does NOT apply to game platform updates such as the TIE95 ISD Laser Patch or the XWA S-Foils patch.

## Shooting your own ships

Contrary to some pilots' beliefs, it is NOT forbidden to shoot at your own friendly ships in a mission as a tactic to get higher points.

## Killing your own ships

While shooting at your own ships is not a forbidden practice, killing your own ships is considered cheating by the Tactical Office. While you do lose 10,000 points in TIE Fighter when you destroy a friendly craft, it is possible in some missions (that have many Star Destroyers or other large capital ships) to offset this deduction and gain a large amount of points. This applies to Imperial craft only, not to Neutral or Rebel ships.

Accidentally killing a wingman or two that got in the way of your laser cannons is tolerated. "Accidentally" killing a friendly Victory-Class Star Destroyer is not.

## Replaying missions

If you want to achieve a higher score, you must replay the free mission or the entire battle on a fresh new pilot file. You cannot simply play only one mission of a battle and turn it in for a high score. Combat Chamber scores and High Score (.HGH) files will not be accepted or acknowledged.

The exception to the rule applies to Macintosh users only for TIE Fighter missions. If the only way to get the battle working is by playing it in the Combat Chamber, in which case the pilot is not eligible for high scores, but can still receive credit and FCHG points for completing the battle.

Another exception concerns the X-wing vs. TIE Fighter platform: as you can fly missions in any order, you can replay a single mission to achieve a higher score. Anyway, ALL missions of the battle must be completed on the pilot file submitted.

## Sharing pilot files

You may not share pilot files between fellow pilots and allow another person to use your pilot file to receive credit for completing a battle or free mission. If pilot sharing is discovered, both parties involved are considered cheating. This includes pilot file sharing with yourself via a Flight Office approved clone.

## Beta testing pilot files

Beta Testers may not turn in pilot files for high scores or for competitions of battles they have beta tested. Since beta tested battles go to the Tactical Officer for final review, the Tactical Officer could have made changes to the battle that could affect scoring. Beta Testers should delete their pilot files after playing the battle since only their Beta Test Reviews are necessary to receive credit for completing the battle.

## X-wing Alliance upgrade

You are not allowed top upgrade your game with the XWAU available from xwaupgrade.com. This upgrade makes considerable changes to the game which could give you an unfair advantage over other pilots.

## Consequences of cheating

The Tactical Office has a zero tolerance policy for cheaters. If a pilot is caught cheating, the pilot (and all of the pilot's registered clones) will lose all of his high scores and FCHG points and will not be allowed to submit new high scores. His combat record will be erased, and the files related to the pilot's past record declared invalid for resubmission. The pilot will also be court-martialed by the High Court of Inquisitors and could face the mandatory sentence of a demotion to Sub-Lieutenant, loss of command position (if any), and loss of all medals. Repeat offenders may face expulsion from the Emperor's Hammer.
