# Fleet Commander's Honor Guard

The Fleet Commander's Honor Guard is a special ranking system that rates pilots and officers in the TIE Corps by combat activity and performance using a point system. These points then determine a pilot's place in the Honor Guard. For combat activity, points are awarded for flying and completing missions or winning online combat engagements. For performance, pilots compete against each other for points for holding high scores for missions and battles, adding a more competitive element for pilots trying to hold a higher rank.

The FCHG knows the following ranking system, based on the number of points awarded:

  <table width="500">
    <tr>
      <td width="200"><img src="images/fchg/fchggren.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">GRENADIER (GREN)<br>10 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchglanc.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">LANCER (LANC)<br>25 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchghuss.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">HUSSAR (HUSS)<br>50 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgfusl.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">FUSILIER (FUSL)<br>75 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgdrag.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">DRAGOON (DRAG)<br>100 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgcavl.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">CAVALIER (CAVL)<br>150 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchggall.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">GALLANT (GALL)<br>200 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgkngt.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">KNIGHT (KNGT)<br>250 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgpldn.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">PALADIN (PLDN)<br>300 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchglgnr.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">LEGIONNAIRE (LGNR)<br>400 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgaqfr.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">AQUILIFER (AQFR)<br>500 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgdcrn.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">DECURION (DCRN)<br>750 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgtsrs.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">TESSERARIUS (TSRS)<br>1,000 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgopti.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">OPTIO (OPTI)<br>1,500 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgcntr.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">CENTURION (CNTR)<br>2,000 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgexcr.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">EXECUTOR (EXCR)<br>2,500 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchggldr.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">GLADIATOR (GLDR)<br>3,000 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgarcn.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">ARCHON (ARCN)<br>4,000 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgtmpr.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">TEMPLAR (TMPR)<br>5,000 points needed
      </td>
    </tr>
    <tr>
      <td width="200"><img src="images/fchg/fchgimpr.jpg"></td>
      <td width="300" class="text" valign="top">
        <font class="newstitle">IMPERATOR (IMPR)<br>6,000 points needed
      </td>
    </tr>
  </table>

Points can be awarded for the following flight activity:

- every mission flown (1 point)
- every Iron Star with Bronze Wings earned (1)
- every Iron Star with Silver Wings earned (3)
- every Iron Star with Gold Wings earned (5)
- every Iron Star with Platinum Wings earned (10)
- every mission high score (2)
- every battle high score (2 points for every mission)
