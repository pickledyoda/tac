<?php
$title = "Emperor's Hammer Tactical Office";
require_once('includes/header.php');
require_once("includes/bootstrap.php");
?>
<div>
  <p>Welcome to the website of the Emperor's Hammer's Tactical Center.</p>
  <p>The Tactical Center is the home of the Tactical Officer and his staff, whose primary responsibility is to maintain
    the Mission Compendium and High Scores. The Tactical Center serves as platform for the entire process that
    leads to new battles being released onto the Mission Compendium.</p>
</div>
<?php
Counter::load();
?>
<p>Currently the Mission Compendium holds <?php echo Counter::$battleTotal; ?> released battles with a total of <?php echo Counter::$missionTotal; ?>
  missions.</p>
<table class="table table-striped w-50 mx-auto">
  <thead class="thead-dark">
    <tr>
      <th>Game</th>
      <th>Battles</th>
      <th>Missions</th>
    </tr>
  </thead>
  <?php
  foreach (Counter::$all as $counter) {
    echo "<tr><td>{$counter->label()}</td><td>{$counter->battleCount}</td><td>{$counter->missionCount}</td></tr>";
  }
  ?>
</table>

<p>
  If you have any questions/suggestions, feel free to e-mail the Tactical Officer at any time. That's
  what he's there for !
</p>

<i><?php echo getTACLabel(); ?></i>
<br>
<p>
  <small>TAC/AD Pickled Yoda/CS-6/VSDII Sinister</small>
</p>
<p>
  <small>IC/GOE/GSx5/SSx12/BSx13/PCx13/ISMx21/IS-3PW-27GW-109SW-179BW-3PR-31GR-65SR-93BR/MoI/
    MoT-6rh/LoC-RS-TS-IS-CSx4-Rx2/LoS-PS-RS-TS-IS-CSx4/DFC-SW-Rx2/MoC-7doc-6poc-7goc-6soc-
    54boc/CoLx11/CoE/CoSx2/CoB/LoAx45/OV-15E [IMPR] [Veteran 4th] [Ranger 3rd] {IWATS-AIM-
    AMP-ASP-BX-CBX-CSS-CTW-DW-FLA-FW-FWT-FZ-GFX-HIST-IBX-ICQ-IIC/1/2/3-JS-LIN-M/1/2-MCBS-
    MP/1/2-MS-PHP-RT-SFW-SGBMC-SM/3/4-SWGB-TLN-TM/1/3-TT-VBS-WIKI-WM-WPN-XAM-XMD-XML-XTM/
    1-XTT-XWAI}
  </small>
</p>
<?php include_once('includes/footer.php'); ?>