<?php

/*  login info  */

$dbname     = "emperors_tacc";
$dbhost     = "mysql";
$dbusername = "emperors_tacc";
$dbpassword = "5TAC5SWS";

/*  TAC officer positions  */

$tac     = ["", "TAC", "CA:TAC", "TACA", "TCT", "TCS", "TCT-TCS", "RSV", "ADMIN", "NEW"];
$tacfull = ["", "Tactical officer (TAC)", "Command Attach� (CA:TAC)", "Command Assistant (TACA)", "Tactician (TCT)", "Tactical Surveyor (TCS)", "Tactician-Surveyor (TCT-TCS)", "Reservist (RSV)", "ADMIN", "New member"];

/*  TC ranks  */

$ranks     = ["", "GA", "SA", "HA", "FA", "AD", "VA", "RA", "GN", "COL", "LC", "MAJ", "CPT", "CM", "LCM", "LT", "SL", "CT"];
$ranksfull = ["", "Grand Admiral", "Sector Admiral", "High Admiral", "Fleet Admiral", "Admiral", "Vice Admiral", "Rear Admiral", "General", "Colonel", "Lieutenant Colonel", "Major", "Captain", "Commander", "Lieutenant Commander", "Lieutenant", "Sub Lieutenant", "Cadet"];

/*  report status  */

$repstats = ["", "submitted", "accepted", "rejected", "corrected"];

/*  battle status  */

$batstat = ["", "submitted to TAC", "initial check", "queued", "under testing", "under correction", "final check", "rejected", "accepted", "released"];

/*  flying platforms  */

$flyplat = ["", "TIE", "XvT", "BoP", "XWA", "XW", "JA", "SWGB"];

/*  turn rank/position into integer  */

if (!function_exists('setRank')) {
	function setRank($stuff) {
		switch (trim($stuff)) {
			case "Grand Admiral":
				$rankno = 1;
				break;
			case "Sector Admiral":
				$rankno = 2;
				break;
			case "High Admiral":
				$rankno = 3;
				break;
			case "Fleet Admiral":
				$rankno = 4;
				break;
			case "Admiral":
				$rankno = 5;
				break;
			case "Vice Admiral":
				$rankno = 6;
				break;
			case "Rear Admiral":
				$rankno = 7;
				break;
			case "General":
				$rankno = 8;
				break;
			case "Colonel":
				$rankno = 9;
				break;
			case "Lieutenant Colonel":
				$rankno = 10;
				break;
			case "Major":
				$rankno = 11;
				break;
			case "Captain":
				$rankno = 12;
				break;
			case "Commander":
				$rankno = 13;
				break;
			case "Lieutenant Commander":
				$rankno = 14;
				break;
			case "Lieutenant":
				$rankno = 15;
				break;
			case "Sub-Lieutenant":
				$rankno = 16;
				break;
			case "Cadet":
				$rankno = 17;
				break;
			default:
				$rankno = 55;
				break;
		}
		return $rankno;
	}
}

if (!function_exists('setPosition')) {
	function setPosition($stuff) {
		switch ($stuff) {
			case "Tactical Officer (TAC)":
				$positionno = 1;
				break;
			case "Command Attach� (CA:TAC)":
				$positionno = 2;
				break;
			case "Command Assistant (TACA)":
				$positionno = 3;
				break;
			case "Tactical Surveyor (TCS)":
				$positionno = 5;
				break;
			case "Tactician (TCT)":
				$positionno = 4;
				break;
			case "Tactician/Surveyor (TCT-TCS)":
				$positionno = 6;
				break;
			case "Reserves (RSV)":
				$positionno = 7;
				break;
			case "Admin":
				$positionno = 8;
				break;
		}
		return $positionno;
	}
}

if (!function_exists('setPlatforms')) {
	function setPlatforms($stuff) {
		$platforms = $stuff[0];
		$i         = 1;
		while ($stuff[$i]) {
			$platforms = $platforms . " " . $stuff[$i];
			$i++;
		}
		return $platforms;
	}
}

/*
table 1: TAC roster
table name: roster
=========================================
*/

$N_ID        = 0;  // unique TAC database ID number
$N_PIN       = 3;  // user's unique TIE Corps PIN - used for logging in
$N_Rank      = 7;  // user's current rank - needs to be manually maintained (1)
$N_Name      = 2;  // user's name (1)
$N_Position  = 1;  // user's position within the TAC office (2)
$N_Subgroup  = 4;  // user's group within the EH (3)
$N_Password  = 8;  // user's password in the database (4)
$N_EMail     = 5;  // user's e-mail address
$N_Points    = 6;  // user's beta points count
$N_Platforms = 9;  // platform the user can be assigned to (5)

/*
(1) if we can use the TC database, this column is not needed
(2) current options: 0-8 (TAC  TACA  CA:TAC  TCT  TCS TCT-TCS  RSV  ADMIN  NEW)
(3) current options:  TC  CS  --  is this needed?
(4) saved in encrypted format - cannot be retrieved, only reset
(5) comma separated value, XW,TIE,XVT.BOP,XWA,JA,BF.BFII,SWGB,EAW
*/

/*
table 2: submitted battle information
table name: battles
=========================================
*/

$B_ID         = 0;  // unique battle ID number
$B_Name       = 1;  // battle's name
$B_Type       = 2;  // battle type (1)
$B_Platform   = 3;  // playing platform (2)
$B_Medal      = 4;  // medal name (can be empty, always empty for free missions)
$B_Missions   = 5;  // number of missions
$B_File       = 6;  // filename
$B_Patches    = 7;  // needed patches, including EHSP
$B_Comments   = 8;  // comments provided by creator
$B_Author     = 9;  // battle creator
$B_PIN        = 10;  // creator's unique TIE Corps PIN
$B_EMail      = 11;  // creator's e-mail address
$D_Status     = 12;  // battle's status (3)
$D_TestEasy   = 13;  // PIN of TCS assigned to testfly on Easy
$D_TestMedium = 14;  // PIN of TCS assigned to testfly on Medium
$D_TestHard   = 15;  // PIN of TCS assigned to testfly on Hard
$D_Correct    = 16;  // PIN of TCT assigned to correct
$D_History    = 17;  // history of battle's progress through the queue, items semi-colon separated
$D_Corrected  = 18;   // filename of corrected version

/*
(1) options: TC, IW, DB, ID, BHG, CAB, FCHG, IS, HF, FMC, DIR, CD, MP, FREE
(2) options: XW, TIE,  XvT, BoP, XWA, JA, SWGB, IA, IAS, EaW, BF, BF2
(3) options: 1-9 for submitted to TAC, initial check, queued, under testing, under correction, final check, rejected, released

note: for rejected battles, reason will be disclosed in the 'history' field
*/

/*
table 3: beta reports information
table name: reports
=========================================
*/

$R_ID     = 0;  // unique report ID number
$R_Battle = 1;  // battle's ID (see table 2; B_ID)
$R_Level  = 2;  // level the test report applies to
$R_Tester = 3;  // tester's unique TIE Corps PIN
$R_Status = 4;    // beta report status (1)
$R_Report = 5;  // full report, sections separated by �, section title and content separated by �, content lines separated by �

/*
(1) options: (1) submitted by TCS     - available to TCS/TAC
             (2) approved by TAC      - available to TCT/TAC
             (3) rejected by TAC      - available to TCS/TAC
             (4) corrected by TCS     - available to TCT/TAC
    note: you can't assign a battle to the TCTs unless all reports for the battle have status 2 !
*/

/*
table 4: TAC news
table name: news
=========================================
*/

$S_ID       = 0;  // unique news item ID number
$S_Date     = 1;  // date item was added
$S_Time     = 2;  // time item headline will be visible
$S_Author   = 3;  // author of item
$S_Headline = 4;    // item headline
$S_Content  = 5;    // item itself
$S_Shown    = 6;  // list of people who have seen this item
$S_Public   = 7;    // display publicly?

/*
table 5: Battle/mission counter
table name: counter
=========================================
*/

$C_ID       = 0;  // unique news item ID number
$C_PLATFORM = 1;  // flying platform (1)
$C_BATTLES  = 2;  // battle count
$C_MISSIONS = 3;    // mission count

$DELIM = chr(164);

/*
(1) options: XW, TIE, XvT, BoP, XWA, JA, SWGB, IA, IAS, EaW, BF, BF2
*/

?>




