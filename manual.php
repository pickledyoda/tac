<?php
$title = "TAC Manual";
include_once('includes/header.php');

$pages = [
  ['compendium', 'Mission Compendium'],
  ['patches', 'Updates and patches'],
  ['fchg', 'Fleet Commander\'s Honor Guard'],
  ['playing', 'Playing custom battles'],
  ['submitting', 'Creating and submitting battles'],
  ['process', 'From submission to release'],
  ['bugs', 'Bugs and problems'],
  ['cheating', 'Cheating policy'],
  ['rewards', 'Rewards']
];

function md2html($md)
{
  return implode("\n", array_map(function ($line) {
    if (!$line) {
      return '';
    } else if (substr($line, 0, 2) === '# ') {
      $rest = substr($line, 2);
      return "<h1>{$rest}</h1>";
    } else if (substr($line, 0, 3) === '## ') {
      $rest = substr($line, 3);
      return "<h2>{$rest}</h2>";
    } else if (substr($line, 0, 4) === '### ') {
      $rest = substr($line, 4);
      return "<p><b>{$rest}</b></p>";
    } else {
      return "<p>$line</p>";
    }
  }, explode("\n", $md)));
}

?>
<p>This is version 4.1 of the Emperor's Hammer Tactical Manual, dated November 2019.</p>
<p>
  The Tactical Manual explains all there is to know about playing and creating missions for
  any of the game platforms in use by the Emperor's Hammer. It was originally written by
  Alan Kawolski and David Geiger, with updates by Julien Martinez, Marcin Szydlowski and
  Frederik Taillefer. In 2007, The manual was rewritten and upgraded by Anahorn Dempsey.
  In 2019, it had minor edits by Pickled Yoda</p>
<p>&nbsp;</p>
<div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills" id="man-nav" role="tablist" aria-orientation="vertical">
      <?php
      $first = ' active';
      foreach ($pages as $page) {
        list($id, $label) = $page;
        echo "<a class='nav-link text-light{$first}' id='{$id}-tab' data-toggle='pill' href='#{$id}' role='tab'>{$label}</a>";
        $first = '';
      }
      ?>
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="man-content">
      <?php
      $first = ' show active';
      foreach ($pages as $page) {
        list($id, $label) = $page;
        $file = "tacman/{$id}.md";

        $content =  file_exists($file)
          ? md2html(file_get_contents($file))
          : $label;
        echo "<div class='tab-pane fade{$first}' id='{$id}' role='tabpanel'>{$content}</div>";
        $first = '';
      }
      ?>
    </div>
  </div>
</div>
<?php include_once('includes/header.php'); ?>