<?php
$title = "TAC Battle Queue";
include_once('includes/header.php');
require_once("includes/bootstrap.php");
?>
<p>This is the current queue of battles that have been submitted to the TAC Office.</p>
<table class="table table-striped mx-auto">
  <thead class="thead-dark">
    <tr>
      <th>Name</th>
      <th>Type</th>
      <th>Status</th>
      <th>Submitted</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $battles = tacQueryAll("SELECT * FROM battles WHERE D_Status <> 7 AND D_STATUS <> 9 ORDER BY B_NAME ASC");
    foreach ($battles as $row) {
      $battle = new Battle($row);

      echo "<tr>
        <td>{$battle->name}</td>
        <td>{$battle->platform}-{$battle->type}</td>
        <td>{$battle->status()}</td>
        <td>{$battle->submissionDate()}</td>
      </tr>";
    }
    ?>
  </tbody>
</table>
<?php include_once('includes/footer.php'); ?>