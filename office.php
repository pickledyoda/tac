<?php
$title = "TAC Office";
include_once('includes/header.php');
require_once("includes/bootstrap.php");

$tacs = Roster::getByPosition(POS_TAC);
$tac = count($tacs) ? $tacs[0] : NULL;
$catacs = Roster::getByPosition(POS_CATAC);
$catac = count($catacs) ? $catacs[0] : NULL;
$taca = Roster::getByPosition(POS_TACA);

?>
<p>The Tactical Office is the branch of the Command Staff that deals with (among other things) the processing of new
  battles and free missions, the maintenance of the EH Mission Compendium, Battle Center and Tactical Manual, and the
  approval of High Scores. The Tactical Officer, who is third in command of the entire Emperor's Hammer, leads it. The
  Tactical Officer supervises and runs the Tactical Office. The TAC is supported by a competent staff with several testers
  and engineers.</p>

<h2>Tactical Officer (TAC)</h2>
<p><i><?php echo $tac ? "The current TAC is {$tac->label()}" : "There is currently no TAC assigned"; ?></i></p>
<p>The Tactical Officer is the head of the Tactical Office and. His duties range from everyday duties such as high score
  checking and maintaining the Tactical Database to long-range projects for the benefit of the Fleet.</p>
<p>The Tactical Officer is personally responsible for the approval of all new battles and free missions for the Mission
  Compendium. He also maintains the High Score Battle Board and the Fleet Commander Honor Guard system. Finally, the
  Tactical Officer is also responsible for making sure that all of the battles run smoothly - without bugs - and that all
  BSFs are approved.</p>
<p>The Tactical Officer watches over fair play in every major flying competition as an expert judge. He also judges main
  mission creation and other competitions including FCHG/CAB competitions, Golden Tug Awards and Mission Creation Wars.</p>
<p>The Tactical Officer is always working on new ideas and ways to make the EH Mission Compendium better and the mission
  creation process easier. He also works together with the Science Officer to adapt new gaming platforms for the use of
  Emperor's Hammer Strike Fleet.</p>
<p>The Tactical Officer holds the minimum rank of Vice Admiral, and is in a position of prestige and power as third in
  command of the Emperor's Hammer. Maximal rank Tactical Office can achieve is High Admiral.</p>

<h2>Command Attache to the Tactical Officer (CA:TAC)</h2>
<p><i>
    <?php
    if ($catac) {
      echo "The current CA:TAC is {$catac->label()}.";
    } else {
      echo "There currently is no CA:TAC assigned to the TAC Office.";
    }
    ?>
  </i></p>
<p>The Command Attache to the Tactical Officer is the primary advisor to the Tactical Officer. He supports the Tactical Officer
  in his usual duties, providing additional help as needed and acting for the Tactical Officer when he is unavailable. The
  Command Attache is always trained by the TAC to be ready to take over when TAC resigns or is on leave. This however does
  not mean that the CA:TAC will always take the position of tactical officer when the TAC does resign.</p>
<p>The specific duties of the CA:TAC will depend on the TAC's confidence and trust in his attache, the CA:TAC's abilities
  and skills and the needs of the Fleet.</p>
<p>Usually, the CA:TAC will also serve as Tactical Head Coordinator, providing an additional final check, and adding new
  battles to Tactical Database. He also supervises the Tactical Coordinators and beta testing staff.</p>
<p>The minimum rank for a CA:TAC is Vice Admiral, while Fleet Admiral is maximal.</p>

<h2>Command Assistant (TACA)</h2>
<p><i>
    <?php
    if ($taca) {
      $plural = count($taca) > 1 ? "s are " : " is ";
      $str = "The current TAC Assistant{$plural}";
      $str .= implode("; ", $taca);
      echo "{$str}.";
    } else {
      echo "There currently are no TAC Assistants assigned to the TAC Office.";
    }
    ?>
  </i></p>
<p>Every Command Officer can have up to four Command Assistants. Their job is to help their CO with different projects and
  tasks. They are usually hired to perform some specific project, while the Command Officer concentrates on more important
  assignments.</p>
<p>A Command Assistant to Tactical Officer can perform many different duties, from helping with competitions to web design.
  TACA is an auxiliary position, so a person becoming TACA does not have to resign from his current position while
  performing duties in Tactical Office. There is also a tradition that a retiring Tactical Officer takes an honorary
  assistant position to assist the new TAC with his knowledge and experience.</p>

<h2>Tactical Surveyor (TCS)</h2>
<p>Tactical Surveyor (or tester) is the most basic position within the Tactical Office, and is the entry-level position for
  the Tactical Office. A TCS is assigned to fly a battle or mission on particular difficulty level, searches for errors,
  and reports the ones he finds. These beta reports are uploaded to the Tactical Database, and are used by the Tacticians as
  a checklist of things to fix when they correct the mission.</p>
<p>Testers must have the rank of Lieutenant or higher, a reasonable FCHG rating, and be able to fly 2 missions or battles
  per week. A Tactical Surveyor must have passed the IWATS course 'Tactical Mission Creation and Beta Testing Standards'
  (MCBS). Having passed the appropriate mission creation course is a definite advantage, but not mandatory. They report
  directly to a Tactical Coordinator, and may work on more than one platform. The position of Tactical Surveyor is
  auxiliary.</p>

<h2>Tactician (TCT)</h2>
<p>Tacticians are in charge of making corrections to missions. Once they receive their assignments from the TCC, they read
  the testers' beta reports and implement the necessary changes. Once the corrections are completed and documented, the TCT
  posts his beta correction report to the Tactical Database. He then sends a copy of the report and the corrected mission
  file(s) to Tactical Coordinator.</p>
<p>The position of Tactician may be held by any member of the Emperor's Hammer, and is an auxiliary position. A person
  wishing to serve as a Tactician must have proven mission creation experience and familiarity with Tactical Office
  procedures. They must have passed the IWATS course 'Tactical Mission Creation and Beta Testing Standards' (MCBS) as well
  as the relevant mission creation course. The Tactical Officer may open positions of TCT for applications when additional
  staff is needed. Usually, new TCTs are chosen from among proven Surveyors.</p>
<p>A Tactician is expected to correct at least one battle (or free mission) per week.</p>

<h2>Tactical Coordinator (TCC)</h2>
<p><i>There currently are no TCCs assigned to the TAC Office.</i></p>
<p>Tactical Coordinator is one of the most important positions within the Tactical Office. The three TCCs serve as the beta
  testing leaders for their respective platforms: TIE, XvT/BoP, and XWA. They are responsible for ensuring that battles move
  through the testing queue quickly and are free of bugs before being passed on to the TCHC. They are also in charge of
  addressing bug reports on existing battles in the Mission Compendium.</p>
<p>However, the Tactical Officer and his Command attache are free to do these positions by themselves also. Should there not
  be any competent staff officers ready to do the work of tactical coordination.</p>

<h2>Tactical Head Coordinator (TCHC)</h2>
<p><i>There currently is no TCHC assigned to the TAC Office.</i></p>
<p>The position of Tactical Head Coordinator, if not filled by the CA:TAC, is usually given to a person who would like to
  do full-time service in Tactical Office, but does not wish to resign from his current assignment. The TCHC watches over
  beta testing as the direct superior to the Tactical Coordinators. The TCHC performs additional final checks and sends
  battles to the TAC for release.</p>
<p>The Tactical Head Coordinator can also help the TAC with different tasks connected to beta testing. When a TCC resigns,
  the TCHC takes over until a new TCC is chosen. The Tactical Head Coordinator can also help with judging Tactical Office
  competitions.</p>
<p>A TCHC is however, rarely appointed. Usually the TAC and his Command attache do the job by themselves.</p>
<?php include_once('includes/footer.php'); ?>