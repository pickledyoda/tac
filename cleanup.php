<?php
$file = "database/showbattle.php";
$contents = file_get_contents($file);

$contents = str_replace("(\$GLOBALS[\"___mysqli_ston\"] = mysqli_connect(\$dbhost, \$dbusername, \$dbpassword)) or die(\"Unable to connect to database\");", "", $contents);
$contents = str_replace("((bool)mysqli_query(\$GLOBALS[\"___mysqli_ston\"], \"USE \" . \$dbname));", "", $contents);
$contents = str_replace('\"', "'", $contents);
$contents = str_replace("\$GLOBALS[\"___mysqli_ston\"]", "\$dbLink", $contents);

if (strpos($contents, 'auth.php') === FALSE) {
  $contents = "<?php
  \$title = \"TAC Database - Battles\";
  include_once('../includes/header.php');
  require_once('../includes/bootstrap.php');
  require_once('auth.php');

  \$battle = FALSE;
  if (isAuthed() && isset(\$_GET['id'])) {
    \$battle = battle(\$_GET['id']);
  }
  " . $contents . "
  include_once('../includes/footer.php');
  ";
}


file_put_contents($file, $contents);
