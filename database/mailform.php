<?php

$name = "TAC Administration";
$email = "tac@emperorshammer.org";
$headers = "MIME-Version: 1.0\r\nContent-type: text/html; charset=iso-8859-1\r\n";
$headers .= "From: ".$name." <".$email.">";

switch($mailrsn) {
  case "newpass":
    $subject = "New password requested for the TAC database";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>This e-mail was generated because someone requested a new password for the TAC database for your PIN number.
             <br>Your TAC database password has been changed, and you can log in with the password listed below.
             <br>After logging in with this password, you are free to change it to a more convenient one.</p>
             <p><b><font color=\"blue\">Your PIN number</font></b>: ".$lppin."
             <br><b><font color=\"blue\">Your name</font></b>: ".$ranks[$lprank]." ".$lpname."</p>
             <p><b><font color=\"blue\">Your new password</font></b>: <b>".$lpnewpass."</b></p>
             <p>This request was made from the following IP address: <b>".$lpip."</b>
             <br>If you did not request a new password, please report this password change to <b>".$email."</b> and be sure to include this message!</p>
             <i>TAC Office Administration Services</i>";
    $sendto = $lpemail;
    break;

  case "newbattle":
    $subject = "New battle submission to the TAC database";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>This is a notification that a new custom battle has been submitted to the database using the <b>battle submission form</b>.</p>
             <p><b>Battle name:</b> ".$cbname."
             <br><b>Battle type:</b> ".$cbtype."
             <br><b>Platform:</b> ".$cbplatform."
             <br><b>Author:</b> ".$authname." (".$authpin.")</p>
             <p>The battle has been uploaded. The filename is <b>".$cbfile."</b></p>
             <p>This battle was uploaded from <b>".$safeip."</b></p>
             <i>TAC Office Administration Services</i>";
    $sendto = "tac@emperorshammer.org";
    break;

  case "newuser":
    $subject = "Welcome to the Tactical Office, ".$ranksfull[$_POST['rank']]." ".$_POST['name'];
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>Good day to you, <b><font color=\"blue\">".$ranksfull[$_POST['rank']]." ".$_POST['name']."</b></font> and welcome to the <font color=\"blue\"><b>Emperor's Hammer Tactical Office</b></font>.</p>
             <p>As you know, the Tactical Office is responsible for releasing new battles to the Mission Compendium, and as of today, you are a part of its staff.</p>
             <p>You can find all the information you need on the Tactical Office website at <a href=\"http://tac.emperorshammer.org\">http://tac.emperorshammer.org</a>, but never hesitate to send an e-mail to the TAC if you have questions.</p>
             <p>A random password has been generated for you which you can use to log in to the database at the Tactical Office. There, you can change the password if you want to.</p>
             <p>The Tactical Office is highly automated: when you receive a new assignment, you will receive an e-mail informing you. Then, you can use the database features to complete your assignment.</p>
             <p>Again, welcome to the TAC Office, and may your stay here be fruitful!</p>
             <p><i>".$ranksfull[$_SESSION['rank']]." ".$_SESSION['name']."</i><br>".$tacfull[$_SESSION['position']]."</p>
             <p><b><font color=\"red\">IMPORTANT INFORMATION:</font></b></p>
             <p><b><font color=\"blue\">database login</font></b>: Your TIE Corps PIN number (<b><font color=\"blue\">".$_POST['pin']."</b></font>)
             <br><b><font color=\"blue\">your password is</font></b>: ".$lpnewpass." (case-sensitive !)
             <br><b><font color=\"blue\">Tactical Office</font></b>: <a href=\"http://tac.emperorshammer.org\">http://tac.emperorshammer.org</a>.
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = $_POST['email'];
    break;

  case "newrank":
    $subject = "Rank change request in the TAC Database";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>This e-mail was generated because <b>".$rnknick."</b> has updated his rank on the TAC database.
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = "tac@emperorshammer.org";
    break;

  case "newbeta":
    $subject = "A beta test report has been uploaded to the TAC Database";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>A new beta report was uploaded to the TAC database by <b>".$ranks[$_SESSION['rank']]." ".$_SESSION['name']." (".$tac[$_SESSION['position']].")</b>
             <br>He has tested the battle <b>".$battle[$B_Name]."</b> on level <b>".$_POST['level']."</b>.
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = "tac@emperorshammer.org";
    break;

  case "corrected":
    $subject = "A corrected battle has been submitted by a TCT";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>A new file was uploaded to the TAC database by <b>".$ranks[$_SESSION['rank']]." ".$_SESSION['name']." (".$tac[$_SESSION['position']].")</b>
             <br>He has tested the battle <b>".$battle[$B_Name]."</b>.
             <br>He has uploaded the file /corrected/".$corrfile."
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = "tac@emperorshammer.org";
    break;

  case "betaccept":
    $subject = "A beta test report has been accepted.";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>A new beta report was uploaded to the TAC database by <b>".$ranks[$_SESSION['rank']]." ".$_SESSION['name']." (".$tac[$_SESSION['position']].")</b>
             <br>He has tested the battle <b>".$battle[$B_Name]."</b> on level <b>".$_POST['level']."</b>.</p>
             <p>Please credit him with the ".$creditbeta." �-points for testflying this battle.
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = "tac@emperorshammer.org";
    break;

  case "betadeny":
    $subject = "The TAC has denied your beta report.";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>The Tactical Officer has denied the beta test report you submitted for the battle <b><font color=\"blue\">".$battle[$B_Name]."</font></b>.</p>
             <p>You should login to the database, and correct your report according to comply with the reason the
                TAC has given for the denial. Please do so as soon as possible, but no later than <b><font color=\"blue\">".date("d F Y",date('U') + 604800)."</font></b>.
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = $tester[$N_EMail];
    break;

  case "tacdb1":
    $subject = "TAC update on the progress of your submitted battle";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>Your battle <b><font color=\"blue\">".$battle."</font></b> is currently undergoing its initial check. If your submission meets the submission guidelines in the TAC manual, it will be added to the queue. Please have patience, as the entire process can take several weeks.
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = $rcpt;
    break;
  case "tacdb2":
    $subject = "TAC update on the progress of your submitted battle";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>Your battle <b><font color=\"blue\">".$battle."</font></b> has passed the initial check and has been added to the queue. Submitted battles are assigned to our staff in order they are received. You can always view the status of your battle at the following url: <a href=\"".$viewurl."\">".$viewurl."</a></b>.
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = $rcpt;
    break;
  case "tacdb3":
    $subject = "New TAC Office assignment";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>You have been assigned to testfly a new battle in the queue. Please log in to the TAC database and download the battle. Your beta testreport is due no later than <b><font color=\"blue\">".date("d F Y",date('U') + 1209600)."</font></b>
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = $rcpt;
    break;
  case "tacdb4":
    $subject = "New TAC Office assignment";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>You have been assigned to correct a new battle in the queue. Please log in to the TAC database and download the battle and beta testreports. Upload the new .ZIP file to the database no later than <b><font color=\"blue\">".date("d F Y",date('U') + 1209600)."</font></b>
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = $rcpt;
    break;
  case "tacdb5":
    $subject = "TAC update on the progress of your submitted battle";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>Your battle <b><font color=\"blue\">".$battle."</font></b> is currently undergoing its final check by the TAC and should be released soon, assuming no problems are found.
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = $rcpt;
    break;
  case "tacdb6":
    $subject = "Your submitted battle has been rejected";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>Your battle <b><font color=\"blue\">".$battle."</font></b> has been rejected by the Tactical Officer for the reasons listed below. You are encouraged to fix the stated problems and resubmit your battle. You are always welcome to e-mail the TAC for further information at <b><a href=\"mailto:tac@emperorshammer.org\">tac@emperorshammer.org</a></b>.
             </p><p>".$reject."
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = $rcpt;
    break;
  case "tacdb7":
    $subject = "Your submitted battle has been accepted!";
    $text = "<b>This is an automatically generated e-mail.</b>
             <p>Congratulations! Your custom battle <b><font color=\"blue\">".$battle."</font></b> has been accepted and will soon be released onto the Mission Compendium. When it does, you will receive your <b>Medal of Tactics</b>.
             </p>
             <i>TAC Office Administration Services</i>";
    $sendto = $rcpt;
    break;
}

mail($sendto,$subject,$text,$headers);

?>
