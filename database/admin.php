<?php
$title = "TAC Database";
include_once('../includes/header.php');
require_once("../includes/bootstrap.php");
require_once("auth.php");

$user = Roster::get($_SESSION['PIN']);

if (hasAuthLevel(1)) {
  echo "<p>Welcome to the Tactical Office Database, " . $user->label() . ". You are logged in as " . $user->position() . ".</p>";
} else {
  echo "<p>Good day{$_SESSION['name']} 
  You have been set up with a temporary account on the TAC Office database. 
  Here, you can find your assignment. As you know, you have two weeks to complete the assignment.
  If the Tactical Officer approves, you will be promoted to TCS and get the appropriate database access,
 as well as the 'TCS' in your ID line to reflect your status.</p>";
  echo "<p>If you have any questions, don't hesitate to ask the TAC on IRC or via e-mail.</p>";
}

$menu = [];
if (hasAuthLevel(1)) { // all staff except new
  $menu['Account Options'] = [
    'My Account' => 'myaccount',
    'Change Account Info' => 'changeaccount',
    'Logout' => 'logout'
  ];
}
if (hasAuthLevel(2)) { // active staff
  $menu['Test Assignments'] = [
    'My Assignments' => 'myassignment',
    'View My Beta Reports' => 'myreports'
  ];
}
if (hasAuthLevel(3)) { // TAC , CA:TAC
  $menu['Account Administration'] = [
    'Add a New Account' => 'newaccount',
    'Roster Management' => 'rosteradmin',
    'Beta Points Management' => 'betapoints'
  ];
  $menu['Administration'] = [
    'Assignment Management' => 'assignments',
    'Battle Management' => 'battles',
    'Beta Report Management' => 'reportmanagement',
  ];
}
if (hasAuthLevel(4)) {
  $menu['TAC'] = [




    
    'Mission Counter' => 'managecounter',
    'Backup/Restore Database' => 'backup'
  ];
}
echo "<div class='d-flex flex-wrap justify-content-between'>";
foreach ($menu as $heading => $options) {
  echo "<div class='list-group p-2'>";
  echo "<div class='list-group-item text-warning'><h5 class='m-0'>{$heading}</h3></div>";
  foreach ($options as $text => $page) {
    echo "<a href='{$page}.php' class='list-group-item list-group-item-action text-light'>{$text}</a>";
  }
  echo "</div>";
}
echo "</div>";
include_once('../includes/footer.php');
