<?php
session_start();
?>

<html>
<head>
<?php
include "../tac/dbstuff.tac";
include "../includes/phpself.php";

if (isset($_POST['submit1']) || isset($_POST['submit2'])) {
  echo "<META http-equiv=\"refresh\" content=\"2;URL=rosteradmin.php\">";
}

?>
<link rel="stylesheet" type="text/css" href="../tac.css">
</head>
<body>
<?php
if (isAuthed()) {
  echo "<font class=\"text\">";

  if (isset($_POST['submit1'])) {
    $plf = setPlatforms($_POST['platform']);
    $query = "UPDATE roster SET N_Password ='";
      if (strlen($_REQUEST['newpass']) == 32) { $query .= $_REQUEST['newpass']; }
      else { $query .= md5($_REQUEST['newpass']); }
    $query .= "', N_Rank = '".$_REQUEST['newrank']."', N_Position = '".$_REQUEST['newposition']."', N_Email = '".addslashes($_REQUEST['newemail'])."', N_Pts = '".$_REQUEST['newpoints']."', N_Platform = '".$plf."' WHERE N_ID = '".$_GET['id']."'";
    ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    echo "The information for this user has been changed.";
  }

  elseif (isset($_POST['submit2'])) {
    $query = "DELETE FROM roster WHERE N_ID='".$_GET['id']."'";
    ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    echo "The user has been permanently deleted from the database.";
  }

  else {
    ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
    $query = "SELECT * FROM roster WHERE N_ID='".$_GET['id']."'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $profile = mysqli_fetch_row($result);

    echo "\n<form method=\"POST\" action=\"".getPHPSelf()."?id=".$profile[$N_ID]."\">";
    echo "\n<table width=\"700\" border=\"0\">";
    echo "\n<tr><td width=\"700\" colspan=\"3\">";
        echo "<table width=\"700\" border=\"0\"><tr><td width=\"450\">Editing the profile of staff member ".$profile[$N_ID].": ".$profile[$N_Name]." (".$profile[$N_PIN].")<br>&nbsp;</td><td width=\"250\">&nbsp;</td></tr></table>";
    echo "</td></tr>";
    echo "\n<tr><td width=\"250\" valign=\"center\" class=\"text\">Rank and Position:</td><td width=\"150\" valign=\"center\" class=\"text\"><select name=\"newrank\">";
         $j = 1;
         while ($ranks[$j]) {
           echo "<option value=\"".$j."\"";
           if ($j == $profile[$N_Rank]) { echo " selected=\"selected\""; }
           echo ">".$ranksfull[$j]."</option>";
           $j++;
         }
         echo "</select></td>";
    echo "\n<td width=\"300\" valign=\"center\" class=\"text\"><select name =\"newposition\">";
         $j = 1;
         while ($tac[$j]) {
           echo "<option value=\"".$j."\"";
           if ($j == $profile[$N_Position]) { echo " selected=\"selected\""; }
           echo ">".$tacfull[$j]."</option>";
           $j++;
         }
         echo "</select></td></tr>";
    echo "\n<tr><td width=\"250\" valign=\"center\" class=\"text\">Password (md5 encrypted):</td><td width=\"450\" colspan=\"2\" valign=\"center\" class=\"text\"><input type=\"text\" name=\"newpass\" size=\"50\" value=\"".$profile[$N_Password]."\"></td></tr>";
    echo "\n<tr><td width=\"250\" valign=\"center\" class=\"text\">e-Mail Address:</td><td width=\"450\" colspan=\"2\" valign=\"center\" class=\"text\"><input type=\"text\" name=\"newemail\" size=\"50\" value=\"".$profile[$N_EMail]."\"></td></tr>";
    echo "\n<tr><td width=\"250\" valign=\"center\" class=\"text\">�-Points:</td><td width=\"450\" colspan=\"2\" valign=\"center\" class=\"text\"><input type=\"text\" name=\"newpoints\" size=\"50\" value=\"".$profile[$N_Points]."\"></td></tr>";
    echo "\n<tr><td width=\"250\" valign=\"top\" class=\"text\">Platforms:</td><td width=\"450\" colspan=\"2\">";
          echo "<table width=\"450\" border=\"0\"><tr>";
	      echo "<td width=\"225\" valign=\"top\" class=\"text\">";
	      $arr = explode(" ",$profile[$N_Platforms]);
          echo "<input type=\"checkbox\" name=\"platform[]\" value=\"XW\"";
	           if (in_array("XW",$arr)) { echo " checked"; }
	           echo "> X-wing<br>";
	      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"TIE\"";
	           if (in_array("TIE",$arr)) { echo " checked"; }
	           echo "> TIE Fighter<br>";
	      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"XvT\"";
	           if (in_array("XvT",$arr)) { echo " checked"; }
	           echo "> X-wing vs. TIE Fighter<br>";
	      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"BoP\"";
	           if (in_array("BoP",$arr)) { echo " checked"; }
	           echo "> Balance of Power<br>";
	      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"XWA\"";
	           if (in_array("XWA",$arr)) { echo " checked"; }
	           echo "> X-wing Alliance<br>";
	      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"JA\"";
	           if (in_array("JA",$arr)) { echo " checked"; }
	           echo "> Jedi Academy";
	      echo "</td>";
	      echo "<td width=\"225\" valign=\"top\" class=\"text\">";
	      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"SWGB\"";
	           if (in_array("SWGB",$arr)) { echo " checked"; }
	           echo "> Galactic Battlegrounds<br>";
	      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"IAII\"";
	           if (in_array("IAII",$arr)) { echo " checked"; }
	           echo "> Imperial Alliance<br>";
	      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"IAS\"";
	           if (in_array("IAS",$arr)) { echo " checked"; }
	           echo "> Imperial Assault<br>";
	      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"EaW\"";
	           if (in_array("EaW",$arr)) { echo " checked"; }
	           echo "> Empire at War<br>";
	      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"BF\"";
	           if (in_array("BF",$arr)) { echo " checked"; }
	           echo "> Battlefront<br>";
	      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"BFII\"";
	           if (in_array("BFII",$arr)) { echo " checked"; }
	           echo "> Battlefront II";
	      echo "</td></tr></table>";
    echo "</td></tr>";
    echo "\n<tr><td width=\"250\" valign=\"center\">&nbsp;</td><td width=\"450\" colspan=\"2\" valign=\"top\">";
          echo "<table width=\"450\"><tr>";
          echo "<td width=\"175\" height=\"60\" valign=\"top\"><br><input type=\"submit\" value=\"change profile\" name=\"submit1\"></td>";
          echo "<td width=\"275\" height=\"60\" valign=\"top\"><br><input type=\"submit\" value=\"delete account\" name=\"submit2\"><br><b> &nbsp; (Warning! - This CANNOT be undone!)</b></td>";
          echo "</tr></table>";
    echo "\n</table>";
    echo "\n</form>";
  }
}

else { echo "<p class=\"text\">You have no access to this page.</p>"; }
?>
</body>
</html>
