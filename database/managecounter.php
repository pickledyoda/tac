<?php
session_start();
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../tac.css">
</head>
<body>

<?php

/* no use logging in twice is there... */
if (isAuthed()) {

  include "../tac/dbstuff.tac";
  include "../includes/phpself.php";

  /* if the `submit changes`  has been clicked... */

  if (isset($_POST['submit'])) {
    $xwb = $_POST['xwb']; $xwm = $_POST['xwm'];
    $tieb = $_POST['tieb']; $tiem = $_POST['tiem'];
    $xvtb = $_POST['xvtb']; $xvtm = $_POST['xvtm'];
    $bopb = $_POST['bopb']; $bopm = $_POST['bopm'];
    $xwab = $_POST['xwab']; $xwam = $_POST['xwam'];
    $swgbb = $_POST['swgbb']; $swgbm = $_POST['swgbm'];
    $jab = $_POST['jab']; $jam = $_POST['jam'];

    $link = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool)mysqli_query($link, "USE " . $dbname));

    $query = "UPDATE counter SET C_BATTLES = ".$xwb.",  C_MISSIONS = '".$xwm."' WHERE C_PLATFORM = 'XW'";
    $result = mysqli_query($link, $query);
    $query = "UPDATE counter SET C_BATTLES = '".$tieb."',  C_MISSIONS = '".$tiem."' WHERE C_PLATFORM = 'TIE'";
    $result = mysqli_query($link, $query);
    $query = "UPDATE counter SET C_BATTLES = '".$xvtb."',  C_MISSIONS = '".$xvtm."' WHERE C_PLATFORM = 'XvT'";
    $result = mysqli_query($link, $query);
    $query = "UPDATE counter SET C_BATTLES = '".$bopb."',  C_MISSIONS = '".$bopm."' WHERE C_PLATFORM = 'BoP'";
    $result = mysqli_query($link, $query);
    $query = "UPDATE counter SET C_BATTLES = '".$xwab."',  C_MISSIONS = '".$xwam."' WHERE C_PLATFORM = 'XWA'";
    $result = mysqli_query($link, $query);
    $query = "UPDATE counter SET C_BATTLES = '".$swgbb."',  C_MISSIONS = '".$swgbm."' WHERE C_PLATFORM = 'SWGB'";
    $result = mysqli_query($link, $query);
    $query = "UPDATE counter SET C_BATTLES = '".$jab."',  C_MISSIONS = '".$jam."' WHERE C_PLATFORM = 'JA'";
    $result = mysqli_query($link, $query);

    echo "<font class=\"text\">The counters have been updated.</font>";
  }

  /* else just display the stats */

  else {
    echo "\n<p class=\"text\">This page will allow you to manually reset the counters, just in case something went wrong or needs to be changed.</p><p>Please use with caution, as changes can only be undone by using this form again...</p>";

    ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
    $query = "SELECT * FROM counter";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $battot = 0;
    $mistot = 0;

    while ($countme = mysqli_fetch_row($result)) {
      switch ($countme[$C_PLATFORM]) {
        case "TIE": $tieb = $countme[$C_BATTLES]; $tiem = $countme[$C_MISSIONS]; break;
        case "XvT": $xvtb = $countme[$C_BATTLES]; $xvtm = $countme[$C_MISSIONS]; break;
        case "BoP": $bopb = $countme[$C_BATTLES]; $bopm = $countme[$C_MISSIONS]; break;
        case "XWA": $xwab = $countme[$C_BATTLES]; $xwam = $countme[$C_MISSIONS]; break;
        case "XW": $xwb = $countme[$C_BATTLES]; $xwm = $countme[$C_MISSIONS]; break;
        case "JA": $jab = $countme[$C_BATTLES]; $jam = $countme[$C_MISSIONS]; break;
        case "SWGB": $swgbb = $countme[$C_BATTLES]; $swgbm = $countme[$C_MISSIONS]; break;
      }
    }

    echo "<form method=\"POST\" action=\"".getPHPSelf()."\">";
    echo "\n<table width=\"400\" border=\"0\" class=\"alt\">";
    echo "\n<tr><td width=\"200\">Platform</td><td width=\"100\" align=\"center\">Battles</td><td width=\"100\" align=\"center\">Total Missions</td></tr>";
    echo "\n<tr><td>X-wing</td><td align=\"center\"><input type=\"text\" name=\"xwb\" size=\"7\" value=\"".$xwb."\"></td><td align=\"center\"><input type=\"text\" name=\"xwm\" size=\"7\" value=\"".$xwm."\"></td></tr>";
    echo "\n<tr><td>TIE Fighter</td><td align=\"center\"><input type=\"text\" name=\"tieb\" size=\"7\" value=\"".$tieb."\"></td><td align=\"center\"><input type=\"text\" name=\"tiem\" size=\"7\" value=\"".$tiem."\"></td></tr>";
    echo "\n<tr><td>X-wing vs. TIE Fighter</td><td align=\"center\"><input type=\"text\" name=\"xvtb\" size=\"7\" value=\"".$xvtb."\"></td><td align=\"center\"><input type=\"text\" name=\"xvtm\" size=\"7\" value=\"".$xvtm."\"></td></tr>";
    echo "\n<tr><td>Balance of Power</td><td align=\"center\"><input type=\"text\" name=\"bopb\" size=\"7\" value=\"".$bopb."\"></td><td align=\"center\"><input type=\"text\" name=\"bopm\" size=\"7\" value=\"".$bopm."\"></td></tr>";
    echo "\n<tr><td>X-wing Alliance</td><td align=\"center\"><input type=\"text\" name=\"xwab\" size=\"7\" value=\"".$xwab."\"></td><td align=\"center\"><input type=\"text\" name=\"xwam\" size=\"7\" value=\"".$xwam."\"></td></tr>";
    echo "\n<tr><td>SW: Galactic Battlegrounds</td><td align=\"center\"><input type=\"text\" name=\"swgbb\" size=\"7\" value=\"".$swgbb."\"></td><td align=\"center\"><input type=\"text\" name=\"swgbm\" size=\"7\" value=\"".$swgbm."\"></td></tr>";
    echo "\n<tr><td>Jedi Academy</td><td align=\"center\"><input type=\"text\" name=\"jab\" size=\"7\" value=\"".$jab."\"></td><td align=\"center\"><input type=\"text\" name=\"jam\" size=\"7\" value=\"".$jam."\"></td></tr>";
    echo "\n<tr><td>&nbsp;</td><td colspan=\"2\"> &nbsp; &nbsp; <input type=\"submit\" value=\"change\" name=\"submit\"></td></tr>";
  }

}

else {
  echo "\n<p class=\"text\">You have no access to this page.</p>";
}
?>
