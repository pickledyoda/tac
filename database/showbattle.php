<?php
$title = "TAC Database";
require_once('../includes/bootstrap.php');
require_once('auth.php');
require_once('model/bootstrap.php');

$battle = FALSE;
if (isAuthed() && isset($_GET['id'])) {
  $battle = Battle::load($_GET['id']);
  $title .= " - " . $battle->name;
}
include_once('../includes/header.php');

function getStaff($temp)
{
  include "../tac/dbstuff.tac";
  $staffp = "<option>Do not re-assign";


  $query  = "SELECT * FROM roster ORDER BY N_Position,N_Rank,N_Name";
  $result = mysqli_query($dbLink, $query);
  while ($staff = mysqli_fetch_row($result)) {
    if ($temp == "TCS") {
      if ($staff[$N_Position] <= 6) {
        $staffp = $staffp . "<option>" . $ranks[$staff[$N_Rank]] . " " . $staff[$N_Name] . " (" . $tac[$staff[$N_Position]] . ")";
      }
    } elseif ($temp == "TCT") {
      if ($staff[$N_Position] <= 4 || $staff[$N_Position] == 6) {
        $staffp = $staffp . "<option>" . $ranks[$staff[$N_Rank]] . " " . $staff[$N_Name] . " (" . $tac[$staff[$N_Position]] . ")";
      }
    }
  }
  return $staffp;
}

function getPin($user)
{
  include "../tac/dbstuff.tac";
  $tok  = strtok($user, "(");
  $bat  = strtok($tok, " ");
  $name = trim(str_replace($bat, "", $tok));


  $query  = "SELECT N_PIN FROM roster WHERE N_Name='" . $name . "'";
  $result = mysqli_query($dbLink, $query);
  $info   = mysqli_fetch_array($result);
  return $info[0];
}

if ($battle) {
  if (!isset($_POST['submit'])) {
    ?>
    <h2><?php echo $battle->name; ?></h2>
    <h4>by <?php echo $battle->authorName; ?> (<?php echo $battle->authorEmail; ?>)</h4>
    <?php
        foreach ($battle->fields() as $label => $value) {
          echo "<p><b>$label</b></p>";
          echo "<p>$value</p>";
        }

        if ($tac[$_SESSION['position']] == "TAC" && $info[$D_Status] >= 5) {
          $temp = explode($DELIM, $info[$D_History]);
          $k    = count($temp);
          $m    = 0;
          while ($m < $k) {
            if (strpos($temp[$m], "testing")) {
              $item = explode($DELIM, $temp[$m]);
              $t    = 1;
              while ($t <= 3) {
                if ($item[$t]) {
                  echo "\n<tr><td width='200' valign='top' class='text'>&nbsp;</td>";
                  echo str_replace("to", ":", str_replace("assigned for testing on level", "<td width='500' valign='top' class='text'>TCS", $item[$t]));
                  echo "\n</td></tr>";
                }
                $t++;
              }
            }
            $m++;
          }
        }

        echo "</td></tr>";
        echo "\n<tr><td width='200' valign='top' class='text'><a href='viewhistory.php?id=" . $ID . "'>View Battle History</a></td><td width='500' valign='top' class='text'>&nbsp;</td></tr>";

        // here = database/showbattle
        $bDir = "../uploads/battles/$ID";
        if (file_exists($bDir)) {
          echo "\n<tr><td width='200' valign='top' class='text'><a href='viewsummary.php?id=" . $ID . "'>View Battle Summary</a></td><td width='500' valign='top' class='text'>&nbsp;</td></tr>";
        }

        echo "\n</table>";

        if ($info[$D_Status] < 9) {
          echo "<hr>";

          echo "\n<form method='POST' action='" . getPHPSelf() . "?id=" . $ID . "'>";
          echo "\n<table width='600' border='0'>";
          echo "\n<tr><td width='600' colspan='2' class='text'>Change Status / Assignments for this battle to:</td></tr>";
          echo "\n<tr><td width='200' valign='top' class='text'><input type='radio' name='newstatus' value='1' valign='top' class='text'> submitted to TAC</td><td width='400'></tr>";
          echo "\n<tr><td width='200' valign='top' class='text'><input type='radio' name='newstatus' value='2' valign='top' class='text'> initial check</td><td width='400'></tr>";
          echo "\n<tr><td width='200' valign='top' class='text'><input type='radio' name='newstatus' value='3' valign='top' class='text'> queued</td><td width='400'></tr>";
          echo "\n<tr><td width='200' valign='top' class='text
          
          '><input type='radio' name='newstatus' value='4' valign='top' class='text'> under testing</td><td width='400'>";
          echo "<table width='400' border='0'>";
          $stat = "TCS";
          echo "\n<tr><td width='100' valign='center' class='text'>Easy:</td><td width='300' valign='center' class='text'><select name='testeasy'>" . getStaff(TCS) . "</td></tr>";
          echo "\n<tr><td width='100' valign='center' class='text'>Medium:</td><td width='300' valign='center' class='text'><select name='testmedium'>" . getStaff("TCS") . "</td></tr>";
          echo "\n<tr><td width='100' valign='center' class='text'>Hard:</td><td width='300' valign='center' class='text'><select name='testhard'>" . getStaff($stat) . "</td></tr>";
          echo "</table>";
          echo "</tr>";
          echo "\n<tr><td width='200' valign='top' class='text'><input type='radio' name='newstatus' value='5' valign='top' class='text'> under correction</td><td width='400'>";
          echo "<table width='400' border='0'>";
          $stat = "TCT";
          echo "\n<tr><td width='100' valign='center' class='text'>&nbsp;</td><td width='300' valign='center' class='text'><select name='correct'>" . getStaff($stat) . "</td></tr>";
          echo "</table>";
          echo "</tr>";
          echo "\n<tr><td width='200' valign='top' class='text'><input type='radio' name='newstatus' value='6' valign='top' class='text'> final check</td><td width='400'>";
          echo "<table width='400'>";
          echo "\n<tr><td width='100' valign='center' class='text'>&nbsp;</td><td width='450' valign='center' class='text'>&nbsp;</td></tr>";
          echo "</table>";
          echo "</tr>";
          echo "\n<tr><td width='200' valign='top' class='text'><input type='radio' name='newstatus' value='7' valign='top' class='text'> rejected <br> &nbsp; &nbsp; &nbsp; (reason)</td><td width='550'>";
          echo "<table width='400'>";
          echo "\n<tr><td width='100' valign='center' class='text'>&nbsp;</td><td width='450' valign='center' class='text'><textarea name='reason' cols='40' rows='5'>Enter reason for rejection</textarea></td></tr>";
          echo "</table>";
          echo "</tr>";
          echo "\n<tr><td width='200' valign='top' class='text'><input type='radio' name='newstatus' value='8' valign='top' class='text'> accepted</td><td width='400'></tr>";
          echo "\n<tr><td width='200' valign='top' class='text'><input type='radio' name='newstatus' value='9' valign='top' class='text'> released</td><td width='400'></tr>";
          echo "\n<tr><td width='200'><br><input type='submit' value='update status' name='submit'></td><td width='400'>&nbsp;</td></tr>";
          echo "\n</table>";
          echo "\n</form>";
        } // end if status
      } // end if not submitted

      elseif ($_POST['newstatus'] >= 1 && $_POST['newstatus'] <= 9) {
        include "../tac/dbstuff.tac";

        $newstatus = $_POST['newstatus'];



        $query  = "SELECT * FROM battles WHERE B_ID='" . $ID . "'";
        $result = mysqli_query($dbLink, $query);
        $info   = mysqli_fetch_row($result);

        $battle = $info[$B_Name];
        switch ($newstatus) {
          case 1:
            $query = "UPDATE battles SET " .
              "D_Status = '" . $newstatus . "', " .
              "D_History = '" . addslashes($info[$D_History]) . $DELIM . date('U') . "�status changed to submitted to TAC'" .
              "where B_ID = '" . $ID . "'";
            break;
          case 2:
            $query   = "UPDATE battles SET " .
              "D_Status = '" . $newstatus . "', " .
              "D_History = '" . addslashes($info[$D_History]) . $DELIM . date('U') . "�initial check by TAC' " .
              "where B_ID = '" . $ID . "'";
            $mailrsn = "tacdb1";
            $mailto  = "user";
            break;
          case 3:
            $query   = "UPDATE battles SET " .
              "D_Status = '" . $newstatus . "', " .
              "D_History = '" . addslashes($info[$D_History]) . $DELIM . date('U') . "�battle added to the queue' " .
              "where B_ID = '" . $ID . "'";
            $mailrsn = "tacdb2";
            $mailto  = "user";
            break;
          case 4:
            $query = "UPDATE battles SET ";
            $query = $query . "D_Status = '" . $newstatus . "', ";
            if ($_POST['testeasy'] != "Do not re-assign") {
              $query = $query . "D_TestEasy = '" . getPin($_POST['testeasy']) . "', ";
            }
            if ($_POST['testmedium'] != "Do not re-assign") {
              $query = $query . "D_TestMedium = '" . getPin($_POST['testmedium']) . "', ";
            }
            if ($_POST['testhard'] != "Do not re-assign") {
              $query = $query . "D_TestHard = '" . getPin($_POST['testhard']) . "', ";
            }
            $query = $query . "D_History = '" . $info[$D_History] . $DELIM . date('U');
            if ($_POST['testeasy'] != "Do not re-assign") {
              $query = $query . "�assigned for testing on level easy to " . $_POST['testeasy'];
            }
            if ($_POST['testmedium'] != "Do not re-assign") {
              $query = $query . "�assigned for testing on level medium to " . $_POST['testmedium'];
            }
            if ($_POST['testhard'] != "Do not re-assign") {
              $query = $query . "�assigned for testing on level hard to " . $_POST['testhard'];
            }
            $query   = $query . "' ";
            $query   = $query . "where B_ID = '" . $ID . "'";
            $mailrsn = "tacdb3";
            $mailto  = "TCS";
            break;
          case 5:
            $query = "UPDATE battles SET " .
              "D_Status = '" . $newstatus . "', " .
              "D_TestEasy = '0', " .
              "D_TestMedium = '0', " .
              "D_TestHard = '0', " .
              "D_Correct = '" . getPin($_POST['correct']) . "', " .
              "D_History = '" . addslashes($info[$D_History]) . $DELIM . date('U') . "�assigned for correcting to " . $_POST['correct'] . "' " .
              "where B_ID = '" . $ID . "'";
            if ($info[$D_TestEasy] != "0") {
              $extra[1] = "easy�" . $info[$D_TestEasy];
            }
            if ($info[$D_TestMedium] != "0") {
              $extra[2] = "medium�" . $info[$D_TestMedium];
            }
            if ($info[$D_TestHard] != "0") {
              $extra[3] = "hard�" . $info[$D_TestHard];
            }
            $mailrsn = "tacdb4";
            $mailto  = "TCT";
            break;
          case 6:
            $query   = "UPDATE battles SET " .
              "D_Status = '" . $newstatus . "', " .
              "D_Correct = '0', " .
              "D_History = '" . addslashes($info[$D_History]) . $DELIM . date('U') . "�Assigned to TAC for final check " . $_POST['finalcheck'] . "' " .
              "where B_ID = '" . $ID . "'";
            $mailrsn = "tacdb5";
            $mailto  = "user";
            break;
          case 7:
            $query   = "UPDATE battles SET " .
              "D_Status = '" . $newstatus . "', " .
              "D_Correct = '0', " .
              "D_History = '" . addslashes($info[$D_History]) . $DELIM . date('U') . "�battle rejected�reason: " . $_POST['reason'] . "' " .
              "where B_ID = '" . $ID . "'";
            $mailrsn = "tacdb6";
            $mailto  = "user";
            $reject  = $_POST['reason'];
            break;
          case 8:
            $query   = "UPDATE battles SET " .
              "D_Status = '" . $newstatus . "', " .
              "D_Correct = '0', " .
              "D_History = '" . addslashes($info[$D_History]) . $DELIM . date('U') . "�battle accepted and awaiting release' " .
              "where B_ID = '" . $ID . "'";
            $mailrsn = "tacdb7";
            $mailto  = "user";
            break;
          case 9:
            $query = "UPDATE battles SET " .
              "D_Status = '" . $newstatus . "', " .
              "D_History = '" . addslashes($info[$D_History]) . $DELIM . date('U') . "�battle released' " .
              "where B_ID = '" . $ID . "'";
            // change counter!
            switch ($info[$B_Platform]) {
              case XW:
                $plt = "XW";
                break;
              case TIE:
                $plt = "TIE";
                break;
              case XvT:
                $plt = "XvT";
                break;
              case BoP:
                $plt = "BoP";
                break;
              case XWA:
                $plt = "XWA";
                break;
              case JA:
                $plt = "JA";
                break;
              case SWGB:
                $plt = "SWGB";
                break;
            }

            $query3    = "SELECT * FROM counter WHERE C_PLATFORM='" . $plt . "'";
            $cntr      = mysqli_query($dbLink, $query3);
            $counts    = mysqli_fetch_row($cntr);
            $newmcount = $counts[$C_MISSIONS] + $info[$B_Missions];
            if ($info[$B_Type] == "FREE" || $info[$B_Platform] == "JA") {
              $hammer = "Green Hammer";
              $query2 = "UPDATE counter SET C_MISSIONS='" . $newmcount . "' WHERE C_PLATFORM = '" . $counts[$C_PLATFORM] . "'";
            } else {
              $hammer = "Red Hammer";
              $cnt    = intval(($info[$B_Missions] - 4) / 5) + 1;
              if ($cnt > 1) {
                $s == "s";
              }
              $hammer    = $cnt . " " . $hammer . $s;
              $newbcount = $counts[$C_BATTLES] + 1;
              $query2    = "UPDATE counter SET C_BATTLES='" . $newbcount . "', C_MISSIONS='" . $newmcount . "' WHERE C_PLATFORM = '" . $counts[$C_PLATFORM] . "'";
            }

            break;
        } // end switch



        $result = mysqli_query($dbLink, $query);

        if (isset($query2)) {
          $result = mysqli_query($dbLink, $query2);
          echo "\nThis battle has been released. You should recommend a Medal of Tactics with a " . $hammer . " to " . $info[$B_Author] . " (" . $info[$B_PIN] . ").";
          echo "\n<p>Don't forget to delete the files /uploads/" . $info[$B_File] . " and /corrected/" . $info[$D_Corrected] . ".";
        } else {
          echo "Battle Status has been changed.";
        }

        /* function to get mail address for a PIN */

        function pinMail($pin)
        {
          include "../tac/dbstuff.tac";


          $query   = "SELECT N_EMail FROM roster WHERE N_PIN='" . $pin . "'";
          $result  = mysqli_query($dbLink, $query);
          $pinmail = mysqli_fetch_array($result);
          return $pinmail[0];
        } // end pinMail

        switch ($mailto) {
          case "user":
            $rcpt = $info[$B_EMail];
            break;
          case "TCT":
            $rcpt = pinMail(getPin($_POST['correct']));
            break;
          case "TCS":
            $rcpt = pinMail(getPin($_POST['testeasy'])) . "," . pinMail(getPin($_POST['testmedium'])) . "," . pinMail(getPin($_POST['testhard']));
            break;
        } // end switch

        $viewurl = "http://tac.emperorshammer.org/base.php?id=" . $ID;
        if ($mailrsn == "tacdb1" || $mailrsn == "tacdb2" || $mailrsn == "tacdb3" || $mailrsn == "tacdb4" || $mailrsn == "tacdb5" || $mailrsn == "tacdb6" || $mailrsn == "tacdb7") {
          include "mailform.php";
        }

        $query  = "SELECT * FROM battles WHERE B_ID='" . $ID . "'";
        $result = mysqli_query($dbLink, $query);
        $info   = mysqli_fetch_row($result);

        if ($extra[1]) {
          $query  = "SELECT * FROM roster where N_PIN ='" . $info[$D_TestEasy] . "'";
          $result = mysqli_query($dbLink, $query);
          $names  = mysqli_fetch_row($result);

          echo "<p class='tacmantitle'>WARNING</font>:<br>You have assigned a TCT to this battle, but it is still assigned to a TCS: " . $ranks[$names[$N_Rank]] . " " . $names[$N_Name] . " (level Easy)</p>";
        }
        if ($extra[2]) {
          $query  = "SELECT * FROM roster where N_PIN ='" . $info[$D_TestMedium] . "'";
          $result = mysqli_query($dbLink, $query);
          $names  = mysqli_fetch_row($result);

          echo "<p class='tacmantitle'>WARNING</font>:<br>You have assigned a TCT to this battle, but it is still assigned to a TCS: " . $ranks[$names[$N_Rank]] . " " . $names[$N_Name] . " (level Medium)</p>";
        }
        if ($extra[3]) {
          $query  = "SELECT * FROM roster where N_PIN ='" . $info[$D_TestHard] . "'";
          $result = mysqli_query($dbLink, $query);
          $names  = mysqli_fetch_row($result);

          echo "<p class='tacmantitle'>WARNING</font>:<br>You have assigned a TCT to this battle, but it is still assigned to a TCS: " . $ranks[$names[$N_Rank]] . " " . $names[$N_Name] . " (level Hard)</p>";
        }

        echo "<p><a href='battles.php'>Return to the Battle Listings</a></p>";
      } else {
        echo "Error: you have not selected a new status for the battle!";
      }
    } // end if session

    else {
      echo "<p class='text'>You have no access to this page.</p>";
    }

    include_once('../includes/footer.php');
