<?php
$title = "TAC Database - Battles";
include_once('../includes/header.php');
require_once("../includes/bootstrap.php");
require_once("auth.php");
require_once("../pyrite/bootstrap.php");

$battle = FALSE;
if (isAuthed() && isset($_GET['id'])) {
  $battle = battle($_GET['id']);
}
if ($battle) {
  $file = $battle['B_File'];
  $path = "../uploads/{$file}";

  if ($battle['D_Corrected']) {
    $file = $battle['D_Corrected'];
    $path = "../corrected/{$file}";
  }

  if (file_exists($path)) {
    pre($battle);
    $name = "{$battle['B_Platform']}TAC{$battle['B_ID']}";
    $battle  = \Pyrite\EHBL\Battle::fromZip($path, $name);
    $package = \Pyrite\EHBL\Packager::fromBattle($battle);
    $ehmPath = $package->toEHM();
    $ehmFile = basename($ehmPath);

    header('Content-Type: application/ehm');
    header("Content-Disposition: attachment; filename=$ehmFile");
    echo file_get_contents($ehmPath);
  } else {
    echo "File not found: $path";
  }
} else {
  echo "<p>You have no access to this page.</p>";
}
include_once('../includes/footer.php');
