<?php
session_start();
?>

<html>

<head>
  <link rel="stylesheet" type="text/css" href="../tac.css">
</head>

<body>

  <?php

  /* no use logging in twice is there... */
  if (isAuthed()) {
    include "../tac/dbstuff.tac";
    include "../includes/phpself.php";

    if (isset($_POST['submit'])) {
      ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
      ((bool) mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));

      $delete = $_POST['delete'];
      $i = 0;
      while ($delete[$i]) {
        $split = explode("-", $delete[$i]);
        if ($split[1] == "easy") {
          $type = "D_TestEasy";
        }
        if ($split[1] == "medium") {
          $type = "D_TestMedium";
        }
        if ($split[1] == "hard") {
          $type = "D_TestHard";
        }
        if ($split[1] == "correct") {
          $type = "D_Correct";
        }

        $query = "SELECT * FROM battles where B_ID = '" . $split[2] . "'";
        $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
        $bn = mysqli_fetch_row($result);

        $query = "SELECT * FROM roster where N_PIN = '" . $split[0] . "'";
        $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
        $name = mysqli_fetch_row($result);

        $query = "UPDATE battles SET " . $type . " = '0', D_History = '" . addslashes($bn[$D_History]) . "�" . date('U') . "�assignment " . str_replace("D_", "", $type) . " removed from " . $ranks[$name[$N_Rank]] . " " . $name[$N_Name] . "' WHERE " . $type . " = '" . $split[0] . "' and B_ID = '" . $split[2] . "'";
        $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
        $i++;
      }
    }

    ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool) mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
    $query = "SELECT * FROM battles";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

    echo "<form method=\"POST\" action=\"" . getPHPSelf() . "\">";
    echo "<table width=\"825\" border=\"0\" class=\"alt\">";
    echo "<tr><td width=\"350\"><b>Battle</b></td>
            <td width=\"200\"><b>Assigned to</b></td>
            <td width=\"150\"><b>Assignment</b></td>
            <td width=\"100\"><b>Deadline</b></td>
            <td width=\"25\">&nbsp;</td>
        </tr>";

    $type = array("", "$D_TestEasy", "$D_TestMedium", "$D_TestHard", "$D_Correct");
    $desc = array("", "Test on Easy", "Test on Medium", "Test on Hard", "Correct Battle");
    $lvl = array("", "easy", "medium", "hard", "correct");

    while ($battle = mysqli_fetch_row($result)) {
      $j = 1;
      while ($j <= 4) {
        if ($battle[$type[$j]] != 0) {
          $query = "SELECT * FROM roster WHERE N_PIN = '" . $battle[$type[$j]] . "'";
          $output = mysqli_query($GLOBALS["___mysqli_ston"], $query);
          $roster = mysqli_fetch_row($output);

          /* hardest of all : find out when the assignment was set  */

          $temp = split("�", $battle[$D_History]);

          /*  start searching at the end.....  */

          $k = count($temp);
          while ($k > 0) {
            /*  new let`s get the name and the assignment and see if both appear in $item  */
            /*  when found, set $k to -1 so the loop will stop!  */

            $name = $roster[$N_Name];
            if (strpos($temp[$k - 1], $name) && strpos($temp[$k - 1], $lvl[$j])) {
              $items = explode($DELIM, $temp[$k - 1]);
              $datum = date("M d Y", $items[0] + 1209600);
              $k = 0;
            }
            $k--;
          }
          $batt = $battle[$B_Name];
          /*  if (strlen($battle[$B_Name]) >= 33) {
          $batt = substr($battle[$B_Name],0,30);
          if ($batt != $battle[$B_Name]) { $batt = $batt."..."; }
        }
        else { $batt = $battle[$B_Name]; } */
          echo "<tr><td>" . $batt . "</td>
                  <td><a href=\"mailto:" . $roster[$N_EMail] . "\">" . $ranks[$roster[$N_Rank]] . " " . $roster[$N_Name] . "</a></td>
                  <td>" . $desc[$j] . "</td>
                  <td>" . $datum . "</td>
                  <td align=\"center\"><input type=\"checkbox\" name=\"delete[]\" value=\"" . $roster[$N_PIN] . "-" . $lvl[$j] . "-" . $battle[$B_ID] . "\"></td>
              </tr>";
        }
        $j++;
      }
    }
    echo "<tr><td valign=\"center\">&nbsp;</td><td colspan=\"4\" valign=\"center\"><br><input type=\"submit\" value=\"delete checked assignments\" name=\"submit\"></td></tr>";
    echo "</table>";
    echo "</form>";
  } else {
    echo "\n<p class=\"text\">You have no access to this page.</p>";
  }
  ?>