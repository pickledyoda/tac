<?php
session_start();
?>

<html>

<head>
  <link rel="stylesheet" type="text/css" href="../tac.css">
</head>

<body>
  <font class="text">
    <?php

    function bu($table)
    {
      include "../tac/dbstuff.tac";
      ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
      ((bool) mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));

      $fname = "backup/" . $table . ".txt";
      if ($table == "roster") {
        $max = 9;
      }
      if ($table == "battles") {
        $max = 18;
      }
      if ($table == "reports") {
        $max = 5;
      }
      $query = "SELECT * FROM " . $table;
      $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
      $fh = fopen($fname, 'w');
      while ($data = mysqli_fetch_row($result)) {
        $i = 0;
        unset($line);
        while ($i <= $max) {
          $line = $line . "�" . $data[$i];
          $i++;
        }
        $line = $line . "���";
        fwrite($fh, $line);
      }
      fclose($fh);
      echo "\n<br>Finished backing up table " . $table . " to file " . $fname . "!";
    }

    function ru($table)
    {
      include "../tac/dbstuff.tac";
      $link = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
      ((bool) mysqli_query($link, "USE " . $dbname));

      $fname = "backup/" . $table . ".txt";
      if ($table == "roster") {
        $max = 9;
      }
      if ($table == "battles") {
        $max = 18;
      }
      if ($table == "reports") {
        $max = 5;
      }

      $fh = fopen($fname, 'r');
      $content = fread($fh, filesize($fname));
      $lines = explode("���", $content);
      $i = 0;
      $j = count($lines) - 1;
      while ($i < $j) {

        $data = explode($DELIM, $lines[$i]);
        if ($table == "roster") {
          $query = "INSERT INTO roster (N_ID, N_Position, N_Name, N_PIN, N_Subgroup, N_Email, N_Pts, N_Rank, N_Password, N_Platform) VALUES ('$data[1]','$data[2]','$data[3]','$data[4]','$data[5]','$data[6]','$data[7]','$data[8]','$data[9]','$data[10]')";
        }
        if ($table == "battles") {
          $query = "INSERT INTO battles (B_ID, B_Name, B_Type, B_Platform, B_Medal, B_Missions, B_File, B_Patches, B_Comments, B_Author, B_PIN, B_EMail, D_Status, D_TestEasy, D_TestMedium, D_TestHard, D_Correct, D_History, D_Corrected) values ('$data[1]', '$data[2]', '$data[3]', '$data[4]', '$data[5]', '$data[6]', '$data[7]', '$data[8]', '$data[9]', '$data[10]', '$data[11]', '$data[12]', '$data[13]', '$data[14]', '$data[15]', '$data[16]', '$data[17]', '$data[18]', '$data[19]')";
        }
        if ($table == "reports") {
          $query = "INSERT INTO reports (R_ID, R_Battle, R_Level, R_Tester, R_Status, ', R_Reason) VALUES ('$data[1]','$data[2]','$data[3]','$data[4]','$data[5]','$data[6]','$data[7]')";
        }
        $result = mysqli_query($link, $query);
        echo "<p>" . ((is_object($link)) ? mysqli_errno($link) : (($___mysqli_res = mysqli_connect_errno()) ? $___mysqli_res : false)) . ": " . ((is_object($link)) ? mysqli_error($link) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)) . "\n";

        $i++;
      }
      fclose($fh);

      echo "\n<br>Finished restoring table " . $table . " from file " . $fname . "!";
    }


    /* no use logging in twice is there... */
    if (isAuthed()) {

      include "../tac/dbstuff.tac";
      include "../includes/phpself.php";
      ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
      ((bool) mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));

      /* if the `make backup` has been clicked... */

      if (isset($_POST['submit1'])) {
        bu('roster');
      } elseif (isset($_POST['submit2'])) {
        bu('battles');
      } elseif (isset($_POST['submit3'])) {
        bu('reports');
      } elseif (isset($_POST['submit4'])) {
        bu('roster');
        bu('battles');
        bu('reports');
      } elseif (isset($_POST['submit5'])) {
        ru('roster');
      } elseif (isset($_POST['submit6'])) {
        ru('battles');
      } elseif (isset($_POST['submit7'])) {
        ru('reports');
      } elseif (isset($_POST['submit8'])) {
        ru('roster');
        ru('battles');
        ru('reports');
      } else {
        echo "Database Backup Utilities!";
        echo "<p>";
        echo "\n<form method=\"POST\" action=\"" . getPHPSelf() . "\">";
        echo "\n<table width=\"500\">";
        echo "\n<tr><td width=\"250\">Backup</td><td width=\"250\" class=\"restorebg\">Restore</td></tr>";
        echo "\n<tr><td width=\"250\"><input type=\"submit\" value=\"Roster\" name=\"submit1\"></td><td width=\"250\" class=\"restorebg\"><input type=\"submit\" value=\"Roster\" name=\"submit5\"></td></tr>";
        echo "\n<tr><td width=\"250\"><input type=\"submit\" value=\"Battles\" name=\"submit2\"></td><td width=\"250\" class=\"restorebg\"><input type=\"submit\" value=\"Battles\" name=\"submit6\"></td></tr>";
        echo "\n<tr><td width=\"250\"><input type=\"submit\" value=\"Reports\" name=\"submit3\"></td><td width=\"250\" class=\"restorebg\"><input type=\"submit\" value=\"Reports\" name=\"submit7\"></td></tr>";
        echo "\n<tr><td width=\"250\"><input type=\"submit\" value=\"All Tables\" name=\"submit4\"></td><td width=\"250\" class=\"restorebg\"><input type=\"submit\" value=\"All Tables\" name=\"submit8\"></td></tr>";
        echo "\n</table>";
        echo "\n</form></p>";
      }
    } else {
      echo "\n<p class=\"text\">You have no access to this page.</p>";
    }
    ?>