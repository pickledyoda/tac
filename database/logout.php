<?php
session_start();
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../tac.css">
<META http-equiv="refresh" content="1;URL=admin.php">
</head>
<body>
<font class="text">
<?php
/* no use logging in twice is there... */
if (isAuthed()) {

  session_destroy();

  echo "<br>You are now logged out. thank you for your visit.";

}
?>