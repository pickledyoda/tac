<?php
session_start();
?>

<html>
<head>
	<?php
	include "../tac/dbstuff.tac";
	include "../includes/phpself.php";
	?>
	<link rel="stylesheet" type="text/css" href="../tac.css">
</head>
<body>
<?php
if (isAuthed()) {

	$ID = $_GET['id'];

	if (isset($_POST['submit'])) {

		/* update battle history and unset player assignment  */

		($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost, $dbusername, $dbpassword)) or die("Unable to connect to database");
		((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
		$query  = "SELECT * FROM battles WHERE B_ID = '" . $ID . "'";
		$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
		$battle = mysqli_fetch_row($result);

		$creditbeta = $battle[$B_Missions];

		$query  = "UPDATE battles SET D_History = '" . $battle[$D_History] . "�" . date('U') . "�beta report uploaded by " . $_SESSION['name'] . " (level " . $_POST['level'] . ")' where B_ID = '" . $ID . "'";
		$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

		/* add report to database */
		/* what we need:
			 - separate different fields with a �
			 - start a new section with a �
			 - separate section name and items with a �
		*/

		/* add the setting for the radiobuttons */
		$report = "�BATTLE INFO�" . $_POST['readme'] . "�" . $_POST['medal'] . "�" . $_POST['story'] . "�" . $_POST['updates'] . "�" . $_POST['patches'] . "�" . $_POST['install'];

		/* add the checkboxes */
		$report = $report . "�" . $_POST['patchesneeded'] . "�" . $_POST['installpresent'];

		/* add the textareas */
		/* just in case, replace � in textareas with � */
		$report = $report . "�" . nl2br(addslashes(str_replace("�", "�", $_REQUEST['storyprobs']))) . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST['readmeprobs']))) . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST['patchesprobs']))) . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST['updatesprobs'])));

		/* XWA only: crest */
		if ($battle[$B_Platform] == "XWA") {
			$report = $report . "�" . $_POST['crest'];
		}

		if ($battle[$B_Platform] == "TIE" || $battle[$B_Platform] == "XvT" || $battle[$B_Platform] == "XW" || $battle[$B_Platform] == "BoP" || $battle[$B_Platform] == "XWA") {

			/* add radios and textareas for each mission */
			/* just in case, replace � in textareas with � */
			$j = 1;
			while ($j <= $battle[$B_Missions]) {
				/* TIE */
				$preofficer    = "preofficer" . $j;
				$postofficer   = "postofficer" . $j;
				$presecret     = "presecret" . $j;
				$postsecret    = "postsecret" . $j;
				$officererrors = "officererrors" . $j;
				$secreterrors  = "secreterrors" . $j;
				/* XvT */
				$briefing       = "briefing" . $j;
				$voice          = "voice" . $j;
				$briefingerrors = "briefingerrors" . $j;
				/* XWA */
				$opening         = "opening" . $j;
				$commander       = "commander" . $j;
				$hint            = "hint" . $j;
				$debrief         = "debrief" . $j;
				$voice           = "voice" . $j;
				$openingerrors   = "openingerrors" . $j;
				$commandererrors = "commandererrors" . $j;
				$hinterrors      = "hinterrors" . $j;
				$debrieferrors   = "debrieferrors" . $j;
				/* XW */
				$brf       = "briefing" . $j;
				$brferrors = "briefingerrors" . $j;
				/* all */
				$animated       = "animated" . $j;
				$content        = "content" . $j;
				$inflight       = "inflight" . $j;
				$animatederrors = "animatederrors" . $j;
				$inflighterrors = "inflighterrors" . $j;
				$gameplayerrors = "gameplayerrors" . $j;
				$comments       = "comments" . $j;

				/* common */
				$report = $report . "�MISSION " . $j . " INFO�";

				/* per platform briefings */
				if ($battle[$B_Platform] == "TIE") {
					$report = $report . str_replace("�", "�", $_POST[$preofficer]) . "�" . str_replace("�", "�", $_POST[$postofficer]) . "�" . str_replace("�", "�", $_POST[$presecret]) . "�" . str_replace("�", "�", $_POST[$postsecret]);
				}
				if ($battle[$B_Platform] == "XvT") {
					$report = $report . str_replace("�", "�", $_POST[$briefing]);
				}
				if ($battle[$B_Platform] == "XWA") {
					$report = $report . str_replace("�", "�", $_POST[$opening]) . "�" . str_replace("�", "�", $_POST[$commander]) . "�" . str_replace("�", "�", $_POST[$hint]) . "�" . str_replace("�", "�", $_POST[$debrief]);
				}
				if ($battle[$B_Platform] == "XW") {
					$report = $report . str_replace("�", "�", $_POST[$brf]);
				}

				/* common */
				$report = $report . "�" . str_replace("�", "�", $_POST[$animated]) . "�" . str_replace("�", "�", $_POST[$content]) . "�" . str_replace("�", "�", $_POST[$inflight]);

				/* XvT and XWA */
				if ($battle[$B_Platform] == "XvT" || $battle[$B_Platform] == "XWA") {
					$report = $report . "�" . $_POST[$voice];
				}

				/* per platform briefing errors */
				if ($battle[$B_Platform] == "TIE") {
					$report = $report . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$officererrors]))) . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$secreterrors])));
				}
				if ($battle[$B_Platform] == "XvT") {
					$report = $report . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$briefingerrors])));
				}
				if ($battle[$B_Platform] == "XWA") {
					$report = $report . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$openingerrors]))) . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$commandererrors]))) . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$hinterrors]))) . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$debrieferrors])));
				}
				if ($battle[$B_Platform] == "XW") {
					$report = $report . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$brferrors])));
				}

				/* common */
				$report = $report . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$animatederrors]))) . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$inflighterrors]))) . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$gameplayerrors]))) . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$comments])));

				$j++;
			}
		}

		if ($battle[$B_Platform] == "SWGB") {

			/* add radios and textareas for each mission */
			/* just in case, replace � in textareas with � */
			$j = 1;
			while ($j <= $battle[$B_Missions]) {
				/* SWGB */
				$pre         = "pre" . $j;
				$post        = "post" . $j;
				$brieferrors = "brieferrors" . $j;
				/* all */
				$animated       = "animated" . $j;
				$content        = "content" . $j;
				$animatederrors = "animatederrors" . $j;
				$gameplayerrors = "gameplayerrors" . $j;
				$comments       = "comments" . $j;

				/* common */
				$report = $report . "�MISSION " . $j . " INFO�";

				/* per platform briefings */
				if ($battle[$B_Platform] == "SWGB") {
					$report = $report . str_replace("�", "�", $_POST[$pre]) . "�" . str_replace("�", "�", $_POST[$post]);
				}

				/* common */
				$report = $report . "�" . str_replace("�", "�", $_POST[$animated]) . "�" . str_replace("�", "�", $_POST[$content]);

				/* per platform briefing errors */
				if ($battle[$B_Platform] == "SWGB") {
					$report = $report . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$brieferrors])));
				}

				/* common */
				$report = $report . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$animatederrors]))) . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$gameplayerrors]))) . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST[$comments])));

				$j++;
			}
		}

		/* add final thoughts */
		$report = $report . "�FINAL THOUGHTS�" . $_POST['rate'] . "�" . str_replace("�", "�", nl2br(addslashes($_REQUEST['finalcomment'])));

		($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost, $dbusername, $dbpassword)) or die("Unable to connect to database");
		((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));

		/*  hooo - if this battle id and level are already in for this PIN, it's a corrected report, and status = 4, otherwise it's new and status = 1  */
		$query  = "SELECT * FROM reports WHERE R_Battle = '" . $ID . "'  and R_Level = '" . $_POST['level'] . "' and R_Tester = '" . $_SESSION['pin'] . "'";
		$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
		$temp   = mysqli_fetch_row($result);

		if ($temp[$R_ID]) {
			$newstat = 4;
		} else {
			$newstat = 1;
		}

		/*  backup purposes - replace */
		$query   = "INSERT INTO reports (R_Battle, R_Level, R_Tester, R_Status, R_Report, R_Reason) VALUES ('" . $ID . "','" . $_POST['level'] . "','" . $_SESSION['pin'] . "','" . $newstat . "','" . $report . "','0')";
		$result  = mysqli_query($GLOBALS["___mysqli_ston"], $query);
		$mailrsn = "newbeta";
		include "mailform.php";

		echo "<font class=\"text\">Your report has been received. thank you";

	} // end submit
	else {

		/* get battle and tester stats for use */

		($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost, $dbusername, $dbpassword)) or die("Unable to connect to database");
		((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
		$query  = "SELECT * FROM battles WHERE B_ID = '" . $ID . "'";
		$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
		$battle = mysqli_fetch_row($result);

		($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost, $dbusername, $dbpassword)) or die("Unable to connect to database");
		((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
		$query  = "SELECT * FROM roster WHERE N_PIN ='" . $_SESSION['pin'] . "'";
		$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
		$tester = mysqli_fetch_row($result);

		echo "\n<font class=\"tacmantitle\">Beta Test Report Submission Form</font><p>";

		echo "\n<form method=\"POST\" action=\"" . getPHPSelf() . "?id=" . $ID . "\">";
		echo "\n<table width=\"700\">";

		/* display battle/tester stats here */
		echo "\n<tr><td width=\"700\" colspan=\"2\" class=\"tacmantitle\">GENERAL INFORMATION</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Battle Name:</b></td><td width=\"400\" class=\"text\" valign=\"top\">" . $battle[$B_Name] . "</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Battle Type:</b></td><td width=\"400\" class=\"text\" valign=\"top\">" . $battle[$B_Platform] . " " . $battle[$B_Type] . "</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Tester's Name:</b></td><td width=\"400\" class=\"text\" valign=\"top\">" . $ranks[$tester[$N_Rank]] . " " . $tester[$N_Name] . " (" . $tac[$tester[$N_Position]] . ")</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Tester's PIN:</b></td><td width=\"400\" class=\"text\" valign=\"top\">" . $tester[$N_PIN] . "</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Tester's e-Mail:</b></td><td width=\"400\" class=\"text\" valign=\"top\">" . $tester[$N_EMail] . "</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Test Level:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"level\" value=\"easy\"";
		if ($battle[$D_TestEasy] == $_SESSION['pin']) {
			echo " checked";
		}
		echo "> Easy &nbsp; &nbsp; <input type=\"radio\" name=\"level\" value=\"medium\"";
		if ($battle[$D_TestMedium] == $_SESSION['pin']) {
			echo " checked";
		}
		echo "> Medium &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"level\" value=\"hard\"";
		if ($battle[$D_TestHard] == $_SESSION['pin']) {
			echo " checked";
		}
		echo "> Hard</td></tr>";

		echo "\n<tr><td width=\"700\" colspan=\"2\"><hr></td></tr>";

		/* general input section */
		echo "\n<tr><td width=\"700\" colspan=\"2\" class=\"tacmantitle\">REPORT ON THE BATTLE</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Readme File:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"readme\" value=\"yes\"> Yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"readme\" value=\"no\"> No</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Medal Name:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"medal\" value=\"yes\"> Yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"medal\" value=\"no\"> No</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Plot/Storyline:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"story\" value=\"yes\"> Yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"story\" value=\"no\"> No</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Other Text Files:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"updates\" value=\"yes\"> Yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"updates\" value=\"no\"> No</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Patches Needed:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"patches\" value=\"yes\"> Yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"patches\" value=\"no\"> No &nbsp; &nbsp; &nbsp; <input type=\"checkbox\" name=\"patchesneeded\" value=\"yes\"> patches.txt Present</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Installation Instructions Needed:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"install\" value=\"yes\"> Yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"install\" value=\"no\"> No &nbsp; &nbsp; &nbsp; <input type=\"checkbox\" name=\"installpresent\" value=\"yes\"> Installation Instructions Present</td></tr>";

		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Plot/Storyline Problems:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"storyprobs\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Readme Problems:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"readmeprobs\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Patches Problems:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"patchesprobs\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Other Text Problems:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"updatesprobs\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
		if ($battle[$B_Platform] == "XWA") {
			echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Custom EH Crest:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"crest\" value=\"yes\"> Yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"crest\" value=\"no\"> No</td></tr>";
		}

		/* per mission report */

		$j = 1;

		if ($battle[$B_Platform] == "TIE") {
			while ($j <= $battle[$B_Missions]) {
				echo "\n<tr><td width=\"700\" colspan=\"2\"><hr></td></tr>";
				echo "\n<tr><td width=\"700\" colspan=\"2\" class=\"tacmantitle\">REPORT ON MISSION " . $j . "</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>pre-mission officer briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"preofficer" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"preofficer" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>post-mission officer briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"postofficer" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"postofficer" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>pre-mission secret order briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"presecret" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"presecret" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>post-mission secret order briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"postsecret" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"postsecret" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"animated" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"animated" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>appropriate content:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"content" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"content" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"inflight" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"inflight" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>officer briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"officererrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>secret order errors::</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"secreterrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing errors::</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"animatederrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"inflighterrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>gameplay errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"gameplayerrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>additional comments:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"comments" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				$j++;
			}
		}

		if ($battle[$B_Platform] == "XW") {
			while ($j <= $battle[$B_Missions]) {
				echo "\n<tr><td width=\"700\" colspan=\"2\"><hr></td></tr>";
				echo "\n<tr><td width=\"700\" colspan=\"2\" class=\"tacmantitle\">REPORT ON MISSION " . $j . "</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>pre-mission briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"preofficer" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"preofficer" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>post-mission briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"postofficer" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"postofficer" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"animated" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"animated" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>appropriate content:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"content" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"content" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"inflight" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"inflight" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>officer briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"officererrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing errors::</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"animatederrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"inflighterrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>gameplay errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"gameplayerrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>additional comments:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"comments" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				$j++;
			}
		}

		if ($battle[$B_Platform] == "XvT" || $battle[$B_Platform] == "BoP") {
			while ($j <= $battle[$B_Missions]) {
				echo "\n<tr><td width=\"700\" colspan=\"2\"><hr></td></tr>";
				echo "\n<tr><td width=\"700\" colspan=\"2\" class=\"tacmantitle\">REPORT ON MISSION " . $j . "</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>mission text:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"briefing" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"briefing" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"animated" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"animated" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>appropriate content:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"content" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"content" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"inflight" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"inflight" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>custom voice addon messages:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"voice" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"voice" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>opening text errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"briefingerrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing errors::</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"animatederrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"inflighterrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>gameplay errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"gameplayerrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>additional comments:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"comments" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				$j++;
			}
		}

		if ($battle[$B_Platform] == "XWA") {
			while ($j <= $battle[$B_Missions]) {
				echo "\n<tr><td width=\"700\" colspan=\"2\"><hr></td></tr>";
				echo "\n<tr><td width=\"700\" colspan=\"2\" class=\"tacmantitle\">REPORT ON MISSION " . $j . "</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>opening briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"opening" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"opening" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>commander briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"commander" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"commander" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>failed mission hints:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"hint" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"hint" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>post-mission briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"debrief" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"debrief" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"animated" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"animated" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>appropriate content:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"content" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"content" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"inflight" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"inflight" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>custom voice addon messages:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"voice" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"voice" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>opening briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"openingerrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>commander briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"commandererrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>failed mission hints errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"hinterrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>post-mission briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"debrieferrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing errors::</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"animatederrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"inflighterrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>gameplay errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"gameplayerrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>additional comments:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"comments" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				$j++;
			}
		}

		if ($battle[$B_Platform] == "SWGB") {
			while ($j <= $battle[$B_Missions]) {
				echo "\n<tr><td width=\"700\" colspan=\"2\"><hr></td></tr>";
				echo "\n<tr><td width=\"700\" colspan=\"2\" class=\"tacmantitle\">REPORT ON MISSION " . $j . "</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>pre-mission briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"pre" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"pre" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>post-mission briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"post" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"post" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"animated" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"animated" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>appropriate content:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"content" . $j . "\" value=\"yes\"> yes &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"content" . $j . "\" value=\"no\"> no</td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"brieferrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing errors::</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"animatederrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>gameplay errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"gameplayerrors" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>additional comments:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"comments" . $j . "\" rows=\"4\" cols=\"45\"></textarea></td></tr>";
				$j++;
			}
		}

		/* final thoughts */
		echo "\n<tr><td width=\"700\" colspan=\"2\"><hr></td></tr>";
		echo "\n<tr><td width=\"700\" colspan=\"2\" class=\"tacmantitle\">FINAL THOUGHTS ON THIS BATTLE</td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>rating:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><input type=\"radio\" name=\"rate\" value=\"1\"> 1 &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"rate\" value=\"2\"> 2 &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"rate\" value=\"3\"> 3 &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"rate\" value=\"4\"> 4 &nbsp; &nbsp; &nbsp; <input type=\"radio\" name=\"rate\" value=\"5\"> 5  </td></tr>";
		echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>additional overall comments:</b></td><td width=\"400\" class=\"text\" valign=\"top\"><textarea name=\"finalcomment\" rows=\"4\" cols=\"45\"></textarea></td></tr>";

		echo "\n<tr><td width=\"300\" valign=\"center\"><p><input type=\"submit\" value=\"submit report\" name=\"submit\"></td><td width=\"400\">&nbsp;</td></tr>";
		echo "\n</table>";
		echo "\n</form>";

	} // end else

} else {
	echo "<p class=\"text\">You have no access to this page.</font>";
}
?>
</body>
</html>
