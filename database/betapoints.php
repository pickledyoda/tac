<?php
session_start();
?>

<html>

<head>
  <link rel="stylesheet" type="text/css" href="../tac.css">
</head>

<body>

  <?php

  /* no use logging in twice is there... */
  if (isAuthed()) {

    include "../tac/dbstuff.tac";
    include "../includes/phpself.php";

    /* if the `submit changes`  has been clicked... */

    if (isset($_POST['submit'])) {

      ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
      ((bool) mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
      $query = "SELECT * FROM roster";
      $temp = mysqli_query($GLOBALS["___mysqli_ston"], $query);
      while ($old = mysqli_fetch_row($temp)) {
        $tmp = "pin" . $old[$N_PIN];
        $newpts = $_POST[$tmp];
        if ($old[$N_Points] != $newpts) {
          $query = "UPDATE roster SET N_Pts = '" . $newpts . "' WHERE N_PIN = '" . $old[$N_PIN] . "'";
          $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
          echo "\n<font class=\"text\"><br>The beta points count for " . $ranks[$old[$N_Rank]] . " " . $old[$N_Name] . " has been updated from " . $old[$N_Points] . " to " . $newpts . ".</font>";
        }
      }
      echo "\n<p><a href=\"admin.php\">Back to admin page</a></p>";
    }

    /* else just display the stats */ else {

      /* step one: display all beta points in a form, and save them in a variable so we can check
         later which have been changed. that way, we only update the changed ones... */

      ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
      ((bool) mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
      $query = "SELECT * FROM roster ORDER BY N_Position,N_Rank,N_Name";
      $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

      echo "\n<form method=\"POST\" action=\"" . getPHPSelf() . "\">";

      echo "\n<table width=\"575\" class=\"alt\">";
      echo "\n<tr><td width=\"75\" bgcolor=\"#555555\" class=\"text\" align=\"center\"><font color=\"#FFFFFF\"> <b> PIN</b></font></td>";
      echo "\n<td width=\"225\" bgcolor=\"#555555\" class=\"text\"><font color=\"#FFFFFF\"> <b> Name</b></font></td>";
      echo "\n<td width=\"75\" bgcolor=\"#555555\" class=\"text\" align=\"center\"><font color=\"#FFFFFF\"> <b> Position</b></font></td>";
      echo "\n<td width=\"75\" bgcolor=\"#555555\" class=\"text\" align=\"center\"><font color=\"#FFFFFF\"> <b> Points</b></font></td>";
      echo "\n</tr>";
      while ($staff = mysqli_fetch_row($result)) {
        if ($staff[$N_Position] < 7) {
          echo "\n<tr><td class=\"text\" align=\"center\">" . $staff[$N_PIN] . "</td>";
          echo "<td class=\"text\"> " . $ranks[$staff[$N_Rank]] . " " . $staff[$N_Name] . "</td>";
          echo "<td class=\"text\" align=\"center\">" . $tac[$staff[$N_Position]] . "</td>";
          echo "<td class=\"text\" align=\"center\"><input type=\"text\" name=\"pin" . $staff[$N_PIN] . "\" value=\"" . $staff[$N_Points] . "\" size=\"5\"></td>";
          echo "\n</tr>";
        }
      }
      $query = "SELECT * FROM roster ORDER BY N_Name";
      $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
      while ($staff = mysqli_fetch_row($result)) {
        if ($staff[$N_Position] == 7) {
          echo "\n<tr><td class=\"text\" align=\"center\">" . $staff[$N_PIN] . "</td>";
          echo "<td class=\"text\"> " . $ranks[$staff[$N_Rank]] . " " . $staff[$N_Name] . "</td>";
          echo "<td class=\"text\" align=\"center\">" . $tac[$staff[$N_Position]] . "</td>";
          echo "<td class=\"text\" align=\"center\"><input type=\"text\" name=\"pin" . $staff[$N_PIN] . "\" value=\"" . $staff[$N_Points] . "\" size=\"5\"></td>";
          echo "\n</tr>";
        }
      }
      echo "\n</table>";
      echo "\n<input type=\"submit\" value=\"Submit Changes\" name=\"submit\">";
      echo "\n</form>";
    }
  } else {
    echo "\n<p class=\"text\">You have no access to this page.</p>";
  }
  ?>