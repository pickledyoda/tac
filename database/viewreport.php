<?php
session_start();
?>

<html>
<head>
<?php
include "../tac/dbstuff.tac";
include "../includes/phpself.php";
?>
<link rel="stylesheet" type="text/css" href="../tac.css">
</head>
<body>
<?php
if (isAuthed()) {
  echo "<font class=\"text\">";

  $BID = $_GET['id'];
  $level = $_GET['level'];

  $link1 = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
  ((bool)mysqli_query($link1, "USE " . $dbname));
  $query = "SELECT * FROM reports WHERE R_ID = '".$BID."'";
  $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
  $report = mysqli_fetch_row($result);

  $link2 = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
  ((bool)mysqli_query($link2, "USE " . $dbname));
  $query = "SELECT * FROM roster WHERE N_PIN = '".$report[$R_Tester]."'";
  $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
  $tester = mysqli_fetch_row($result);

  $link3 = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
  ((bool)mysqli_query($link3, "USE " . $dbname));
  $query = "SELECT * FROM battles WHERE B_ID = '".$report[$R_Battle]."'";
  $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
  $battle = mysqli_fetch_row($result);

  if (isset($_POST['submit1'])) {
    echo "This beta report has been approved. ".$ranks[$tester[$N_Rank]]." ".$tester[$N_Name]." has been credited with ".$battle[$B_Missions]." beta points.";
//    $mailrsn = "betaccept";
//    include "mailform.php";

    $query = "SELECT * FROM reports WHERE R_ID = '".$report[$R_ID]."'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $query = "UPDATE reports SET R_Status = '2' WHERE R_ID='".$report[$R_ID]."'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $field = "D_Test".strtoupper(substr($level,0,1)).substr($level,1);
    $query = "UPDATE battles SET $field = '0', D_History = '".$battle[$D_History].$DELIM.date('U')."�beta testreport from ".$ranks[$tester[$N_Rank]]." ".$tester[$N_Name]." approved' where B_ID = '".$report[$R_Battle]."'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $newpts = $tester[$N_Points] + $battle[$B_Missions];
    $query = "UPDATE roster SET N_Pts = '".$newpts."' WHERE N_PIN = '".$report[$R_Tester]."'";
   $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
  }

  elseif (isset($_POST['submit2'])) {
    echo "This beta report has been denied.";
    echo "<br>The TCS has been notified via email.";
    $mailrsn = "betadeny";
    include "mailform.php";

    ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
    $query = "SELECT * FROM reports WHERE R_ID = '".$report[$R_ID]."'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $query = "UPDATE reports SET R_Status = '3', R_Reason = '".nl2br(addslashes($_REQUEST['reason']))."' WHERE R_ID='".$report[$R_ID]."'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

    $field = "D_Test".strtoupper(substr($report[$R_Level], 0, 1)).substr($report[$R_Level], -3, 3);
    $query = "UPDATE battles SET ".$field." = '".$report[$R_Tester]."', D_History = '".$battle[$D_History].$DELIM.date('U')."�beta testreport from ".$ranks[$tester[$N_Rank]]." ".$tester[$N_Name]." denied (".nl2br(addslashes($_REQUEST['reason'])).") and re-assigned' where B_ID = '".$report[$R_Battle]."'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
  }
  else {
    $BID = $_GET['id'];
    $level = $_GET['level'];

    ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
    $query = "SELECT * FROM reports WHERE R_ID = '".$BID."'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $report = mysqli_fetch_row($result);

    $query = "SELECT * FROM roster WHERE N_PIN = '".$report[$R_Tester]."'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $tester = mysqli_fetch_row($result);

    $query = "SELECT * FROM battles WHERE B_ID = '".$report[$R_Battle]."'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $battle = mysqli_fetch_row($result);

    /*  status 1: display only to TAC and submitter

             (1) submitted by TCS     - available to TCS/TAC			TCS = 1
             (2) approved by TAC      - available to TCT/TAC			TCT = 2
             (3) rejected by TAC      - available to TCS/TAC			TAC = 3
             (4) corrected by TCS     - available to TCS/TAC
    */

    if ($report[$R_Status] == "1" || $report[$R_Status] == "3" || $report[$R_Status] == "4") {
      if ($_SESSION['pin'] == 12945 || $tac[$_SESSION['position']] == "TAC" || $_SESSION['pin'] == $report[$R_Tester]) { $show = 1; }
      else { $show = 2; }
    }
    elseif ($report[$R_Status] == "2") {
      if ($_SESSION['pin'] == 12945 ||  $tac[$_SESSION['position']] == "TAC" || $battle[$D_Correct] == $_SESSION['pin']) { $show = 1; }
      else { $show = 2; }
    }
    if ($_GET['me'] == md5($report[$R_ID])) { $show = 1; }

    if ($show == 1) {
      /* this is the hard part: get the report and chop it up into nice easy little pieces.... */
      $sections = explode($DELIM,$report[$']);

      /* create a form, in case you`re TAC to approve or deny */

      if ($_SESSION['pin'] == 12945 ||  $tac[$_SESSION['position']] == "TAC" && $report[$R_Status] != "2") { echo "\n<form method=\"POST\" action=\"".getPHPSelf()."?id=".$BID."&level=".$report[$R_Level]."\">"; }

      echo "\n<table width=\"700\">";
      echo "\n<tr><td width=\"700\" colspan=\"2\" class=\"tacmantitle\">GENERAL INFORMATION</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Battle name:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$battle[$B_Name]."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Battle type:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$battle[$B_Platform]." ".$battle[$B_Type]."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Tester's name:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$ranks[$tester[$N_Rank]]." ".$tester[$N_Name]." (".$tac[$tester[$N_Position]].")</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Tester's e-mail:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$tester[$N_EMail]."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Test level:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$report[$R_Level]."</td></tr>";

      /* section 1: battle info, chop it up */
      /* format for section 1:

        0	readme file present
        1	medal name present
        2	storyline present
        3	other txt present
        4	patches needed
        5	install instructions needed
        6	patches.txt present
        7	installation instructions present
        8	storyline problems
        9	readme problems
        10	patches  problems
        11	other problems
  	   XWA only:
        12   EH crest present
      */

      $content = explode($DELIM,$sections[1]);
      $items = explode($DELIM,$content[1]);

      echo "\n<tr><td width=\"700\" colspan=\"2\"><hr></td></tr>";
      echo "\n<tr><td width=\"700\" colspan=\"2\" class=\"tacmantitle\"><p>REPORT ON THE BATTLE</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Readme file:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[0]."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Medal name:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[1]."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>Plot/storyline:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[2]."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>updates.txt:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[3]."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>patches needed:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[4]."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>other txt present:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[6]."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>installation instructions needed:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[5]."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>installation instructions present:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[7]."</td></tr>";
      if ($battle[$B_Platform] == "XWA") { echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>custom EH crest:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[12]."</td></tr>"; }

      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>plot/storyline problems:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[8])."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>readme problems:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[9])."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>patches problems:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[10])."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>updates problems:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[11])."</td></tr>";

      /* section 2: mission info, chop it up */
      /* format for section 2:

			TIE								XVT/BOP						XWA						SWGB                          XW


 		0	preofficer briefing				briefing					opening text			pre-mission briefing
		1	postofficer briefing			animated briefing			commander briefing		post-mission briefing
		2	presecretorder briefing			appropriate content			mission hints			animated briefing
		3	postsecretorderbriefing			inflight msgs				debriefing				appropriate content
		4	animated briefing				custom voice msgs			animated briefing		briefing errors
		5	appropriate content				briefing errors				appropriate content		animated errors
		6	inflight msgs					animated errors				inflight msgs			gameplay errors
		7	officer errors					inflight errors				custom voice msgs		comments
		8	secret errors					gameplay errors				opening errors
		9	animated errors					comments					commander errors
		10	inflight errors												hint errors
		11	gameplay errors												debriefing errors
		12	comments													animated errors
		13																inflight errors
		14																gameplay errors
		15																comments
      */

      $j = 1;
      while ($j <= $battle[$B_Missions]) {

        $content = explode($DELIM,$sections[$j+1]);
        $items = explode($DELIM,$content[1]);

        echo "\n<tr><td width=\"700\" colspan=\"2\"><hr></td></tr>";
        echo "\n<tr><td width=\"700\" colspan=\"2\" class=\"tacmantitle\">REPORT ON MISSION ".$j."</td></tr>";
        if ($battle[$B_Platform] == "TIE") {
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>pre-mission officer briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[0]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>post-mission officer briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[1]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>pre-mission secret order briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[2]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>post-mission secret order briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[3]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[4]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>appropriate content:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[5]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[6]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>officer briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[7])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>secret order errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[8])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[9])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[10])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>gameplay errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[11])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>additional comments:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[12])."</td></tr>";
        } // end TIE
        if ($battle[$B_Platform] == "XvT") {
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>mission briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[0]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[1]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>appropriate content:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[2]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[3]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>custom voice addon messages:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[4]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[5])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[6])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[7])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>gameplay errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[8])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>additional comments:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[9])."</td></tr>";
        } // end XvT
        if ($battle[$B_Platform] == "XWA") {
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>opening briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[0]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>commander briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[1]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>failed mission hints:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[2]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>post-mission briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[3]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[4]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>appropriate content:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[5]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[6]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>custom voice addon messages:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[7]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>opening briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[8])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>commander briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[9])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>failed mission hints errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[10])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>post-mission briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[11])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[12])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>in-flight messages errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[13])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>gameplay errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[14])."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>additional comments:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[15])."</td></tr>";
        } // end XWA
        if ($battle[$B_Platform] == "SWGB") {
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>pre-mission briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[0]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>post-mission briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[1]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[2]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>appropriate content:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[3]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[4]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>animated briefing errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[5]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>gameplay errors:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[6]."</td></tr>";
          echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>additional comments:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[7]."</td></tr>";
        } // end SWGB
        $j++;
      } // end WHILE

      /* section 3: final comments, chop it up */

      $content = explode($DELIM,$sections[$j+1]);
      $items = explode($DELIM,$content[1]);
      echo "\n<tr><td width=\"700\" colspan=\"2\"><hr></td></tr>";
      echo "\n<tr><td width=\"700\" colspan=\"2\" class=\"tacmantitle\"><p>FINAL THOUGHTS ON THIS BATTLE</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>rating:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".$items[0]."</td></tr>";
      echo "\n<tr><td width=\"300\" class=\"text\" valign=\"top\"><b>additional overall comments:</b></td><td width=\"400\" class=\"text\" valign=\"top\">".stripslashes($items[1])."</td></tr>";

      /* end form for TAC */

      if ($tac[$_SESSION['position']] == "TAC" && $report[$R_Status] != "2") {
        echo "<tr><td width=\"700\" colspan=\"2\">";
          echo "<table width=\"700\"><tr>";
          echo "<td width=\"100\">&nbsp;</td>";
          echo "<td width=\"200\"><input type=\"submit\" value=\"approve report\" name=\"submit1\"></td>";
          echo "<td width=\"200\"><input type=\"submit\" value=\"deny report\" name=\"submit2\"></td>";
          echo "<td width=\"200\"><textarea name=\"reason\" rows=\"4\" cols=\"35\"></textarea></td>";
          echo "</table>";
        echo "</td></tr>";
        echo "</form>";
      } // end TAC-form

      echo "\n</table>";
    } // end show (1)
    else {
      echo "This report cannot be displayed. "; }
    } // end show (2)
  } // end else


else {
  echo "<p class=\"text\"You have no access to this page.</font>";
}
?>
</body>
</html>