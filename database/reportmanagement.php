<?php
session_start();
?>

<html>
<head>
<?php
include "../tac/dbstuff.tac";
include "../includes/phpself.php";
?>
<link rel="stylesheet" type="text/css" href="../tac.css">
</head>
<body>
<?php
if (isAuthed()) {

  if (isset($_POST['submit'])) {
    switch($_REQUEST['sort']) {
      case 1: $sorter = " ORDER BY R_ID"; break;
      case 2: $sorter = " ORDER BY R_Battle"; break;
      case 3: $sorter = " ORDER BY R_Tester"; break;
    }
    switch ($_REQUEST['filter']) {
      case 1: $filter = ""; break;
      case 2: $filter = " WHERE R_Status = '2'"; break;
      case 3: $filter = " WHERE R_Status = '3'"; break;
      case 4: $filter = " WHERE R_Status = '2' or R_Status = '3'"; break;
      case 5: $filter = " WHERE R_Status = '1'"; break;
      case 6: $filter = " WHERE R_Status = '4'"; break;
    }
    switch($_REQUEST['type']) {
      case 1: $order = " DESC"; break;
      case 2: $order = " ASC"; break;
    }
  }

  else {
    switch($_GET['sort']) {
      case 1: $sorter = " ORDER BY R_ID"; break;
      case 2: $sorter = " ORDER BY R_Battle"; break;
      case 3: $sorter = " ORDER BY R_Tester"; break;
    }
    switch ($_GET['filter']) {
      case 1: $filter = ""; break;
      case 2: $filter = " WHERE R_Status = '2'"; break;
      case 3: $filter = " WHERE R_Status = '3'"; break;
      case 4: $filter = " WHERE R_Status = '2' or R_Status = '3'"; break;
      case 5: $filter = " WHERE R_Status = '1'"; break;
      case 6: $filter = " WHERE R_Status = '4'"; break;
    }
    switch($_GET['type']) {
      case 1: $order = " DESC"; break;
      case 2: $order = " ASC"; break;
    }
  }

  echo "<font class=\"text\">";

  echo "<p>This is a list of submitted beta reports. You can filter on their status if necessary. Click on a report to view it.</p>";

  echo "<form method=\"POST\" action=\"".getPHPSelf()."\">";
  echo "\n<table width=\"620\" border=\"0\"><tr>";
  echo "\n<td width=\"160\" align=\"center\" valign=\"bottom\"><b>Sort By</b><br><select name=\"sort\"><option value=\"1\">Report ID<option value=\"2\">Battle ID<option value=\"3\">Tester's PIN</select></td>";
  echo "\n<td width=\"200\" align=\"center\" valign=\"bottom\"><b>Filter</b><br><select name=\"filter\"><option value=\"1\">Show All<option value=\"2\">Accepted Only<option value=\"3\">Rejected Only<option value=\"4\">Accepted and Rejected<option value=\"5\">Still Open<option value=\"6\">Corrected</select></td>";
  echo "\n<td width=\"160\" align=\"center\" valign=\"bottom\"><b>Sort Order</b><br><select name=\"type\"><option value=\"1\">Ascending<option value=\"2\">Descending</select></td>";
  echo "\n<td width=\"100\" align=\"center\" valign=\"bottom\"><input type=\"submit\" value=\"Refresh List\" name=\"submit\"></td>";
  echo "\n</tr></table>";
  echo "</form>";

  echo "<table width=\"715\" border=\"0\" class=\"alt\"><tr>";
  echo "\n<td width=\"15\" align=\"right\"><b>ID</b></td>";
  echo "\n<td width=\"200\"><b>TCS Assigned</b></td>";
  echo "\n<td width=\"350\"><b>Battle ID</b></td>";
  echo "\n<td width=\"75\"><b>Test Level</b></td>";
  echo "\n<td width=\"75\"><b>Report Status</b></td></tr>";

  ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
  ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
  $query = "SELECT * FROM reports".$filter.$sorter.$order;
  $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

  while ($report = mysqli_fetch_row($result)) {
    $query = "SELECT * FROM roster WHERE N_PIN = '".$report[$R_Tester]."'";
    $output = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $roster = mysqli_fetch_row($output);

    $query = "SELECT * FROM battles WHERE B_ID = '".$report[$R_Battle]."'";
    $output = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $battle = mysqli_fetch_row($output);

    echo "\n<tr><td align=\"right\"><a href=\"viewreport.php?id=".$report[$R_ID]."\">#".$report[$R_ID]." </a></td>";
    echo "\n<td>".$ranks[$roster[$N_Rank]]." ".$roster[$N_Name]."</td>";
    echo "\n<td>".$battle[$B_Name]."</td>";
    echo "\n<td>".$report[$R_Level]."</td>";
    echo "\n<td>".$repstats[$report[$R_Status]]."</td></tr>";
  }
  echo "</table>";

}

else { echo "<p class=\"text\"You have no access to this page.</p>"; }
?>
</body>
</html>
