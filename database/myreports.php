<?php
session_start();
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../tac.css">
</head>
<body>
<?php

include "../tac/dbstuff.tac";
include "../includes/phpself.php";

if (isAuthed()) {
    ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
    $query = "SELECT * FROM reports WHERE R_Tester = '".$_SESSION['pin']."'";
    $reps = mysqli_query($GLOBALS["___mysqli_ston"], $query);

    echo "\n<font class=\"text\"><p><br>You have submitted the following beta reports. Click on a battle's name to view your report.</p>";
    echo "\n<table width=\"630\" class=\"alt\">";
    echo "\n<tr><td width=\"630\" colspan=\"3\"><p><hr></p></td></tr>";
    echo "\n<tr><td width=\"80\"><b>Platform</b></td><td width=\"400\"><b>Battle Name</b></td><td width=\"150\"><b>Level</b></td></tr>";
    while ($report = mysqli_fetch_row($reps)) {
      $query = "SELECT * FROM battles WHERE B_ID = '".$report[$R_Battle]."'";
      $battle = mysqli_fetch_row(mysqli_query($GLOBALS["___mysqli_ston"], $query));

      echo "\n<tr><td>".$battle[$B_Platform]."</td><td><a href=\"viewreport.php?id=".$report[$R_ID]."&me=".md5($report[$R_ID])."\">".$battle[$B_Name]."</a></td><td>".$report[$R_Level]."</td></tr>";
    }
    echo "\n<tr><td width=\"630\" colspan=\"3\"><p><hr></p></td></tr>";
    echo "\n</table></font>";
  echo "<p align=\"right\" class=\"text\"><a href=\"admin.php\">back to menu</a></p>";
}
else {
  echo "<p class=\"text\">You have no access to this page.</p>";
}
?>
