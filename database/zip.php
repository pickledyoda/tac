<?php
$title = "TAC Database - Battles";
include_once('../includes/header.php');
require_once("../includes/bootstrap.php");
require_once("auth.php");

$battle = FALSE;
if (isAuthed() && isset($_GET['id'])) {
  $battle = battle($_GET['id']);
}
if ($battle) {
  $file = $battle['B_File'];
  $path = "../uploads/{$file}";

  if ($battle['D_Corrected']) {
    $file = $battle['D_Corrected'];
    $path = "../corrected/{$file}";
  }

  if (file_exists($path)) {
    header('Content-Type: application/zip');
    header("Content-Disposition: attachment; filename=$file");
    echo file_get_contents($path);
  } else {
    echo "File not found: $path";
  }
} else {
  echo "<p>You have no access to this page.</p>";
}
include_once('../includes/footer.php');
