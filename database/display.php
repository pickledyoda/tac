<?php
session_start();
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../tac.css">
</head>
<body>

<?php

include "../tac/dbstuff.tac";

/* display users */

($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
$query = "SELECT * FROM roster ORDER BY N_Position,N_Rank,N_Name";
$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

?>
<font class="tacmantitle">table roster</font><p>
<table width="1350">
<tr>
  <td width="50" align="center"><b>ID</b></td>
  <td width="75" align="center"><b>Position</b></td>
  <td width="75" align="center"><b>PIN</b></td>
  <td width="75" align="center"><b>Rank</b></td>
  <td width="400" align="center"><b>Name</b></td>
  <td width="75" align="center"><b>Subgroup</b></td>
  <td width="125" align="center"><b>Password</b></td>
  <td width="125" align="center"><b>E-mail</b></td>
  <td width="75" align="center"><b>�-Pts</b></td>
  <td width="275" align="center"><b>Platforms</b></td>
</tr>
<?php

while ($staff = mysqli_fetch_row($result)) {

?>
<tr>
  <td width="50" align="center"><?php echo $staff[$N_ID]; ?></td>
  <td width="75" align="center"><?php echo $tac[$staff[$N_Position]]; ?></td>
  <td width="75" align="center"><?php echo $staff[$N_PIN]; ?></td>
  <td width="75" align="center"><?php echo $ranks[$staff[$N_Rank]]; ?></td>
  <td width="400" align="center"><?php echo $staff[$N_Name]; ?></td>
  <td width="75" align="center"><?php echo $staff[$N_Subgroup]; ?></td>
  <td width="125" align="center"><?php echo $staff[$N_Password]; ?></td>
  <td width="125" align="center"><?php echo $staff[$N_EMail]; ?></td>
  <td width="75" align="center"><?php echo $staff[$N_Points]; ?></td>
  <td width="275" align="center"><?php echo $staff[$N_Platforms]; ?></td>
</tr>
<?php
}
?>
</table>

<?php


/* display battles */

($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
$query = "SELECT * FROM battles ORDER BY B_ID";
$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

?>
<font class="tacmantitle">table battles</font><p>
<table width="4700">
<tr>
  <td width="50" align="center"><b>ID</b></td>
  <td width="400" align="center"><b>Name</b></td>
  <td width="50" align="center"><b>Type</b></td>
  <td width="50" align="center"><b>Platf.</b></td>
  <td width="250" align="center"><b>medal</b></td>
  <td width="25" align="center"><b>#</b></td>
  <td width="125" align="center"><b>filename</b></td>
  <td width="200" align="center"><b>Patches</b></td>
  <td width="200" align="center"><b>Comments</b></td>
  <td width="400" align="center"><b>Author</b></td>
  <td width="75" align="center"><b>PIN</b></td>
  <td width="125" align="center"><b>E-mail</b></td>
  <td width="125" align="center"><b>Status</b></td>
  <td width="125" align="center"><b>TCS-Easy</b></td>
  <td width="125" align="center"><b>TCS-Medium</b></td>
  <td width="125" align="center"><b>TCS-Hard</b></td>
  <td width="125" align="center"><b>TCT</b></td>
  <td width="125" align="center"><b>corrected</b></td>
  <td width="2000" align="leftr"><b>History</b></td>
</tr>
<?php

while ($staff = mysqli_fetch_row($result)) {

?>
<tr>
  <td width="50" align="center"><?php echo $staff[$B_ID]; ?></td>
  <td width="400" align="center"><?php echo $staff[$B_Name]; ?></td>
  <td width="50" align="center"><?php echo $staff[$B_Type]; ?></td>
  <td width="50" align="center"><?php echo $staff[$B_Platform]; ?></td>
  <td width="250" align="center"><?php echo $staff[$B_Medal]; ?></td>
  <td width="25" align="center"><?php echo $staff[$B_Missions]; ?></td>
  <td width="125" align="center"><?php echo $staff[$B_File]; ?></td>
  <td width="200" align="center"><?php echo $staff[$B_Patches]; ?></td>
  <td width="200" align="center"><?php echo $staff[$B_Comments]; ?></td>
  <td width="400" align="center"><?php echo $staff[$B_Author]; ?></td>
  <td width="75" align="center"><?php echo $staff[$B_PIN]; ?></td>
  <td width="125" align="center"><?php echo $staff[$B_EMail]; ?></td>
  <td width="125" align="center"><?php echo $staff[$D_Status]; ?></td>
  <td width="125" align="center"><?php echo $staff[$D_TestEasy]; ?></td>
  <td width="125" align="center"><?php echo $staff[$D_TestMedium]; ?></td>
  <td width="125" align="center"><?php echo $staff[$D_TestHard]; ?></td>
  <td width="125" align="center"><?php echo $staff[$D_Correct]; ?></td>
  <td width="125" align="center"><?php echo $staff[$D_Corrected]; ?></td>
  <td width="2000" align="left"><?php echo $staff[$D_History]; ?></td>
</tr>
<?php
}
?>
</table>

<?php

/* display reports */

($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
$query = "SELECT * FROM reports ORDER BY R_ID";
$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

?>
<font class="tacmantitle">table reports</font><p>
<table width="4575">
<tr>
  <td width="50" align="center"><b>ID</b></td>
  <td width="50" align="center"><b>B_ID</b></td>
  <td width="75" align="center"><b>Level</b></td>
  <td width="75" align="center"><b>Tester</b></td>
  <td width="50" align="center"><b>Status</b></td>
  <td width="5000" align="left"><b>Content</b></td>
  <td width="250" align="center"><b>Reason</b></td>
</tr>
<?php

while ($report = mysqli_fetch_row($result)) {

?>
<tr>
  <td width="50" align="center"><?php echo $report[$R_ID]; ?></td>
  <td width="50" align="center"><?php echo $report[$R_Battle]; ?></td>
  <td width="75" align="center"><?php echo $report[$R_Level]; ?></td>
  <td width="75" align="center"><?php echo $report[$R_Tester]; ?></td>
  <td width="50" align="center"><?php echo $report[$R_Status]; ?></td>
  <td width="5000" align="left"><?php echo $report[$']; ?></td>
  <td width="250" align="center"><?php echo $report[$R_Reason]; ?></td>
</tr>
<?php
}
?>
</table>

<?php

/* display news */

($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
$query = "SELECT * FROM news ORDER BY S_ID";
$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

?>
<font class="tacmantitle">table news</font><p>
<table width="4575">
<tr>
  <td width="50" align="center"><b>ID</b></td>
  <td width="70" align="center"><b>Date</b></td>
  <td width="70" align="center"><b>Time</b></td>
  <td width="50" align="center"><b>Author</b></td>
  <td width="150" align="center"><b>Headline</b></td>
  <td width="5000" align="left"><b>Content</b></td>
  <td width="250" align="center"><b>Shown to</b></td>
</tr>
<?php

while ($news = mysqli_fetch_row($result)) {

?>
<tr>
  <td width="50" align="center"><?php echo $news[$S_ID]; ?></td>
  <td width="70" align="center"><?php echo $news[$S_Date]; ?></td>
  <td width="70" align="center"><?php echo $news[$S_Time]; ?></td>
  <td width="50" align="center"><?php echo $news[$S_Author]; ?></td>
  <td width="150" align="center"><?php echo $news[$S_Headline]; ?></td>
  <td width="5000" align="left"><?php echo $news[$S_Content]; ?></td>
  <td width="250" align="center"><?php echo $news[$S_Shown]; ?></td>
</tr>
<?php
}
?>
</table>