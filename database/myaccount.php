<?php
session_start();
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../tac.css">
</head>
<body>
<?php

include "../tac/dbstuff.tac";

if (isAuthed()) {

  ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
  ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
  $query = "SELECT * FROM roster WHERE N_PIN = '".$_SESSION['pin']."'";
  $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
  $data = mysqli_fetch_row($result);
  echo "";

  echo "<p class=\"text\">These are your account settings at this time.</p>";

  echo "<table width=\"300\" class=\"alt\">";
  echo "<tr><td width=\"100\" height=\"30\" valign=\"center\" class=\"text\"><b>PIN:</b></td><td width=\"200\" valign=\"center\" class=\"text\">".$data[$N_PIN]."</td></tr>";
  echo "<tr><td height=\"30\" valign=\"center\" class=\"text\"><b>Name:</b></td><td valign=\"center\" class=\"text\">".$ranks[$data[$N_Rank]]." ".$data[$N_Name]."</td></tr>";
  echo "<tr><td height=\"30\" valign=\"center\" class=\"text\"><b>Position:</b></td><td valign=\"center\" class=\"text\">".$tac[$data[$N_Position]]."</td></tr>";
  echo "<tr><td height=\"30\" valign=\"center\" class=\"text\"><b>e-Mail:</b></td><td valign=\"center\" class=\"text\">".$data[$N_EMail]."</td></tr>";
  echo "<tr><td height=\"30\" valign=\"center\" class=\"text\"><b>Beta Points:</b></td><td valign=\"center\" class=\"text\">".$data[$N_Points]."</td></tr>";
  echo "<tr><td height=\"30\" valign=\"center\" class=\"text\"><b>Platforms:</b></td><td valign=\"center\" class=\"text\">".$data[$N_Platforms]."</td></tr>";
  echo "</table>";

  echo "<p align=\"right\" class=\"text\"><a href=\"admin.php\">back to menu</a></p>";
}
else {
  echo "<p class=\"text\">You have no access to this page.</p>";
}
?>
