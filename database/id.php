<?php
$title = "TAC Database - Battles";
include_once('../includes/header.php');
require_once("../includes/bootstrap.php");
require_once("auth.php");

$battle = FALSE;
if (isAuthed() && isset($_GET['id'])) {
  $battle = battle($_GET['id']);
}
if ($battle) {
  pre($battle);
} else {
  echo "<p>You have no access to this page.</p>";
}
include_once('../includes/footer.php');
