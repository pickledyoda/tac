<?php
session_start();
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../tac.css">
<?php
include "../tac/dbstuff.tac";
include "../includes/phpself.php";
?>
</head>
<body>
<font class="text">
<?php

if (!isset($_SESSION['name'])) {
  echo "You do not have access to this page.";
}
else {
  if (isset($_POST['submit'])) {

    /* generate a random password */

	$lpnewpass = "";
	$length = rand(6,12);

	$chars = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
	while (strlen($lpnewpass) <= $length) { $lpnewpass .= $chars[mt_rand(1,strlen($chars))]; }
    $lpnewpassenc = md5($lpnewpass);

    $plf = setPlatforms($_POST[platform]);

    ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
    $query = "INSERT INTO roster (N_Position, N_Name, N_PIN, N_Subgroup, N_Email, N_Pts, N_Rank, N_Password, N_Platform) VALUES ('".$_POST['position']."', '".addslashes($_POST['name'])."', '".$_POST['pin']."', '$NULL', '".addslashes($_POST['email'])."', '0', '".$_POST['rank']."', '$lpnewpassenc', '$plf')";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

    echo "A new user account has been created:";
    echo "<p>";
    echo "<b>Name</b>: ".$ranks[$_POST['rank']]." ".$_POST['name']." (".$_POST['pin'].")<br>";
    echo "<b>E-mail address</b>: ".$_POST['email']."<br>";
    echo "<b>Position</b>: ".$tacfull[$_POST['position']]."<br>";
    echo "<b>Platforms</b>: ".$plf."<br>";
    echo "<p>";
    echo "<a href=\"admin.php\">Return to menu</a>";

    $mailrsn = "newuser";
    include "mailform.php";
  }
  else {
    echo "Please make sure all fields are filled in properly before submitting. Error checking is far from optimal !";
    echo "<p>";

    echo "<form method=\"POST\" action=\"".getPHPSelf()."\">";
    echo "<table width=\"650\" border=\"0\">";
    echo "<tr><td width=\"150\" class=\"text\">name:</td></td><td width=\"500\"><input type=\"text\" name=\"name\" size=\"50\"></td></tr>";
    echo "<tr><td width=\"150\" class=\"text\">rank:</td><td width=\"500\"><select name=\"rank\">";
	         $j = 1;
	         while ($ranks[$j]) {
	           echo "<option value=\"".$j."\"";
	           if ($j == $profile[$N_Rank]) { echo " selected=\"selected\""; }
	           echo ">".str_replace($ranks,$ranksfull,$ranks[$j]);
	           $j++;
	         }
         echo "</select></td>";
    echo "<tr><td width=\"150\" class=\"text\">pin:</td><td width=\"500\"><input type=\"text\" name=\"pin\" size=\"50\"></td></tr>";
    echo "<tr><td width=\"150\" class=\"text\">position:</td><td width=\"500\"><select name =\"position\">";
	         $j = 1;
	         while ($tac[$j]) {
	           echo "<option value=\"".$j."\"";
	           if ($j == $profile[$N_Position]) { echo " selected=\"selected\""; }
	           echo ">".str_replace($tac,$tacfull,$tac[$j]);
	           $j++;
	         }
	         echo "</select></td></tr>";
    echo "<tr><td width=\"150\" class=\"text\">e-mail address:</td><td width=\"500\"><input type=\"text\" name=\"email\" size=\"50\"></td></tr>";
    echo "<tr><td width=\"150\" class=\"text\" valign=\"top\">platforms:</td><td width=\"500\">";
      echo "<table width=\"500\" border=\"0\"><tr>";
      echo "<td width=\"250\" valign=\"top\" class=\"text\">";
      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"XW\"> X-wing<br>";
      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"TIE\"> TIE Fighter<br>";
      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"XvT\"> X-wing vs. TIE Fighter<br>";
      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"BoP\"> Balance of Power<br>";
      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"XWA\"> X-wing Alliance<br>";
      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"JA\"> Jedi Academy";
      echo "</td>";
      echo "<td width=\"250\" valign=\"top\" class=\"text\">";
      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"SWGB\"> Galactic Battlegrounds<br>";
      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"IAII\"> Imperial Alliance<br>";
      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"IAS\"> Imperial Assault<br>";
      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"EaW\"> Empire at War<br>";
      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"BF\"> Battlefront<br>";
      echo "<input type=\"checkbox\" name=\"platform[]\" value=\"BFII\"> Battlefront II";
      echo "</td></tr></table>";
    echo "</td></tr>";
    echo "<tr><td width=\"150\"><input type=\"submit\" value=\"add user\" name=\"submit\"></td><td width=\"500\">&nbsp;</td></tr>";
    echo "</table>";
    echo "</form>";
  }
}
?>