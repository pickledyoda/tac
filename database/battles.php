<?php
$title = "TAC Database - Battles";
include_once('../includes/header.php');
require_once("../includes/bootstrap.php");
require_once("auth.php");

if (isAuthed()) {
  $showAll = isset($_POST['showall']) ? $_POST['showall'] : '';
  ?>
  <table class="table table-striped mx-auto">
    <thead class="thead-dark">
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Type</th>
        <th>Creator</th>
        <th>Status</th>
        <th>Submitted</th>
        <th style="min-width: 150px;">Download</th>
      </tr>
    </thead>
    <tbody>
      <?php
        $filter = $showAll ? '1=1' : 'D_STATUS <> 7 AND D_STATUS <> 9';
        $sql = "SELECT * FROM battles WHERE {$filter} ORDER BY B_ID ASC";
        $battles = tacQueryAll($sql);
        foreach ($battles as $battle) {
          $id = $battle['B_ID'];
          $dte = explode($DELIM, $battle['D_History']);
          $dte = date("d M Y", $dte[0]);

          echo "<tr>
          <td><a href='id.php?id={$id}'>{$id}</a></td>
          <td><a href='showbattle.php?id=$id' class='text-light'>{$battle['B_Name']}</a></td>
          <td>{$battle['B_Platform']}-{$battle['B_Type']}</td>
          <td>{$battle['B_Author']}</td>
          <td>{$batstat[$battle['D_Status']]}</td>
          <td>{$dte}</td>
          <td><a href='zip.php?id=$id' class='btn btn-success btn-sm'>Zip</a> 
              <a href='ehm.php?id=$id' class='btn btn-warning btn-sm'>EHM</a></td>
        </tr>";
        }
        ?>
    </tbody>
  </table>
<?php
} else {
  echo "<p>You have no access to this page.</p>";
}
include_once('../includes/footer.php');
