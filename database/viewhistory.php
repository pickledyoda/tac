<?php
session_start();
?>

<html>
<head>
	<link rel="stylesheet" type="text/css" href="../tac.css">
</head>
<body>

<?php

/* no use logging in twice is there... */
if (isAuthed()) {

	include "../tac/dbstuff.tac";

	$ID = $_GET['id'];

	($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost, $dbusername, $dbpassword)) or die("Unable to connect to database");
	((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
	$query  = "SELECT * FROM battles WHERE B_ID='" . $ID . "'";
	$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
	$info   = mysqli_fetch_row($result);

	echo "<p><span class='tacmantitle'>" . $info[$B_Name] . "</span><br> by " . $info[$B_Author] . " (<a href='mailto:" . $info[$B_EMail] . "'>" . $info[$B_EMail] . "</a>)</p>";
	echo "<hr>\n";

	$data[0] = strtok($info[$D_History], $DELIM);
	$i       = 1;
	while ($data[$i - 1]) {
		$data[$i] = strtok($DELIM);
		$i++;
	}
	$i = 0;
	while ($data[$i]) {
		$disp = explode($DELIM, $data[$i]);
		echo "<p><b><span color='#DD2200'>" . date("d F Y, H:i:s", $disp[0]) . "</span></b>";
		$t = 1;
		while ($disp[$t]) {
			echo "<br> &nbsp; &nbsp; " . $disp[$t];
			$t++;
		}
		echo "</p>\n";
		$i++;
	}

	echo "<hr>";
} // end if session

else {
	echo "<p>You have no access to this page.</p>";
}
?>
