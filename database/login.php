<?php
include "../tac/dbstuff.tac";
include "../includes/phpself.php";

($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost, $dbusername, $dbpassword, $dbname)) or die("Unable to connect to database");

if (isset($_POST['submit']) && isset($_POST['pin'])) {
    $query = "SELECT * FROM roster WHERE N_PIN = '" . $_POST['pin'] . "'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $data = mysqli_fetch_row($result);
    if (isset($_POST['submit']) && (md5($_POST['pass']) == $data[$N_Password])) {
        echo "<META http-equiv='refresh' content='1;URL=admin.php'>";
    } else if (isset($_POST['lostpasswd'])) {
        echo "<META http-equiv='refresh' content='10;URL=login.php'>";
    }
}

if (isAuthed()) {
    echo "There was an error to this pagecall - you are already logged in.";
} else {
    if (isset($_POST['submit']) && isset($_POST['pin'])) {
        $query = "SELECT * FROM roster WHERE N_Pin = '" . $_POST['pin'] . "'";
        $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
        $data = mysqli_fetch_array($result);

        /* password accepted - logging in and setting session variables */

        if (md5($_POST['pass']) == $data[$N_Password]) {
            echo "\n<br>Password accepted, logging in...";
            $_SESSION['ID'] = $data[$N_ID];
            $_SESSION['name'] = $data[$N_Name];
            $_SESSION['pin'] = $data[$N_PIN];
            $_SESSION['subgroup'] = $data[$N_Subgroup];
            $_SESSION['e-mail'] = $data[$N_EMail];
            $_SESSION['rank'] = $data[$N_Rank];
            $_SESSION['points'] = $data[$N_Pts];
            $_SESSION['platform'] = $data[$N_Platform];
            $_SESSION['position'] = $data[$N_Position];

            /* put here a redirect to the base page, that displays only the links for your access level */
        }

        /* password rejected - displaying login form again, and a password-retrieval */

        else {
            echo "<p>Authentication failed - please try again and remember that passwords are case-sensitive.</p>";
            echo "<p><br>If you have forgotten your password, enter your PIN and a new one will be e-mailed to your address.</p>";
            echo "<form method='POST' action='" . getPHPSelf() . "'>";
            echo "<table width='500'>";
            echo "<tr><td width='75' height='40' valign='center' class='text'><b>pin:</b></td><td width='425' height='40' valign='center' class='text'><input type='text' name='pin'></td></tr>";
            echo "<tr><td width='75' height='40' valign='center' class='text'><b>password:</b></td><td width='425' height='40' valign='center' class='text'><input type='password' name='pass'></td></tr>";
            echo "<tr><td width='75' height='50' valign='center'><input type='submit' value='log in' name='submit'></td><td width='425' height='50' valign='center'><input type='submit' value='I forgot my password' name='lostpasswd'></td></tr>";
            echo "</table>";
            echo "</form>";
        }
    } elseif (isset($_POST['lostpasswd']) && $_POST['pin']) {
        $query = "SELECT * FROM roster WHERE N_Pin = '" . $_POST['pin'] . "'";
        $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
        $data = mysqli_fetch_array($result);
        $lpname = $data[$N_Name];
        $lppin = $data[$N_PIN];
        $lpemail = $data[$N_EMail];
        $lprank = $data[$N_Rank];
        $lpip = $_SERVER['REMOTE_ADDR'];

        /* generate a random password */

        $lpnewpass = "";
        $length = rand(6, 12);

        $chars = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
        while (strlen($lpnewpass) <= $length) {$lpnewpass .= $chars[mt_rand(1, strlen($chars))];}
        $lpnewpassenc = md5($lpnewpass);

        /* write it to the TAC database */

        $query = "UPDATE roster SET N_Password = '" . $lpnewpassenc . "' WHERE N_PIN = '" . $data[$N_PIN] . "'";
        $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

        /* send it via e-mail to the user`s address */

        $mailrsn = "newpass";
        include "mailform.php";

        echo "<p>Your new password has been sent to your e-mail address. please wait for that and login with it.<br>After logging in, you are advised to change it.<p>";
        echo "You will automatically be redirected to the login screen in 10 seconds.</p>";

    } else {
        echo "<p>Welcome to the TAC Office database. please login using your TIE Corps PIN and TAC password.</p>";
        echo "<form method='POST' action='" . getPHPSelf() . "'>";
        echo "<table width='500'>";
        echo "<tr><td width='75' height='40' valign='center' class='text'><b>pin:</b></td><td width='425' height='40' valign='center' class='text'><input type='text' name='pin'></td></tr>";
        echo "<tr><td width='75' height='40' valign='center' class='text'><b>password:</b></td><td width='425' height='40' valign='center' class='text'><input type='password' name='pass'></td></tr>";
        echo "<tr><td width='500' colspan='2' height='50' valign='center'><input type='submit' value='log in' name='submit'></td></tr>";
        echo "</table>";
        echo "</form>";
    }
}
