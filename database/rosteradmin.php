<?php
session_start();
?>

<html>
<head>
<?php
include "../tac/dbstuff.tac";
include "../includes/phpself.php";
?>
<link rel="stylesheet" type="text/css" href="../tac.css">
</head>
<body>
<?php
if (isAuthed()) {

  echo "<font class=\"text\">";
  ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
  ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
  $query = "SELECT * FROM roster ORDER BY N_Position,N_Rank,N_Name";
  $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

  echo "<p>This is a list of all TAC Office personnel. Two tables are shown: Active and Inactive. Click on a name to edit the user's profile.</p>";

  echo "<p class=\"tacmantitle\">Active Personnel</p><br>";

  echo "<table width=\"730\" border=\"0\" class=\"alt\">";
  echo "<tr>";
  echo "<td width=\"200\"><b>Name</b></td>";
  echo "<td width=\"50\"><b>Position</b></td>";
  echo "<td width=\"50\"><b>PIN</b></td>";
  echo "<td width=\"180\"><b>e-Mail</b></td>";
  echo "<td width=\"50\"><b>�-Pts</b></td>";
  echo "<td width=\"200\"><b>Platforms</b></td>";
  echo "</tr>";

  while ($staff = mysqli_fetch_row($result)) {
    if ($tac[$staff[$N_Position]] != "RSV") {
      echo "<tr>";
      echo "<td width=\"200\"><a href=\"editprofile.php?id=".$staff[$N_ID]."\">".$ranks[$staff[$N_Rank]]." ".$staff[$N_Name]."</a></td>";
      echo "<td width=\"50\">".$tac[$staff[$N_Position]]."</td>";
      echo "<td width=\"50\">".$staff[$N_PIN]."</td>";
      echo "<td width=\"180\"><a href=\"mailto:".$staff[$N_EMail]."\">".$staff[$N_EMail]."</a></td>";
      echo "<td width=\"50\">".$staff[$N_Points]."</td>";
      echo "<td width=\"200\">".$staff[$N_Platforms]."</td>";
      echo "</tr>";
    }
  }
  echo "</table>";

  echo "<br>&nbsp;<br>";

  echo "<font class=\"tacmantitle\">Inactive Personnel</font><br>";
  echo "<table width=\"480\" class=\"alt\">";
  echo "<tr>";
  echo "<td width=\"200\"><b>Name</b></td>";
  echo "<td width=\"50\"><b>PIN</b></td>";
  echo "<td width=\"180\"><b>e-Mail</b></td>";
  echo "<td width=\"50\"><b>�-Pts</b></td>";
  echo "</tr>";

  $query = "SELECT * FROM roster ORDER BY N_Name";
  $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

  while ($staff = mysqli_fetch_row($result)) {
    if ($tac[$staff[$N_Position]] == "RSV") {
      echo "<tr>";
      echo "<td width=\"200\"><a href=\"editprofile.php?id=".$staff[$N_ID]."\">".$ranks[$staff[$N_Rank]]." ".$staff[$N_Name]."</a></td>";
      echo "<td width=\"50\">".$staff[$N_PIN]."</td>";
      echo "<td width=\"180\"><a href=\"mailto:".$staff[$N_EMail]."\">".$staff[$N_EMail]."</a></td>";
      echo "<td width=\"50\">".$staff[$N_Points]."</td>";
      echo "</tr>";
    }
  }
  echo "</table>";

}

else { echo "<p class=\"text\">You have no access to this page.</p>"; }
?>
</body>
</html>
