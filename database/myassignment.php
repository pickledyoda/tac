<?php
session_start();
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../tac.css">
</head>
<body>
<?php

include "../tac/dbstuff.tac";
include "../includes/phpself.php";

if (isAuthed()) {
  if (isset($_POST['submit'])) {
    $ID = $_GET['id'];
    $target_path = "../corrected/";
    $uploadfile = $target_path.basename($_FILES['cbfile']['name']);

    if (strtoupper(substr($uploadfile, -4)) != ".ZIP" && strtoupper(substr($uploadfile, -4)) != ".RAR") { echo "Sorry, your battle can not be uploaded - the file must be a .ZIP or .RAR archive.<br>Use the [BACK] button on your browser to return to the form."; }

    /*  check 3:  duplicate filename  */

    elseif (file_exists($uploadfile)) { echo "Sorry, your battle can not be uploaded - the target file already exists on the server. Please use another filename.<br>Use the [BACK] button on your browser to return to the form."; }

    else {
      echo "<font class=\"text\">";
      if (move_uploaded_file($_FILES['cbfile']['tmp_name'], $uploadfile)) {
        ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
        ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
        $query = "SELECT * FROM battles WHERE B_ID='".$ID."'";
        $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
        $info = mysqli_fetch_row($result);

        $cbfile = str_replace("../corrected/","",str_replace($targetpath,"",$uploadfile));
        $mailrsn = "corrected";
        $corrfile = $uploadfile;
        include "mailform.php";

        echo "<p class=\"text\">Your file was uploaded. Thank you.</p>";
        echo "<p><a href=\"myassignment.php\">back to assignments</a></p>";

        $query = "UPDATE battles SET ".
                 "D_Correct ='0', ".
                 "D_History = '".addslashes($info[$D_History])."�".date('U')."�corrected file uploaded by ".$_SESSION['name']."', ".
                 "D_Corrected = '".$cbfile."' where B_ID = '".$ID."'";
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

	    $query = "SELECT * FROM roster WHERE N_PIN='".$info[$D_Correct]."'";
        $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
        $tct = mysqli_fetch_row($result);
        $newpts = $tct[$N_Points] + $info[$B_Missions];

        $query = "UPDATE roster SET N_Pts='".$newpts."' WHERE N_PIN='".$info[$D_Correct]."'";
        $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
	  }
      else {
        echo "<font class=\"text\">Your upload has failed. Please try again, or submit your custom battle via e-mail.</font>";
      }
    }
  }

  else {
    echo "<font class=\"text\">";
    ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));

	// Prevent Final Checks from showing up in non-TAC Assignments (if there were errors in assignment processing?)
    if ($tac[$_SESSION['position']] == "TAC") {
        $query = "SELECT * FROM battles WHERE D_TestEasy = '".$_SESSION['pin']."' or D_TestMedium = '".$_SESSION['pin']."' or D_TestHard = '".$_SESSION['pin']."' or D_Correct = '".$_SESSION['pin']."' or D_Status = '6'";
    }
     else
    {
        $query = "SELECT * FROM battles WHERE D_TestEasy = '".$_SESSION['pin']."' or D_TestMedium = '".$_SESSION['pin']."' or D_TestHard = '".$_SESSION['pin']."' or D_Correct = '".$_SESSION['pin']."'";
    }
    $resbat = mysqli_query($GLOBALS["___mysqli_ston"], $query);

    $i = 1;
    while ($battle = mysqli_fetch_row($resbat)) {
      $beta = 0;
      $test = 0;
      if ($i == 1) { echo "<br><p>You currently have the following outstanding assignments:</p>"; }
      if ($i > 1) { echo "<tr><td width=\"700\" colspan=\"3\"><hr><p></p></td></tr>"; }
      $i++;
      echo "<table width=\"700\" border=\"0\">";
      echo "<tr><td width=\"200\" class=\"text\" valign=\"top\"><b>Battle Name</b></td><td width=\"300\" class=\"text\" valign=\"top\">".$battle[$B_Name]."</td><td width=\"200\">&nbsp;</td></tr>";
      echo "<tr><td width=\"200\" class=\"text\" valign=\"top\"><b>Battle Type</b></td><td width=\"300\" class=\"text\" valign=\"top\">".$battle[$B_Platform]." ".$battle[$B_Type]."</td><td width=\"100\">&nbsp;</td></tr>";
      echo "<tr><td width=\"200\" class=\"text\" valign=\"top\"><b>Number of Missions</b></td><td width=\"300\" class=\"text\" valign=\"top\">".$battle[$B_Missions]."</td><td width=\"200\">&nbsp;</td></tr>";
      echo "<tr><td width=\"200\" class=\"text\" valign=\"top\"><b>Required Patches</b></td><td width=\"300\" class=\"text\" valign=\"top\">".$battle[$B_Patches]."</td><td width=\"200\">&nbsp;</td></tr>";
      echo "<tr><td width=\"200\" class=\"text\" valign=\"top\"><b>Your Assignment</b></td><td width=\"300\" class=\"text\" valign=\"top\">";
        $assign = 1;
        if ($battle[$D_TestEasy] == $_SESSION['pin']) { $assignment[0] = "easy"; if ($assign > 1) { echo "<br>"; } echo "Test this battle on level <b>EASY</b>"; $test = 2; $assign++; }
        if ($battle[$D_TestMedium] == $_SESSION['pin']) { $assignment[1] = "medium"; if ($assign > 1) { echo "<br>"; } echo "Test this battle on level <b>MEDIUM</b>"; $test = 3; $assign++; }
        if ($battle[$D_TestHard] == $_SESSION['pin']) { $assignment[2] = "hard"; if ($assign > 1) { echo "<br>"; } echo "Test this battle on level <b>HARD</b>"; $test = 4; $assign++; }
        if ($battle[$D_Correct] == $_SESSION['pin']) { $assignment[3] = "correct"; if ($assign > 1) { echo "<br>"; } echo "Correct this battle"; $beta = 1; $assign++; }
        if ($battle[$D_Status] == 6 && $tac[$_SESSION['position']] == "TAC") { $assignment[4] = "final check"; if ($assign > 1) { echo "<br>"; } echo "Perform Final Check"; $fc = 1; $assign++; }
      echo "</td><td width=\"200\" class=\"text\" valign=\"top\"><a href=\"../uploads/".$battle[$B_File]."\" onMouseOver=\"window.status=''; return true;\">Download the battle</a>";
        if (file_exists("../corrected/".$battle[$D_Corrected])) { echo "<br><a href=\"../corrected/".$battle[$D_Corrected]."\" onMouseOver=\"window.status=''; return true;\">Download the corrected battle</a>"; }
        echo "</td></tr>";
      if ($fc == 1) { echo "<tr><td width=\"200\">&nbsp;</td><td width=\"300\" class=\"text\" valign=\"top\">&nbsp;</td><td width=\"200\" class=\"text\" valign=\"top\">";
if (file_exists("../corrected/recorrect/".$battle[$D_Corrected])) { echo "<a href=\"../corrected/recorrect/".$battle[$D_Corrected]."\" onMouseOver=\"window.status=''; return true;\">Download the re-corrected battle</a>"; }
 echo "</td></tr>"; }
      if ($fc != 1) {
        echo "<tr><td width=\"200\" class=\"text\" valign=\"top\"><b>Assignment Due on</b></td><td width=\"300\" class=\"text\" valign=\"top\">";

          /* hardest of all : find out when the assignment was set  */

          $temp = split("�",$battle[$D_History]);

          /*  start searching at the end.....  */

          $k = count($temp);
          while ($k > 0) {
            /*  new let`s get the name and the assignment and see if both appear in $item  */
            /*  when found, set $k to -1 so the loop will stop!  */

            $name = $_SESSION['name'];
            if (strpos($temp[$k - 1],$name)) {
              $items = explode($DELIM,$temp[$k - 1]);
              $datum = date("M d Y",$items[0] + 1209600);
              $k = 0;
            }
            $k--;
          }
        echo $datum;
        echo "</td><td width=\"200\" class=\"text\" valign=\"top\">&nbsp;</td></tr>";
      }
      if ($test > 1) {
        echo "<tr><td width=\"700\" class=\"text\" valign=\"top\" colspan=\"3\"><p><a href=\"betareport.php?id=".$battle[$B_ID]."\">Submit your Beta Report</a></p></td></tr>";

	    $query = "SELECT * FROM reports WHERE R_Tester='".$_SESSION['pin']."' and (R_Status='1' or R_Status='3') and R_Battle='".$battle[$B_ID]."'";
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

	    while ($info = mysqli_fetch_row($result)) { echo "<tr><td width=\"700\" class=\"text\" valign=\"top\" colspan=\"3\"><a href=\"viewreport.php?id=".$info[$R_ID]."\">View submitted Beta Report for level <b>".$info[$R_Level]."</b></a></td></tr>"; }
      }

      if ($beta == 1) {
        ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
        ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
        $query = "SELECT * FROM reports WHERE R_Battle='".$battle[$B_ID]."' and R_Status='2' or R_Status='4'";
        $reps = mysqli_query($GLOBALS["___mysqli_ston"], $query);

	    while ($info = mysqli_fetch_row($reps)) {
	      $query = "SELECT * FROM battles WHERE D_Correct = '".$_SESSION['pin']."'";
 	      $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
          $battle = mysqli_fetch_row($result);

	      $query = "SELECT * FROM reports WHERE R_Battle='".$battle[$B_ID]."'";
 	      $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

	      echo "<tr><td width=\"700\" class=\"text\" valign=\"top\" colspan=\"3\">";
  	      echo "<a href=\"viewreport.php?id=".$info[$R_ID]."\">View submitted Beta Report for level <b>".$info[$R_Level]."</b></a>";
  	      echo "</td></tr>";
	    }
        echo "<tr><td width=\"500\" colspan=\"2\" valign=\"top\" class=\"text\">";
        echo "<form enctype=\"multipart/form-data\" action=\"".getPHPSelf()."?id=".$battle[$B_ID]."\" method=\"POST\">";
        echo "<input name=\"cbfile\" size=\"50\" type=\"file\"></td>";
        echo "<td width=\"200\" valign=\"top\"><input type=\"submit\" value=\"upload file\" name=\"submit\"></td>";
        echo "</form>";
        echo "</tr>";
      }
      echo "</table>";
    }
    if ($i == 1) { echo "<p class=\"text\">You have no assignments.</p>";
  }
  echo "<p align=\"right\" class=\"text\"><a href=\"admin.php\">back to menu</a></p>";
  }
}
else {
  echo "<p class=\"text\">You have no access to this page.</p>";
}
?>
