<?php

function select($options, $current = NULL)
{
  $html = "<select class='custom-select'>\n";
  foreach ($options as $value => $label) {
    $selected = $value === $current ? 'selected' : '';
    $html .= "<option value='{$value}' $selected>{$label}</option>";
  }
  $html .= "</select>";
  return $html;
}
