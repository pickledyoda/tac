<?php
session_start();
?>

<html>
<head>
<?php
include "../tac/dbstuff.tac";
include "../includes/phpself.php";

($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
$pwquery = "SELECT * FROM roster WHERE N_PIN = '".$_SESSION['pin']."'";
$pwresult = mysqli_query($GLOBALS["___mysqli_ston"], $pwquery);
$data = mysqli_fetch_row($pwresult);
if (isset($_POST['ranksubmit']) || isset($_POST['mailsubmit']) || (isset($_POST['pwsubmit']) && (md5($_POST['oldpass']) == $data[$N_Password]) && ($_POST['newpass1'] == $_POST['newpass2']))) {
echo "<META http-equiv=\"refresh\" content=\"1;URL=admin.php\">";
}
?>
<link rel="stylesheet" type="text/css" href="../tac.css">
</head>
<body>
<?php
function cpform() {
  echo "<form method=\"POST\" action=\"".getPHPSelf()."\">";
  echo "<table width=\"400\">";
  echo "<tr><td width=\"200\" height=\"30\" valign=\"center\" class=\"text\"><b>Current Password:</b></td><td width=\"200\" height=\"30\" valign=\"center\" class=\"text\"><input type=\"password\" name=\"oldpass\"></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" valign=\"center\" class=\"text\"><b>New Password:</b></td><td width=\"200\" height=\"30\" valign=\"center\" class=\"text\"><input type=\"password\" name=\"newpass1\"></td></tr>";
  echo "<tr><td width=\"200\" height=\"30\" valign=\"center\" class=\"text\"><b>Confirm New Password:</b></td><td width=\"200\" height=\"30\" valign=\"center\" class=\"text\"><input type=\"password\" name=\"newpass2\"></td></tr>";
  echo "<tr><td width=\"200\" height=\"50\" valign=\"center\">&nbsp;</td><td width=\"400\" height=\"50\" valign=\"center\" colspan=\"2\"><input type=\"submit\" value=\"change password\" name=\"pwsubmit\"></td></tr>";
  echo "</table>";
  echo "</form>";
}

if (isAuthed()) {
  ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
  ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));

// Change Password
  $pwquery = "SELECT * FROM roster WHERE N_PIN = '".$_SESSION['pin']."'";
  $pwresult = mysqli_query($GLOBALS["___mysqli_ston"], $pwquery);
  $data = mysqli_fetch_row($pwresult);

   if (isset($_POST['pwsubmit'])) {
    echo "<p class=\"text\">";
    if ((md5($_POST['oldpass']) == $data[$N_Password]) && ($_POST['newpass1'] == $_POST['newpass2'])) {
      $lpnewpassenc = md5($_POST['newpass1']);
      ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
	  ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
	  $pwquery = "UPDATE roster SET N_Password = '".$lpnewpassenc."' WHERE N_PIN = '".$data[$N_PIN]."'";
	  $pwresult = mysqli_query($GLOBALS["___mysqli_ston"], $pwquery);

	  echo "Your password has been changed. You will be logged out and redirected back to the login page in 5 seconds.";
      session_destroy();
    }
    elseif (md5($_POST['oldpass']) != $data[$N_Password]) {
      echo "The current password you entered is incorrect - please try again.";
      echo "<br>".$_POST['oldpass']." - ".md5($_POST['oldpass'])." -- ".$data[$N_Password];
      cpform();
    }
    elseif ($_POST['newpass1'] != $_POST['newpass2']) {
      echo "The new password does not match your confirmation - please try again.";
      cpform();
    }
    elseif (!isset($_POST['oldpass'])) {
      echo "You did not enter your current password - please try again.";
      cpform();
    }
    elseif (!isset($_POST['newpass1']) || !isset($_POST['newpass2'])) {
      echo "You did not enter your new password or confirm it - please try again.";
      cpform();
    }

    echo "</p>";
  }
  else {
      echo "<font class=\"text\">";
      echo "<b>Change TAC Database Password</b>";
      echo "<p>Note that a password change can NOT be undone!</p>";
      cpform();
  }
// End Password Form
// Change Rank
  if (isset($_POST['ranksubmit'])) {
    ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
    $rankquery = "UPDATE roster SET N_Rank = '".$_POST['newrank']."' WHERE N_ID = '".$_SESSION['ID']."'";
    $rankresult = mysqli_query($GLOBALS["___mysqli_ston"], $rankquery);

    $rankquery = "SELECT * FROM roster WHERE N_ID = '".$_SESSION['ID']."'";
    $rankresult = mysqli_query($GLOBALS["___mysqli_ston"], $rankquery);
    $info = mysqli_fetch_row($rankresult);
	$rnknick = $info[$N_Name]." (".$info[$N_PIN].")";

    $_SESSION['rank'] = $_POST['newrank'];

    $mailrsn = "newrank";
    include "mailform.php";

    echo "<font class=\"text\">Your rank has been changed. You will be redirected momentarily.</font>";
  }
  else {
    $prfquery = "SELECT * FROM roster WHERE N_ID = '".$_SESSION['ID']."'";
    $prfresult = mysqli_query($GLOBALS["___mysqli_ston"], $prfquery);
    $profile = mysqli_fetch_row($prfresult);

    echo "<p class=\"text\"><b>Update Rank</b></p>";
    echo "<form method=\"POST\" action=\"".getPHPSelf()."\">";
    echo "<table width=\"400\">";
    echo "<tr><td width=\"200\" height=\"30\" valign=\"center\" class=\"text\">Select new rank:</td><td width=\"200\" height=\"30\" valign=\"center\" class=\"text\"><select name=\"newrank\">";
         $j = 1;
         while ($ranks[$j]) {
           echo "<option value=\"".$j."\"";
           if ($j == $profile[$N_Rank]) { echo " selected=\"selected\""; }
           echo ">".$ranksfull[$j]."</option>";
           $j++;
         }
    echo "</select></td></tr>";
    echo "<tr><td width=\"75\" height=\"50\" valign=\"center\"><p><input type=\"submit\" value=\"change rank\" name=\"ranksubmit\"></td><td width=\"425\" height=\"50\" valign=\"center\">&nbsp;</td></tr>";
    echo "</table>";
    echo "</form>";
  }
// End Rank Form
// Change E-mail
  if (isset($_POST['mailsubmit'])) {
    ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
    ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
    $mailquery = "UPDATE roster SET N_EMail = '".addslashes($_POST['newmail'])."' WHERE N_ID = '".$_SESSION['ID']."'";
    $mailresult = mysqli_query($GLOBALS["___mysqli_ston"], $mailquery);

    $_SESSION['e-mail'] = $_POST['newmail'];
    echo "<font class=\"text\">Your e-mail address has been changed. You will be redirected momentarily.</font>";
  }
  else {
    echo "<font class=\"text\"><b>Change e-Mail Address</b></p>";
    echo "<p><form method=\"POST\" action=\"".getPHPSelf()."\">";
    echo "<table width=\"500\">";
    echo "<tr><td width=\"200\" height=\"30\" valign=\"center\" class=\"text\">Enter new address:</td><td width=\"300\" height=\"30\" valign=\"center\" class=\"text\"><input type=\"text\" name=\"newmail\" size=\"50\" value=\"".$_SESSION['e-mail']."\"></td></tr>";
    echo "<tr><td width=\"75\" height=\"50\" valign=\"center\"><p><input type=\"submit\" value=\"change address\" name=\"mailsubmit\"></p></td><td width=\"425\" height=\"50\" valign=\"center\">&nbsp;</td></tr>";
    echo "</table>";
    echo "</form></p>";
  }
// End E-mail Form
}
else {
  echo "<p class=\"text\">You have no access to this page.</p>";
}
?>
