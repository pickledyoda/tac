<html>
<head>
<link rel="stylesheet" type="text/css" href="tac.css">
</head>
<body>

<font class="text">

Currently, the Tactical Office supplies an office to the following personnel:
<p>

<table width="375" border="0">
  <tr>
    <td width="75" class="text" align="center"><b>PIN</b></td>
    <td width="200" class="text" align="left"><b>NAME</b></td>
    <td width="100" class="text" align="left"><b>POSITION</b></td>
  </tr>

  <?php
  include "database/dbstuff.tac";

  ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbusername,  $dbpassword)) or die("Unable to connect to database");
  ((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
  $query = "SELECT * FROM roster ORDER BY N_Position,N_Rank,N_Name";
  $result = mysqli_query($GLOBALS["___mysqli_ston"], $query);

  while ($staff = mysqli_fetch_row($result)) {
    if ($staff[$N_Position] < 7 && $staff[$N_PIN] < 30000) {
      ?>
      <tr>
        <td width="75" align="center" class="text" ><?php echo $staff[$N_PIN]; ?></td>
        <td width="200" align="left" class="text" ><a href="http://www.ehtiecorps.org/rosters/personnel.asp?record=<?php echo $staff[$N_PIN]; ?>" target="_BLANK"><?php echo $ranks[$staff[$N_Rank]]." ".$staff[$N_Name]; ?></a></td>
        <td width="100" align="left" class="text" ><?php echo $tac[$staff[$N_Position]]; ?></td>
      </tr>
      <?php
      }
   }
   ?>
</table>


</body>
</html>
