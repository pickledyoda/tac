<html>
<head>
	<link rel="stylesheet" type="text/css" href="tac.css">
</head>

<body class="text">
Welcome to the website of the Emperor's Hammer's Tactical Center. The Tactical Center is the home
of the Tactical Officer and his staff, whose primary responsibility is to maintain the Mission
Compendium and High Scores. The Tactical Center serves as platform for the entire process that
leads to new battles being released onto the Mission Compendium.
<p>

	<?php
	include "tac/dbstuff.tac";
	($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost, $dbusername, $dbpassword)) or die("Unable to connect to database");
	((bool)mysqli_query($GLOBALS["___mysqli_ston"], "USE " . $dbname));
	$query  = "SELECT * FROM counter";
	$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
	if (!$result) {
		die(mysqli_error($GLOBALS["___mysqli_ston"]));
	}
	$battot = 0;
	$mistot = 0;

	$counts = [];


	while ($countme = mysqli_fetch_row($result)) {
		$plt = $countme[$C_PLATFORM];
		$b = $countme[$C_BATTLES];
		$m = $countme[$C_MISSIONS];

		$battot += $b;
		$mistot += $m;
		$counts[$plt] = ['b' => $b, 'm' => $m];
	}

	$query  = "SELECT * FROM roster WHERE N_Position = '1'";
	$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
	$data   = mysqli_fetch_row($result);
	$tac    = $ranks[$data[$N_Rank]] . " " . $data[$N_Name];

	?>
	Currently the Mission Compendium holds <?php echo $battot; ?> released battles with a total of <?php echo $mistot; ?>
	missions.
<table width="680" border="0">
	<tr>
		<td width="200" valign="top"><font color="#127734">game</font></td>
		<td width="40" align="right" valign="top"><font color="#127734">battles</font></td>
		<td width="40" align="right" valign="top"><font color="#127734">missions</font></td>
		<td width="25">&nbsp;</td>
		<td width="200" valign="top"><font color="#127734">game</font></td>
		<td width="40" align="right" valign="top"><font color="#127734">battles</font></td>
		<td width="40" align="right" valign="top"><font color="#127734">missions</font></td>
	</tr>
	<tr>
		<td width="200" valign="top"><font color="#FF1234">TIE Fighter<br>X-wing vs TIE Fighter<br>Balance of Power<br>X-wing
				Alliance</font></td>
		<td width="40" align="right" valign="top"><?php echo $counts['TIE']['b']; ?><br><?php echo $counts['XvT']['b']; ?><br><?php echo $counts['BoP']['b']; ?>
			<br><?php echo $counts['XWA']['b']; ?></td>
		<td width="40" align="right" valign="top"><?php echo $counts['TIE']['m']; ?><br><?php echo $counts['XvT']['m']; ?><br><?php echo $counts['BoP']['m']; ?>
			<br><?php echo $counts['XWA']['m']; ?></td>
		<td width="25">&nbsp;</td>
		<td width="200" valign="top"><font color="#FF1234">X-wing<br>SW: Galactic Battlegrounds<br>Jedi Academy</font></td>
		<td width="40" align="right" valign="top"><?php echo $counts['XW']['b']; ?><br><?php echo $counts['SWGB']['b']; ?><br><?php echo $counts['JA']['b']; ?></td>
		<td width="40" align="right" valign="top"><?php echo $counts['XW']['m']; ?><br><?php echo $counts['SWGB']['m']; ?><br><?php echo $counts['JA']['m']; ?></td>
	</tr>
</table>

<p>
	If you have any questions/suggestions, feel free to e-mail the Tactical Officer at any time. That's
	what he's there for !
<p>

	<i><?php echo $tac; ?></i>
	<br>
	<font class="tiny">
		<div align="left">TAC/AD Pickled Yoda/CS-6/VSDII Sinister</div>
		<div align="left">IC/GOE/GSx5/SSx12/BSx13/PCx13/ISMx21/IS-3PW-27GW-109SW-179BW-3PR-31GR-65SR-93BR/MoI/
			MoT-6rh/LoC-RS-TS-IS-CSx4-Rx2/LoS-PS-RS-TS-IS-CSx4/DFC-SW-Rx2/MoC-7doc-6poc-7goc-6soc-
			54boc/CoLx11/CoE/CoSx2/CoB/LoAx45/OV-15E [IMPR] [Veteran 4th] [Ranger 3rd] {IWATS-AIM-
			AMP-ASP-BX-CBX-CSS-CTW-DW-FLA-FW-FWT-FZ-GFX-HIST-IBX-ICQ-IIC/1/2/3-JS-LIN-M/1/2-MCBS-
			MP/1/2-MS-PHP-RT-SFW-SGBMC-SM/3/4-SWGB-TLN-TM/1/3-TT-VBS-WIKI-WM-WPN-XAM-XMD-XML-XTM/
			1-XTT-XWAI}
		</div>
	</font>
</body>
</html>
