</main>
<footer class="bg-dark text-center">
	<p class="mb-0">The Tactical Office site is maintained by <a class="text-warning"
			href="http://tc.emperorshammer.org/record.php?pin=9555&type=profile" target="_blank">Admiral Pickled
			Yoda</a></p>
	<p>&copy; <a class="text-warning" href="http://www.emperorshammer.org/" target="_blank">Emperor's Hammer</a>
		2015-<?php echo date('Y', time()); ?></p>
</footer>