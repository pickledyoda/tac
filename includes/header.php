<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <title><?php echo $title; ?></title>
  <link href="/assets/style/pyrite.css" rel="stylesheet" />
  <style>
    .xdebug-error {
      color: black;
      font-size: 120%;
    }

    .manual {
      display: flex;
      flex-direction: row;
    }

    .sidebar {
      position: sticky;
      top: 0;
    }

    .content {}
  </style>
  <script src="/assets/jquery/jquery.slim.min.js"></script>
  <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
</head>

<body class="container">
  <header class="bg-dark">
    <h1 class="text-center mb-0">Tactical Office</h1>
  </header>
  <nav class="nav bg-dark">
    <ul class="nav nav-pills p-3">
      <?php
      $current = $_SERVER['SCRIPT_NAME'];
      $p = '';
      if (strpos($current, 'database/')) {
        $p = '../';
      }

      $menu = [
        "{$p}index.php" => "Home",
        "{$p}office.php" => "TAC Office",
        "{$p}manual.php" => "TAC Manual",
        "{$p}queue.php" => "Battle Queue",
        "{$p}submit.php" => "Battle Submission Form",
        "{$p}database/admin.php" => "Database"
      ];

      foreach ($menu as $href => $label) {
        $css = strpos($current, $href) ? 'active' : '';
        echo "<li class='nav-item'><a href='{$href}' class='text-light nav-link {$css}'>{$label}</a></li>";
      }
      ?>
    </ul>
  </nav>

  <main role="main" class="jumbotron mb-0">