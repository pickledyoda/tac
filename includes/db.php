<?php

/*  login info  */

$dbname     = "emperors_tacc";
$dbhost     = "mysql";
$dbusername = "emperors_tacc";
$dbpassword = "5TAC5SWS";

/*
DB Schema

table name: roster
=========================================
| N_ID       | int(11)     | NO   | PRI | NULL    | auto_increment |
| N_Position | int(11)     | YES  |     | NULL    |                | // refer Constants::TAC_POS
| N_Name     | varchar(50) | YES  |     | NULL    |                |
| N_PIN      | int(11)     | YES  |     | NULL    |                |
| N_Subgroup | int(11)     | YES  |     | NULL    |                |
| N_Email    | varchar(50) | YES  |     | NULL    |                |
| N_Pts      | int(11)     | YES  |     | NULL    |                |
| N_Rank     | varchar(50) | YES  |     | NULL    |                |
| N_Password | varchar(50) | YES  |     | NULL    |                |
| N_Platform | varchar(50) | YES  |     | NULL    |                | // space delimited Constants::PLATFORM

table name: battles
=========================================
| B_ID         | int(11)        | NO   | PRI | NULL    | auto_increment |
| B_Name       | varchar(100)   | YES  |     | NULL    |                |
| B_Type       | varchar(25)    | YES  |     | NULL    |                | // Constants::BATTLE_TYPE
| B_Platform   | varchar(50)    | YES  |     | NULL    |                | // Constants::PLATFORM
| B_Medal      | varchar(50)    | YES  |     | NULL    |                |
| B_Missions   | int(11)        | YES  |     | NULL    |                |
| B_File       | varchar(100)   | YES  |     | NULL    |                | // File path uploads/
| B_Patches    | varchar(100)   | YES  |     | NULL    |                |
| B_Comments   | varchar(2000)  | YES  |     | NULL    |                |
| B_Author     | varchar(50)    | YES  |     | NULL    |                |
| B_PIN        | int(11)        | YES  |     | NULL    |                |
| B_EMail      | varchar(50)    | YES  |     | NULL    |                |
| D_Status     | int(11)        | YES  |     | NULL    |                | // Constants::BATTLE_STATUS
| D_TestEasy   | int(11)        | YES  |     | NULL    |                | // PIN
| D_TestMedium | int(11)        | YES  |     | NULL    |                | // PIN
| D_TestHard   | int(11)        | YES  |     | NULL    |                | // PIN
| D_Correct    | int(11)        | YES  |     | NULL    |                | // PIN
| D_History    | varchar(10000) | YES  |     | NULL    |                | // really fucking annoying delimited ascii shit
| D_Corrected  | varchar(100)   | YES  |     | NULL    |                | // File path corrected/

table name: reports
=========================================
| R_ID     | int(11)        | NO   | PRI | NULL    | auto_increment |
| R_Battle | int(11)        | YES  |     | NULL    |                |
| R_Level  | varchar(10)    | YES  |     | NULL    |                |
| R_Tester | int(11)        | YES  |     | NULL    |                | // PIN
| R_Status | int(11)        | YES  |     | NULL    |                | // Constants::REPORT_STATUS
| R_Report | varchar(30000) | YES  |     | NULL    |                | // 3 way delimited ascii shit
| R_Reason | varchar(1000)  | YES  |     | NULL    |                | 

/*
table name: counter
=========================================
| C_ID       | int(11)     | NO   | PRI | NULL    | auto_increment |
| C_PLATFORM | varchar(10) | YES  |     | NULL    |                | // Constants::PLATFORM
| C_BATTLES  | int(11)     | YES  |     | NULL    |                |
| C_MISSIONS | int(11)     | YES  |     | NULL    |                |
+------------+-------------+------+-----+---------+----------------+
*/

session_start();
// DB functions... break out somewhere when the file becomes too big
$dbLink = mysqli_connect($dbhost, $dbusername, $dbpassword, $dbname) or die("Unable to connect to database");

function tacQuery($sql)
{
  global $dbLink;

  $query = mysqli_query($dbLink, $sql);
  if (!$query) {
    die(mysqli_error($dbLink));
  }

  return $query;
}

function tacQueryOne($sql)
{
  return mysqli_fetch_assoc(tacQuery($sql));
}

function tacQueryAll($sql)
{
  $query = tacQuery($sql);
  $rows = [];
  while ($row = mysqli_fetch_assoc($query)) {
    $rows[] = $row;
  }
  return $rows;
}
