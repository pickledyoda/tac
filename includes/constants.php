<?php
define('DELIM_A', chr(182));
define('DELIM_B', chr(174));
define('DELIM_C', chr(164));

define('POS_TAC', 1);
define('POS_CATAC', 2);
define('POS_TACA', 3);

define('BATTLE_REJECTED', 7);
define('BATTLE_ACCEPTED', 9);

class Constants
{
  public static $PLATFORM = ["", "TIE", "XvT", "BoP", "XWA", "XW", "JA", "SWGB"];
  public static $PLATFORM_FULL = ["", "TIE Fighter", "X-Wing vs TIE Fighter", "Balance of Power", "X-Wing Alliance", "X-Wing", "Jedi Academy", "Star Wars Galactic Battlegrounds"];
  public static $BATTLE_TYPE = ["", "TC", "IW", "DB", "ID", "BHG", "CAB", "FCHG", "IS", "HF", "FMC", "DIR", "CD", "MP", "FREE"];
  public static $BATTLE_STATUS = ["", "submitted to TAC", "initial check", "queued", "under testing", "under correction", "final check", "rejected", "accepted", "released"];
  public static $REPORT_STATUS = ["", "submitted", "accepted", "rejected", "corrected"];
  public static $TAC_POS = ["", "TAC", "CA:TAC", "TACA", "TCT", "TCS", "TCT-TCS", "RSV", "ADMIN", "NEW"];
  public static $TAC_POS_FULL = ["", "Tactical officer (TAC)", "Command Attache (CA:TAC)", "Command Assistant (TACA)", "Tactician (TCT)", "Tactical Surveyor (TCS)", "Tactician-Surveyor (TCT-TCS)", "Reservist (RSV)", "ADMIN", "New member"];
  public static $RANKS     = ["", "GA", "SA", "HA", "FA", "AD", "VA", "RA", "GN", "COL", "LC", "MAJ", "CPT", "CM", "LCM", "LT", "SL", "CT"];
  public static $RANKS_FULL = ["", "Grand Admiral", "Sector Admiral", "High Admiral", "Fleet Admiral", "Admiral", "Vice Admiral", "Rear Admiral", "General", "Colonel", "Lieutenant Colonel", "Major", "Captain", "Commander", "Lieutenant Commander", "Lieutenant", "Sub Lieutenant", "Cadet"];
}
