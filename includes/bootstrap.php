<?php
require_once(__DIR__ . '/constants.php');
require_once(__DIR__ . '/db.php');
require_once(__DIR__ . '/model/bootstrap.php');

function pre($thing)
{
  echo "<pre class='text-light'>";
  print_r($thing);
  echo "</pre>";
}

function staff($position, $asArray = FALSE)
{
  $sql = "SELECT * FROM roster WHERE N_Position = '$position'";
  return $asArray ? tacQueryAll($sql) : tacQueryOne($sql);
}

function label($staff)
{
  if (!$staff) {
    return '';
  }
  return Constants::$RANKS[$staff['N_Rank']] . " " . $staff['N_Name'];
}

function getTACLabel()
{
  return label(staff(POS_TAC));
}

function getCATAC()
{
  return label(staff(POS_CATAC));
}

function getTACA()
{
  return array_map('label', staff(POS_TACA, TRUE));
}

function battle($id)
{
  return tacQueryOne("SELECT * FROM battles where B_ID=$id");
}
