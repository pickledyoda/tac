<?php
class Report
{
  public $id;
  public $battleID;
  public $level;
  public $testerPIN;
  public $statusID;
  public $reportString;
  public $reason;

  public function __construct($row)
  {
    $this->id = $row['R_ID'];
    $this->battleID = $row['R_Battle'];
    $this->level = $row['R_Level'];
    $this->testerPIN = $row['R_Tester'];
    $this->statusID = $row['R_Status'];
    $this->reportString = $row['R_Report'];
    $this->reason = $row['R_Reason'];
  }

  public function battle()
  {
    return Battle::load($this->battleID);
  }

  public function tester()
  {
    return Roster::get($this->testerPIN);
  }

  public function status()
  {
    return Constants::$REPORT_STATUS[$this->statusID];
  }

  public function save()
  {
    // TODO
  }

  /** @return Report */
  public static function load($id)
  {
    static $lookup;
    if (!$lookup) $lookup = [];

    if (!isset($lookup[$id])) {
      $row = tacQueryOne("SELECT * FROM reports WHERE R_ID=$id");
      $lookup[$id] = $row ? new Report($row) : $row;
    }

    return $lookup[$id];
  }
}
