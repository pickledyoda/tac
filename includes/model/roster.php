<?php
class Roster
{
  public static $all = [];
  public static $lookup = [];

  public $id;
  public $positionID;
  public $name;
  public $PIN;
  public $subgroup;
  public $email;
  public $points;
  public $rankID;
  public $password;
  public $platforms;

  public function __construct($row)
  {
    $this->id = $row['N_ID'];
    $this->positionID = (int) $row['N_Position'];
    $this->name = $row['N_Name'];
    $this->PIN = $row['N_PIN'];
    $this->subgroup = $row['N_Subgroup'];
    $this->email = $row['N_Email'];
    $this->points = $row['N_Pts'];
    $this->rankID = $row['N_Rank'];
    $this->password = $row['N_Password'];
    $this->platforms = $row['N_Platform'] ? explode(" ", $row['N_Platform']) : [];
  }

  public function rank($full)
  {
    return $full ? Constants::$RANKS_FULL[$this->rankID] : Constants::$RANKS[$this->rankID];
  }

  public function position()
  {
    return Constants::$TAC_POS[$this->positionID];
  }

  public function label($full = FALSE)
  {
    return "{$this->rank($full)} {$this->name} ({$this->position()})";
  }

  public function labelWithPosition($full = FALSE)
  {
    return "{$this->rank($full)} {$this->name} ({$this->position()})";
  }

  public function __toString()
  {
    return $this->label();
  }

  public function save()
  {
    //TODO implement
  }

  public static function load()
  {
    $rows = tacQueryAll("SELECT * from roster ORDER BY N_Position, N_Rank, N_Name");
    foreach ($rows as $row) {
      $staff = new Roster($row);
      Roster::$all[] = $staff;
      Roster::$lookup[$staff->PIN] = $staff;
    }
  }

  public static function get($id)
  {
    return Roster::$lookup[$id];
  }

  public static function getByPosition($positionID)
  {
    $matches = array_filter(Roster::$all, function ($staff) use ($positionID) {
      return $staff->positionID === $positionID;
    });
    return $matches;
  }
}
