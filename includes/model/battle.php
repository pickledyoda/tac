<?php
class Battle
{
  public $id;
  public $name;
  public $type;
  public $platform;
  public $medal;
  public $missions;
  public $file;
  public $patches;
  public $comments;
  public $authorName;
  public $authorPIN;
  public $authorEmail;
  public $status;
  public $easyTester;
  public $mediumTester;
  public $hardTester;
  public $corrector;
  public $historyString;
  public $correctedFile;

  public function __construct($row)
  {
    $this->id = $row['B_ID'];
    $this->name = $row['B_Name'];
    $this->type = $row['B_Type'];
    $this->platform = $row['B_Platform'];
    $this->medal = $row['B_Medal'];
    $this->missions = $row['B_Missions'];
    $this->file = $row['B_File'];
    $this->patches = $row['B_Patches'];
    $this->comments = $row['B_Comments'];
    $this->authorName = $row['B_Author'];
    $this->authorPIN = $row['B_PIN'];
    $this->authorEmail = $row['B_EMail'];
    $this->status = $row['D_Status'];
    $this->easyTester = $row['D_TestEasy'];
    $this->mediumTester = $row['D_TestMedium'];
    $this->hardTester = $row['D_TestHard'];
    $this->corrector = $row['D_Correct'];
    $this->historyString = $row['D_History'];
    $this->correctedFile = $row['D_Corrected'];
  }

  public function history()
  {
    return array_map(function ($row) {
      return explode(DELIM_C, $row);
    }, explode(DELIM_A, $this->historyString));
  }

  public function submissionDate()
  {
    $first = $this->history()[0][0];
    return date("d M Y", $first);
  }

  public function status()
  {
    global $batstat;
    return $batstat[$this->status];
  }

  public function fields()
  {
    $fields = [
      'Submitted on' => $this->submissionDate(),
      'Battle Type' => "$this->platform - $this->type",
      'Required Patches' => $this->patches,
      'Medal' => $this->medal,
      'Missions' => $this->missions,
      'Author Comments' => $this->comments,
      'Filename' => $this->file
    ];
    if ($this->easyTester) {
      $fields['Easy Tester'] = Roster::get($this->easyTester)->label();
    }
    if ($this->mediumTester) {
      $fields['Medium Tester'] = Roster::get($this->mediumTester)->label();
    }
    if ($this->hardTester) {
      $fields['Hard Tester'] = Roster::get($this->hardTester)->label();
    }
    if ($this->corrector) {
      $fields['Corrector'] = Roster::get($this->corrector)->label();
    }
    if ($this->status >= 6) {
      $fields['Corrected File'] = $this->correctedFile;
    }
    $fields['Current Status'] = $this->status();
    $fields['History'] = implode("<br />", $this->history());
    return $fields;
  }

  /** @return Battle */
  public static function load($id)
  {
    static $lookup;
    if (!$lookup) $lookup = [];

    if (!isset($lookup[$id])) {
      $row = tacQueryOne("SELECT * from battles where B_ID=$id");
      $lookup[$id] = $row ? new Battle($row) : $row;
    }

    return $lookup[$id];
  }
}
