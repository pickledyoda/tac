<?php
class Counter
{
  public static $all = [];
  public static $lookup = [];
  public static $battleTotal = 0;
  public static $missionTotal = 0;

  public $id;
  public $platform;
  public $battleCount;
  public $missionCount;

  public function __construct($row)
  {
    $this->id = $row['C_ID'];
    $this->platform = $row['C_PLATFORM'];
    $this->battleCount = $row['C_BATTLES'];
    $this->missionCount = $row['C_MISSIONS'];
  }

  public function label()
  {
    $platformID = array_search($this->platform, Constants::$PLATFORM);
    return Constants::$PLATFORM_FULL[$platformID];
  }

  public static function load()
  {
    $rows = tacQueryAll("SELECT * from counter");
    foreach ($rows as $row) {
      $count = new Counter($row);
      Counter::$all[] = $count;
      Counter::$lookup[$count->platform] = $count;
      Counter::$battleTotal += $count->battleCount;
      Counter::$missionTotal += $count->missionCount;
    }
  }
}
